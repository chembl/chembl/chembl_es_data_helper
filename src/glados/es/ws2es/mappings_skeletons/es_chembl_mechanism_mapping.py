# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'action_type': 'TEXT',
            # EXAMPLES:
            # 'INHIBITOR' , 'INHIBITOR' , 'INHIBITOR' , 'INHIBITOR' , 'INHIBITOR' , 'INHIBITOR' , 'INHIBITOR' , 'ANTAGON
            # IST' , 'INHIBITOR' , 'ANTAGONIST'
            'binding_site_comment': 'TEXT',
            # EXAMPLES:
            # '23s rRNA (interacts with the A- and P-sites).' , 'Box B' , 'Hydrophobic cavity in the HA trimer stem at t
            # he interface between two protomers. ' , 'Galactoside binding pocket' , 'Colchicine site' , 'Thumb II site'
            #  , 'Site IV' , 'Effects potentially mediated by beta subunit.' , 'Binds dihydropyridine and nondihydropyri
            # dine binding sites.' , 'Beta tubulin vinca binding domain'
            'direct_interaction': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
            'disease_efficacy': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
            'max_phase': 'NUMERIC',
            # EXAMPLES:
            # '4' , '4' , '4' , '4' , '4' , '4' , '4' , '4' , '4' , '4'
            'mec_id': 'NUMERIC',
            # EXAMPLES:
            # '13' , '14' , '15' , '16' , '17' , '18' , '19' , '20' , '7412' , '21'
            'mechanism_comment': 'TEXT',
            # EXAMPLES:
            # 'Expressed in eye' , 'Expressed in eye' , 'Role in regulating gastric secretion, M3 likely involved' , 'In
            # dicated for overactive bladder, M3 likely responsible' , 'Role in regulating gastric secretion and overact
            # ive bladded, M3 likely involved' , 'Indicated in mydriasis and regulating gastric secretion, M3 likely inv
            # olved (main form in iris)' , 'Dacomitinib is an irreversible pan-ERBB inhibitor.' , 'Role in regulating ga
            # stric secretion, M3 likely involved' , 'Role in regulating gastric secretion, M3 likely involved' , 'Role 
            # in regulating gastric secretion, M3 likely involved'
            'mechanism_of_action': 'TEXT',
            # EXAMPLES:
            # 'Carbonic anhydrase VII inhibitor' , 'Carbonic anhydrase I inhibitor' , 'Carbonic anhydrase I inhibitor' ,
            #  'Carbonic anhydrase I inhibitor' , 'Carbonic anhydrase I inhibitor' , 'Carbonic anhydrase I inhibitor' , 
            # 'Cytochrome b inhibitor' , 'Muscarinic acetylcholine receptor M3 antagonist' , 'PI3-kinase p110-alpha subu
            # nit inhibitor' , 'Muscarinic acetylcholine receptor M3 antagonist'
            'mechanism_refs': 
            {
                'properties': 
                {
                    'ref_id': 'TEXT',
                    # EXAMPLES:
                    # 'setid=8e162b6d-8fa6-45f6-80d8-5132d94c1207' , '1460006' , '10713865' , '18336310' , '10713865' , 
                    # 'setid=8e162b6d-8fa6-45f6-80d8-5132d94c1207' , 'setid=b426b6bf-f07e-4580-97ae-dfca1ddf5b8f' , '978
                    # 0702034718 PP. 153' , 'label/2017/209936s000lbl.pdf' , 'setid=5e29f133-270c-4e5a-8493-e038c163a891
                    # '
                    'ref_type': 'TEXT',
                    # EXAMPLES:
                    # 'DailyMed' , 'PubMed' , 'PubMed' , 'PubMed' , 'PubMed' , 'DailyMed' , 'DailyMed' , 'ISBN' , 'FDA' 
                    # , 'DailyMed'
                    'ref_url': 'TEXT',
                    # EXAMPLES:
                    # 'http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?setid=8e162b6d-8fa6-45f6-80d8-5132d94c1207' , 'ht
                    # tp://europepmc.org/abstract/MED/1460006' , 'http://europepmc.org/abstract/MED/10713865' , 'http://
                    # europepmc.org/abstract/MED/18336310' , 'http://europepmc.org/abstract/MED/10713865' , 'http://dail
                    # ymed.nlm.nih.gov/dailymed/lookup.cfm?setid=8e162b6d-8fa6-45f6-80d8-5132d94c1207' , 'http://dailyme
                    # d.nlm.nih.gov/dailymed/lookup.cfm?setid=b426b6bf-f07e-4580-97ae-dfca1ddf5b8f#nlm34090-1' , 'http:/
                    # /www.isbnsearch.org/isbn/9780702034718' , 'https://www.accessdata.fda.gov/drugsatfda_docs/label/20
                    # 17/209936s000lbl.pdf' , 'http://dailymed.nlm.nih.gov/dailymed/lookup.cfm?setid=5e29f133-270c-4e5a-
                    # 8493-e038c163a891'
                }
            },
            'molecular_mechanism': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL19' , 'CHEMBL1201117' , 'CHEMBL1200814' , 'CHEMBL17' , 'CHEMBL20' , 'CHEMBL19' , 'CHEMBL1450' , 'CH
            # EMBL1200771' , 'CHEMBL3218576' , 'CHEMBL1722209'
            'parent_molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL19' , 'CHEMBL1201117' , 'CHEMBL20' , 'CHEMBL17' , 'CHEMBL20' , 'CHEMBL19' , 'CHEMBL1450' , 'CHEMBL1
            # 201354' , 'CHEMBL3218576' , 'CHEMBL1382'
            'record_id': 'NUMERIC',
            # EXAMPLES:
            # '1343810' , '1344053' , '1344649' , '1343255' , '1344903' , '1343810' , '1343336' , '1343534' , '2473104' 
            # , '1343271'
            'selectivity_comment': 'TEXT',
            # EXAMPLES:
            # 'High selectivity for CXCR2 over CXCR1' , 'IC(50) inhibition PARP2 (2.1 nM)' , 'IC(50) inhibition PARP1 (3
            # .8 nM) ' , 'M3 selective' , 'May be selective for LXR beta' , 'Binds predominantly to MCR-1.' , 'Most acti
            # ve at the gamma subtype, > 20-fold selectivity over RAR alpha and RAR beta.' , 'Indicated for infections c
            # aused by Streptococcus pneumoniae, Staphylococcus aureus (methicillin-susceptible isolates), Haemophilus i
            # nfluenzae, Legionella pneumophila, Mycoplasma pneumoniae, and Chlamydophila pneumoniae. Also active agains
            # t Staphylococcus aureus (methicillin-resistant isolates) Streptococcus agalactiae, Streptococcus anginosus
            # , Streptococcus mitis, Streptococcus pyogenes, Streptococcus salivarius, Haemophilus parainfluenzae and Mo
            # raxella catarrhalis but not validated for these bacteria in clinical trials.' , 'High affinity for the 5-H
            # T1F receptor.' , 'Active against Gram-negative bacteria. No relevant activity against Gram-positives and a
            # naerobes.  Indicated for Escherichia coli, Klebsiella pneumoniae, Proteus mirabilis, Pseudomonas aeruginos
            # a, and Enterobacter cloacae complex.  Also active against (but not necessarily indicated for) certain S. m
            # altophilia, meropenem­resistant Enterobacteriaceae, P. aeruginosa, and A. baumannii isolates. This include
            # s some meropenem, ciprofloxacin, and amikacin-resistant isolates including some isolates with genetically 
            # confirmed resistance genes.  Also displays in vitro against the following bacteria but the clinical releva
            # nce is unknown - Acinetobacter baumannii, Citrobacter freundii complex, Citrobacter koseri, Klebsiella aer
            # ogenes, Klebsiella oxytoca, Morganella morganii, Proteus vulgaris, Providencia rettgeri, Serratia marcesce
            # ns, Stenotrophomonas maltophilia.'
            'site_id': 'NUMERIC',
            # EXAMPLES:
            # '2621' , '2627' , '2627' , '2627' , '1484' , '2627' , '2629' , '9800' , '2621' , '2621'
            'target_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL2326' , 'CHEMBL261' , 'CHEMBL261' , 'CHEMBL261' , 'CHEMBL261' , 'CHEMBL261' , 'CHEMBL1777' , 'CHEMB
            # L245' , 'CHEMBL4005' , 'CHEMBL245'
            'variant_sequence': 
            {
                'properties': 
                {
                    'accession': 'TEXT',
                    # EXAMPLES:
                    # 'P68871' , 'P13569' , 'P00533' , 'P00533' , 'P00533' , 'P00533' , 'P00533' , 'P15056' , 'P15056' ,
                    #  'P00533'
                    'isoform': 'NUMERIC',
                    # EXAMPLES:
                    # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
                    'mutation': 'TEXT',
                    # EXAMPLES:
                    # 'E7V' , 'F508del' , 'UNDEFINED MUTATION' , 'UNDEFINED MUTATION' , 'UNDEFINED MUTATION' , 'UNDEFINE
                    # D MUTATION' , 'T790M' , 'T790M' , 'T790M' , 'T790M'
                    'organism': 'TEXT',
                    # EXAMPLES:
                    # 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens
                    # ' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens'
                    'sequence': 'TEXT',
                    # EXAMPLES:
                    # 'MVHLTPVEKSAVTALWGKVNVDEVGGEALGRLLVVYPWTQRFFESFGDLSTPDAVMGNPKVKAHGKKVLGAFSDGLAHLDNLKGTFATLSELHCDKL
                    # HVDPENFRLLGNVLVCVLAHHFGKEFTPPVQAAYQKVVAGVANALAHKYH' , 'MQRSPLEKASVVSKLFFSWTRPILRKGYRQRLELSDIYQIPSV
                    # DSADNLSEKLEREWDRELASKKNPKLINALRRCFFWRFMFYGIFLYLGEVTKAVQPLLLGRIIASYDPDNKEERSIAIYLGIGLCLLFIVRTLLLHPA
                    # IFGLHHIGMQMRIAMFSLIYKKTLKLSSRVLDKISIGQLVSLLSNNLNKFDEGLALAHFVWIAPLQVALLMGLIWELLQASAFCGLGFLIVLALFQAG
                    # LGRMMMKYRDQRAGKISERLVITSEMIENIQSVKAYCWEEAMEKMIENLRQTELKLTRKAAYVRYFNSSAFFFSGFFVVFLSVLPYALIKGIILRKIF
                    # TTISFCIVLRMAVTRQFPWAVQTWYDSLGAINKIQDFLQKQEYKTLEYNLTTTEVVMENVTAFWEEGFGELFEKAKQNNNNRKTSNGDDSLFFSNFSL
                    # LGTPVLKDINFKIERGQLLAVAGSTGAGKTSLLMVIMGELEPSEGKIKHSGRISFCSQFSWIMPGTIKENIIGVSYDEYRYRSVIKACQLEEDISKFA
                    # EKDNIVLGEGGITLSGGQRARISLARAVYKDADLYLLDSPFGYLDVLTEKEIFESCVCKLMANKTRILVTSKMEHLKKADKILILHEGSSYFYGTFSE
                    # LQNLQPDFSSKLMGCDSFDQFSAERRNSILTETLHRFSLEGDAPVSWTETKKQSFKQTGEFGEKRKNSILNPINSIRKFSIVQKTPLQMNGIEEDSDE
                    # PLERRLSLVPDSEQGEAILPRISVISTGPTLQARRRQSVLNLMTHSVNQGQNIHRKTTASTRKVSLAPQANLTELDIYSRRLSQETGLEISEEINEED
                    # LKECFFDDMESIPAVTTWNTYLRYITVHKSLIFVLIWCLVIFLAEVAASLVVLWLLGNTPLQDKGNSTHSRNNSYAVIITSTSSYYVFYIYVGVADTL
                    # LAMGFFRGLPLVHTLITVSKILHHKMLHSVLQAPMSTLNTLKAGGILNRFSKDIAILDDLLPLTIFDFIQLLLIVIGAIAVVAVLQPYIFVATVPVIV
                    # AFIMLRAYFLQTSQQLKQLESEGRSPIFTHLVTSLKGLWTLRAFGRQPYFETLFHKALNLHTANWFLYLSTLRWFQMRIEMIFVIFFIAVTFISILTT
                    # GEGEGRVGIILTLAMNIMSTLQWAVNSSIDVDSLMRSVSRVFKFIDMPTEGKPTKSTKPYKNGQLSKVMIIENSHVKKDDIWPSGGQMTVKDLTAKYT
                    # EGGNAILENISFSISPGQRVGLLGRTGSGKSTLLSAFLRLLNTEGEIQIDGVSWDSITLQQWRKAFGVIPQKVFIFSGTFRKNLDPYEQWSDQEIWKV
                    # ADEVGLRSVIEQFPGKLDFVLVDGGCVLSHGHKQLMCLARSVLSKAKILLLDEPSAHLDPVTYQIIRRTLKQAFADCTVILCEHRIEAMLECQQFLVI
                    # EENKVRQYDSIQKLLNERSLFRQAISPSDRVKLFPHRNSSKCKSKPQIAALKEETEEEVQDTRL' , '' , '' , '' , '' , 'MRPSGTAGA
                    # ALLALLAALCPASRALEEKKVCQGTSNKLTQLGTFEDHFLSLQRMFNNCEVVLGNLEITYVQRNYDLSFLKTIQEVAGYVLIALNTVERIPLENLQII
                    # RGNMYYENSYALAVLSNYDANKTGLKELPMRNLQEILHGAVRFSNNPALCNVESIQWRDIVSSDFLSNMSMDFQNHLGSCQKCDPSCPNGSCWGAGEE
                    # NCQKLTKIICAQQCSGRCRGKSPSDCCHNQCAAGCTGPRESDCLVCRKFRDEATCKDTCPPLMLYNPTTYQMDVNPEGKYSFGATCVKKCPRNYVVTD
                    # HGSCVRACGADSYEMEEDGVRKCKKCEGPCRKVCNGIGIGEFKDSLSINATNIKHFKNCTSISGDLHILPVAFRGDSFTHTPPLDPQELDILKTVKEI
                    # TGFLLIQAWPENRTDLHAFENLEIIRGRTKQHGQFSLAVVSLNITSLGLRSLKEISDGDVIISGNKNLCYANTINWKKLFGTSGQKTKIISNRGENSC
                    # KATGQVCHALCSPEGCWGPEPRDCVSCRNVSRGRECVDKCNLLEGEPREFVENSECIQCHPECLPQAMNITCTGRGPDNCIQCAHYIDGPHCVKTCPA
                    # GVMGENNTLVWKYADAGHVCHLCHPNCTYGCTGPGLEGCPTNGPKIPSIATGMVGALLLLLVVALGIGLFMRRRHIVRKRTLRRLLQERELVEPLTPS
                    # GEAPNQALLRILKETEFKKIKVLGSGAFGTVYKGLWIPEGEKVKIPVAIKELREATSPKANKEILDEAYVMASVDNPHVCRLLGICLTSTVQLIMQLM
                    # PFGCLLDYVREHKDNIGSQYLLNWCVQIAKGMNYLEDRRLVHRDLAARNVLVKTPQHVKITDFGLAKLLGAEEKEYHAEGGKVPIKWMALESILHRIY
                    # THQSDVWSYGVTVWELMTFGSKPYDGIPASEISSILEKGERLPQPPICTIDVYMIMVKCWMIDADSRPKFRELIIEFSKMARDPQRYLVIQGDERMHL
                    # PSPTDSNFYRALMDEEDMDDVVDADEYLIPQQGFFSSPSTSRTPLLSSLSATSNNSTVACIDRNGLQSCPIKEDSFLQRYSSDPTGALTEDSIDDTFL
                    # PVPEYINQSVPKRPAGSVQNPVYHNQPLNPAPSRDPHYQDPHSTAVGNPEYLNTVQPTCVNSTFDSPAHWAQKGSHQISLDNPDYQQDFFPKEAKPNG
                    # IFKGSTAENAEYLRVAPQSSEFIGA' , 'MRPSGTAGAALLALLAALCPASRALEEKKVCQGTSNKLTQLGTFEDHFLSLQRMFNNCEVVLGNLEIT
                    # YVQRNYDLSFLKTIQEVAGYVLIALNTVERIPLENLQIIRGNMYYENSYALAVLSNYDANKTGLKELPMRNLQEILHGAVRFSNNPALCNVESIQWRD
                    # IVSSDFLSNMSMDFQNHLGSCQKCDPSCPNGSCWGAGEENCQKLTKIICAQQCSGRCRGKSPSDCCHNQCAAGCTGPRESDCLVCRKFRDEATCKDTC
                    # PPLMLYNPTTYQMDVNPEGKYSFGATCVKKCPRNYVVTDHGSCVRACGADSYEMEEDGVRKCKKCEGPCRKVCNGIGIGEFKDSLSINATNIKHFKNC
                    # TSISGDLHILPVAFRGDSFTHTPPLDPQELDILKTVKEITGFLLIQAWPENRTDLHAFENLEIIRGRTKQHGQFSLAVVSLNITSLGLRSLKEISDGD
                    # VIISGNKNLCYANTINWKKLFGTSGQKTKIISNRGENSCKATGQVCHALCSPEGCWGPEPRDCVSCRNVSRGRECVDKCNLLEGEPREFVENSECIQC
                    # HPECLPQAMNITCTGRGPDNCIQCAHYIDGPHCVKTCPAGVMGENNTLVWKYADAGHVCHLCHPNCTYGCTGPGLEGCPTNGPKIPSIATGMVGALLL
                    # LLVVALGIGLFMRRRHIVRKRTLRRLLQERELVEPLTPSGEAPNQALLRILKETEFKKIKVLGSGAFGTVYKGLWIPEGEKVKIPVAIKELREATSPK
                    # ANKEILDEAYVMASVDNPHVCRLLGICLTSTVQLIMQLMPFGCLLDYVREHKDNIGSQYLLNWCVQIAKGMNYLEDRRLVHRDLAARNVLVKTPQHVK
                    # ITDFGLAKLLGAEEKEYHAEGGKVPIKWMALESILHRIYTHQSDVWSYGVTVWELMTFGSKPYDGIPASEISSILEKGERLPQPPICTIDVYMIMVKC
                    # WMIDADSRPKFRELIIEFSKMARDPQRYLVIQGDERMHLPSPTDSNFYRALMDEEDMDDVVDADEYLIPQQGFFSSPSTSRTPLLSSLSATSNNSTVA
                    # CIDRNGLQSCPIKEDSFLQRYSSDPTGALTEDSIDDTFLPVPEYINQSVPKRPAGSVQNPVYHNQPLNPAPSRDPHYQDPHSTAVGNPEYLNTVQPTC
                    # VNSTFDSPAHWAQKGSHQISLDNPDYQQDFFPKEAKPNGIFKGSTAENAEYLRVAPQSSEFIGA' , 'MRPSGTAGAALLALLAALCPASRALEEKK
                    # VCQGTSNKLTQLGTFEDHFLSLQRMFNNCEVVLGNLEITYVQRNYDLSFLKTIQEVAGYVLIALNTVERIPLENLQIIRGNMYYENSYALAVLSNYDA
                    # NKTGLKELPMRNLQEILHGAVRFSNNPALCNVESIQWRDIVSSDFLSNMSMDFQNHLGSCQKCDPSCPNGSCWGAGEENCQKLTKIICAQQCSGRCRG
                    # KSPSDCCHNQCAAGCTGPRESDCLVCRKFRDEATCKDTCPPLMLYNPTTYQMDVNPEGKYSFGATCVKKCPRNYVVTDHGSCVRACGADSYEMEEDGV
                    # RKCKKCEGPCRKVCNGIGIGEFKDSLSINATNIKHFKNCTSISGDLHILPVAFRGDSFTHTPPLDPQELDILKTVKEITGFLLIQAWPENRTDLHAFE
                    # NLEIIRGRTKQHGQFSLAVVSLNITSLGLRSLKEISDGDVIISGNKNLCYANTINWKKLFGTSGQKTKIISNRGENSCKATGQVCHALCSPEGCWGPE
                    # PRDCVSCRNVSRGRECVDKCNLLEGEPREFVENSECIQCHPECLPQAMNITCTGRGPDNCIQCAHYIDGPHCVKTCPAGVMGENNTLVWKYADAGHVC
                    # HLCHPNCTYGCTGPGLEGCPTNGPKIPSIATGMVGALLLLLVVALGIGLFMRRRHIVRKRTLRRLLQERELVEPLTPSGEAPNQALLRILKETEFKKI
                    # KVLGSGAFGTVYKGLWIPEGEKVKIPVAIKELREATSPKANKEILDEAYVMASVDNPHVCRLLGICLTSTVQLIMQLMPFGCLLDYVREHKDNIGSQY
                    # LLNWCVQIAKGMNYLEDRRLVHRDLAARNVLVKTPQHVKITDFGLAKLLGAEEKEYHAEGGKVPIKWMALESILHRIYTHQSDVWSYGVTVWELMTFG
                    # SKPYDGIPASEISSILEKGERLPQPPICTIDVYMIMVKCWMIDADSRPKFRELIIEFSKMARDPQRYLVIQGDERMHLPSPTDSNFYRALMDEEDMDD
                    # VVDADEYLIPQQGFFSSPSTSRTPLLSSLSATSNNSTVACIDRNGLQSCPIKEDSFLQRYSSDPTGALTEDSIDDTFLPVPEYINQSVPKRPAGSVQN
                    # PVYHNQPLNPAPSRDPHYQDPHSTAVGNPEYLNTVQPTCVNSTFDSPAHWAQKGSHQISLDNPDYQQDFFPKEAKPNGIFKGSTAENAEYLRVAPQSS
                    # EFIGA' , 'MRPSGTAGAALLALLAALCPASRALEEKKVCQGTSNKLTQLGTFEDHFLSLQRMFNNCEVVLGNLEITYVQRNYDLSFLKTIQEVAGY
                    # VLIALNTVERIPLENLQIIRGNMYYENSYALAVLSNYDANKTGLKELPMRNLQEILHGAVRFSNNPALCNVESIQWRDIVSSDFLSNMSMDFQNHLGS
                    # CQKCDPSCPNGSCWGAGEENCQKLTKIICAQQCSGRCRGKSPSDCCHNQCAAGCTGPRESDCLVCRKFRDEATCKDTCPPLMLYNPTTYQMDVNPEGK
                    # YSFGATCVKKCPRNYVVTDHGSCVRACGADSYEMEEDGVRKCKKCEGPCRKVCNGIGIGEFKDSLSINATNIKHFKNCTSISGDLHILPVAFRGDSFT
                    # HTPPLDPQELDILKTVKEITGFLLIQAWPENRTDLHAFENLEIIRGRTKQHGQFSLAVVSLNITSLGLRSLKEISDGDVIISGNKNLCYANTINWKKL
                    # FGTSGQKTKIISNRGENSCKATGQVCHALCSPEGCWGPEPRDCVSCRNVSRGRECVDKCNLLEGEPREFVENSECIQCHPECLPQAMNITCTGRGPDN
                    # CIQCAHYIDGPHCVKTCPAGVMGENNTLVWKYADAGHVCHLCHPNCTYGCTGPGLEGCPTNGPKIPSIATGMVGALLLLLVVALGIGLFMRRRHIVRK
                    # RTLRRLLQERELVEPLTPSGEAPNQALLRILKETEFKKIKVLGSGAFGTVYKGLWIPEGEKVKIPVAIKELREATSPKANKEILDEAYVMASVDNPHV
                    # CRLLGICLTSTVQLIMQLMPFGCLLDYVREHKDNIGSQYLLNWCVQIAKGMNYLEDRRLVHRDLAARNVLVKTPQHVKITDFGLAKLLGAEEKEYHAE
                    # GGKVPIKWMALESILHRIYTHQSDVWSYGVTVWELMTFGSKPYDGIPASEISSILEKGERLPQPPICTIDVYMIMVKCWMIDADSRPKFRELIIEFSK
                    # MARDPQRYLVIQGDERMHLPSPTDSNFYRALMDEEDMDDVVDADEYLIPQQGFFSSPSTSRTPLLSSLSATSNNSTVACIDRNGLQSCPIKEDSFLQR
                    # YSSDPTGALTEDSIDDTFLPVPEYINQSVPKRPAGSVQNPVYHNQPLNPAPSRDPHYQDPHSTAVGNPEYLNTVQPTCVNSTFDSPAHWAQKGSHQIS
                    # LDNPDYQQDFFPKEAKPNGIFKGSTAENAEYLRVAPQSSEFIGA'
                    'tax_id': 'NUMERIC',
                    # EXAMPLES:
                    # '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606'
                    'version': 'NUMERIC',
                    # EXAMPLES:
                    # '2' , '3' , '2' , '2' , '2' , '2' , '2' , '4' , '4' , '2'
                }
            }
        }
    }
