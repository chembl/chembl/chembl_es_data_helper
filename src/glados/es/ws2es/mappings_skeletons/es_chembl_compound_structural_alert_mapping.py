# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'alert': 
            {
                'properties': 
                {
                    'alert_id': 'NUMERIC',
                    # EXAMPLES:
                    # '1' , '7' , '7' , '1' , '7' , '1' , '7' , '1' , '1' , '1'
                    'alert_name': 'TEXT',
                    # EXAMPLES:
                    # 'R1 Reactive alkyl halides' , 'R7 Peroxides' , 'R7 Peroxides' , 'R1 Reactive alkyl halides' , 'R7 
                    # Peroxides' , 'R1 Reactive alkyl halides' , 'R7 Peroxides' , 'R1 Reactive alkyl halides' , 'R1 Reac
                    # tive alkyl halides' , 'R1 Reactive alkyl halides'
                    'alert_set': 
                    {
                        'properties': 
                        {
                            'priority': 'NUMERIC',
                            # EXAMPLES:
                            # '8' , '8' , '8' , '8' , '8' , '8' , '8' , '8' , '8' , '8'
                            'set_name': 'TEXT',
                            # EXAMPLES:
                            # 'Glaxo' , 'Glaxo' , 'Glaxo' , 'Glaxo' , 'Glaxo' , 'Glaxo' , 'Glaxo' , 'Glaxo' , 'Glaxo' , 
                            # 'Glaxo'
                        }
                    },
                    'smarts': 'TEXT',
                    # EXAMPLES:
                    # '[Br,Cl,I][CX4;CH,CH2]' , 'OO' , 'OO' , '[Br,Cl,I][CX4;CH,CH2]' , 'OO' , '[Br,Cl,I][CX4;CH,CH2]' ,
                    #  'OO' , '[Br,Cl,I][CX4;CH,CH2]' , '[Br,Cl,I][CX4;CH,CH2]' , '[Br,Cl,I][CX4;CH,CH2]'
                }
            },
            'cpd_str_alert_id': 'NUMERIC',
            # EXAMPLES:
            # '79064924' , '79069937' , '79069939' , '79064928' , '79069941' , '79060088' , '79069945' , '79055088' , '7
            # 9064936' , '79050090'
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL5398626' , 'CHEMBL507515' , 'CHEMBL510548' , 'CHEMBL5398813' , 'CHEMBL518668' , 'CHEMBL3247518' , '
            # CHEMBL470315' , 'CHEMBL1448058' , 'CHEMBL5400003' , 'CHEMBL325824'
        }
    }
