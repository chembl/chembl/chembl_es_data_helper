# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'document_1_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL1128799' , 'CHEMBL1129107' , 'CHEMBL1129107' , 'CHEMBL1128799' , 'CHEMBL1128799' , 'CHEMBL1129107' 
            # , 'CHEMBL1128799' , 'CHEMBL1129107' , 'CHEMBL1128799' , 'CHEMBL1129107'
            'document_2_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL1806431' , 'CHEMBL1121957' , 'CHEMBL1122001' , 'CHEMBL1828507' , 'CHEMBL1914449' , 'CHEMBL1122111' 
            # , 'CHEMBL1955817' , 'CHEMBL1122112' , 'CHEMBL2429824' , 'CHEMBL1122294'
            'mol_tani': 'NUMERIC',
            # EXAMPLES:
            # '0.0' , '0.0' , '0.0' , '0.0' , '0.0' , '0.0' , '0.0' , '0.0' , '0.0' , '0.0'
            'tid_tani': 'NUMERIC',
            # EXAMPLES:
            # '1.0' , '1.0' , '1.0' , '1.0' , '1.0' , '1.0' , '1.0' , '1.0' , '1.0' , '1.0'
        }
    }
