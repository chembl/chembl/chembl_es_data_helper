# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'efo_id': 'TEXT',
            # EXAMPLES:
            # 'EFO:0004269' , 'EFO:0003086' , 'EFO:0008546' , 'EFO:1001482' , 'EFO:0004276' , 'EFO:0011052' , 'EFO:00110
            # 52' , 'EFO:1002018' , 'EFO:0008568' , 'EFO:1002003'
            'efo_id_for_warning_class': 'TEXT',
            # EXAMPLES:
            # 'EFO:0011052' , 'EFO:0011054' , 'EFO:0011053' , 'EFO:0011046' , 'EFO:0011049' , 'EFO:0011052' , 'EFO:00098
            # 80' , 'EFO:0011052' , 'EFO:0011054' , 'EFO:0011052'
            'efo_term': 'TEXT',
            # EXAMPLES:
            # 'cardiac arrhythmia' , 'kidney disease' , 'poisoning' , 'cardiotoxicity' , 'Stevens-Johnson syndrome' , 'h
            # epatotoxicity' , 'hepatotoxicity' , 'bronchial disease' , 'Sleep Disorder' , 'hypersensitivity reaction di
            # sease'
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL4303288' , 'CHEMBL4303288' , 'CHEMBL4303288' , 'CHEMBL3301581' , 'CHEMBL1252' , 'CHEMBL2108570' , '
            # CHEMBL112' , 'CHEMBL112' , 'CHEMBL1131' , 'CHEMBL1131'
            'parent_molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL1380' , 'CHEMBL1380' , 'CHEMBL1380' , 'CHEMBL3301581' , 'CHEMBL1252' , 'CHEMBL2108570' , 'CHEMBL112
            # ' , 'CHEMBL112' , 'CHEMBL1131' , 'CHEMBL1131'
            'warning_class': 'TEXT',
            # EXAMPLES:
            # 'hepatotoxicity' , 'metabolic toxicity' , 'immune system toxicity' , 'carcinogenicity' , 'misuse' , 'hepat
            # otoxicity' , 'teratogenicity' , 'hepatotoxicity' , 'metabolic toxicity' , 'hepatotoxicity'
            'warning_country': 'TEXT',
            # EXAMPLES:
            # 'United States' , 'United States' , 'United States' , 'United States' , 'United States' , 'United States' 
            # , 'United States' , 'United States' , 'United States' , 'United States'
            'warning_description': 'TEXT',
            # EXAMPLES:
            # 'Serious toxicity to the heart; increase the risk for serious abnormal heart rhythms ' , 'Nephropathy' , '
            # Self-poisoning' , 'Higher rate of cardiovascular events in obese and overweight patients using sibutramine
            #  than in patients managing their weight through exercise and diet alone' , 'low but increased risk of seri
            # ous skin reactions (including Stevens-Johnson syndrome, toxic epidermal necrolysis and drug-rash-with-eosi
            # nophilia-and-systemic-symptoms (DRESS) syndrome)' , 'Hepatoxicity' , 'Hepatotoxicity' , 'occurrence of bro
            # nchospasm' , 'Depression, anxiety, sleep disorders, tremor (shaking) and tardive dyskinesia' , 'associated
            #  with incidences of hypersensitivity of varying severity and serious neurological side effects including t
            # he Guillain-Barre syndrome'
            'warning_id': 'NUMERIC',
            # EXAMPLES:
            # '1' , '2' , '3' , '4' , '5' , '11' , '16' , '17' , '26' , '27'
            'warning_refs': 
            {
                'properties': 
                {
                    'ref_id': 'TEXT',
                    # EXAMPLES:
                    # 'de109a2b-e36c-40d0-85fc-a67a9e7f1ae8' , 'de109a2b-e36c-40d0-85fc-a67a9e7f1ae8' , 'de109a2b-e36c-4
                    # 0d0-85fc-a67a9e7f1ae8' , '712143d9-e21e-4013-bb3b-3426a21060a8' , 'c5177abd-9465-40d8-861d-3904496
                    # d82b7' , 'c5177abd-9465-40d8-861d-3904496d82b7' , '6af396c2-af3e-436d-ba9a-583637495910' , '6af396
                    # c2-af3e-436d-ba9a-583637495910' , 'e047f3b2-feae-4c5e-9d07-1fefb4c0ec25' , 'e047f3b2-feae-4c5e-9d0
                    # 7-1fefb4c0ec25'
                    'ref_type': 'TEXT',
                    # EXAMPLES:
                    # 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyM
                    # ed' , 'DailyMed' , 'DailyMed'
                    'ref_url': 'TEXT',
                    # EXAMPLES:
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=de109a2b-e36c-40d0-85fc-a67a9e7f1ae8' , 
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=de109a2b-e36c-40d0-85fc-a67a9e7f1ae8' , 
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=de109a2b-e36c-40d0-85fc-a67a9e7f1ae8' , 
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=712143d9-e21e-4013-bb3b-3426a21060a8' , 
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=c5177abd-9465-40d8-861d-3904496d82b7' , 
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=c5177abd-9465-40d8-861d-3904496d82b7' , 
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=6af396c2-af3e-436d-ba9a-583637495910' , 
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=6af396c2-af3e-436d-ba9a-583637495910' , 
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=e047f3b2-feae-4c5e-9d07-1fefb4c0ec25' , 
                    # 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=e047f3b2-feae-4c5e-9d07-1fefb4c0ec25'
                }
            },
            'warning_type': 'TEXT',
            # EXAMPLES:
            # 'Black Box Warning' , 'Black Box Warning' , 'Black Box Warning' , 'Black Box Warning' , 'Black Box Warning
            # ' , 'Black Box Warning' , 'Black Box Warning' , 'Black Box Warning' , 'Black Box Warning' , 'Black Box War
            # ning'
            'warning_year': 'NUMERIC',
            # EXAMPLES:
            # '2014' , '1965' , '1980' , '2010' , '2013' , '1999' , '1971' , '2001' , '2005' , '1983'
        }
    }
