# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'compound_key': 'TEXT',
            # EXAMPLES:
            # '8' , '241' , '5c' , '5f' , '21' , '204' , '5d' , '4' , '20' , '7'
            'compound_name': 'TEXT',
            # EXAMPLES:
            # '5-{3-[4-(4-Fluoro-benzyl)-4-hydroxy-piperidin-1-yl]-prop-1-ynyl}-1,3-dihydro-benzoimidazol-2-one' , '2-[(
            # 2,2-Dimethyl-cyclopropanecarbonyl)-amino]-oct-2-enoic acid' , '1-[2-(5-Chloro-2-oxo-benzothiazol-3-yl)-ace
            # tyl]-piperidine-4-carboxylic acid (4-fluoro-3-trifluoromethyl-phenyl)-amide' , '5-Chloro-3-{2-[4-(3,4-dihy
            # dro-2H-quinoline-1-carbonyl)-piperidin-1-yl]-2-oxo-ethyl}-3H-benzothiazol-2-one' , '5-{3-[4-(4-Fluoro-benz
            # yl)-piperidin-1-yl]-prop-1-ynyl}-1,3-dihydro-benzoimidazole-2-thione' , '(S)-{7-Carboxy-7-[(2,2-dimethyl-c
            # yclopropanecarbonyl)-amino]-hept-6-enyl}-trimethyl-ammonium' , '1-[2-(5-Chloro-2-oxo-benzothiazol-3-yl)-ac
            # etyl]-piperidine-4-carboxylic acid (3-benzoyl-phenyl)-amide' , 'N-(1-Carboxy-2-hydroxy-ethyl)-3-{2-[4-(3-g
            # uanidino-propyl)-2,5-dioxo-imidazolidin-1-yl]-acetylamino}-succinamic acid' , '6-{3-[4-(4-Fluoro-benzyl)-p
            # iperidin-1-yl]-prop-1-ynyl}-4,5-dimethyl-1,3-dihydro-benzoimidazol-2-one' , 'Quinoxaline-2-carboxylic acid
            #  (3R,7R,10S,11S,14R)-7,12-dihydroxy-10,13-dimethyl-17-[1-methyl-4-(quinoline-2-carbonyloxy)-butyl]-hexadec
            # ahydro-cyclopenta[a]phenanthren-3-yl ester'
            'document_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL1147596' , 'CHEMBL1123761' , 'CHEMBL1135282' , 'CHEMBL1135282' , 'CHEMBL1147596' , 'CHEMBL1123761' 
            # , 'CHEMBL1135282' , 'CHEMBL1134795' , 'CHEMBL1147596' , 'CHEMBL1127888'
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL19576' , 'CHEMBL427640' , 'CHEMBL16986' , 'CHEMBL16948' , 'CHEMBL19559' , 'CHEMBL3559476' , 'CHEMBL
            # 17590' , 'CHEMBL17181' , 'CHEMBL280882' , 'CHEMBL3138137'
            'record_id': 'NUMERIC',
            # EXAMPLES:
            # '25038' , '10008' , '20038' , '20040' , '25044' , '10009' , '20042' , '20043' , '25045' , '30043'
            'src_id': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
        }
    }
