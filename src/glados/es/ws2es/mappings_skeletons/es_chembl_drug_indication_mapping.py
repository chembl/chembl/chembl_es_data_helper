# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'drugind_id': 'NUMERIC',
            # EXAMPLES:
            # '120070' , '120407' , '125807' , '140133' , '114108' , '120071' , '72501' , '73240' , '120408' , '107526'
            'efo_id': 'TEXT',
            # EXAMPLES:
            # 'EFO:1000786' , 'MONDO:0021311' , 'MONDO:0019072' , 'MONDO:0019736' , 'EFO:0000546' , 'EFO:1001998' , 'EFO
            # :0000616' , 'EFO:0003882' , 'MONDO:0021364' , 'EFO:0000326'
            'efo_term': 'TEXT',
            # EXAMPLES:
            # 'osteoarthritis, hip' , 'malignant tumor of parathyroid gland' , 'intrahepatic cholestasis' , 'dense depos
            # it disease' , 'injury' , 'complex regional pain syndrome' , 'neoplasm' , 'osteoporosis' , 'neoplasm of oro
            # pharynx' , 'central nervous system cancer'
            'indication_refs': 
            {
                'properties': 
                {
                    'ref_id': 'TEXT',
                    # EXAMPLES:
                    # 'NCT04303026' , 'NCT00004074' , 'NCT01899703' , 'NCT00583427' , 'NCT00664352' , 'NCT01788176' , 'N
                    # CT00919503,NCT01804634' , '9f89cb91-bd3b-4103-a8a1-c1c1512fe514' , 'NCT01525927' , 'NCT03838042'
                    'ref_type': 'TEXT',
                    # EXAMPLES:
                    # 'ClinicalTrials' , 'ClinicalTrials' , 'ClinicalTrials' , 'ClinicalTrials' , 'ClinicalTrials' , 'Cl
                    # inicalTrials' , 'ClinicalTrials' , 'DailyMed' , 'ClinicalTrials' , 'ClinicalTrials'
                    'ref_url': 'TEXT',
                    # EXAMPLES:
                    # 'https://clinicaltrials.gov/search?term=NCT04303026' , 'https://clinicaltrials.gov/search?term=NCT
                    # 00004074' , 'https://clinicaltrials.gov/search?term=NCT01899703' , 'https://clinicaltrials.gov/sea
                    # rch?term=NCT00583427' , 'https://clinicaltrials.gov/search?term=NCT00664352' , 'https://clinicaltr
                    # ials.gov/search?term=NCT01788176' , 'https://clinicaltrials.gov/search?term=NCT00919503%20NCT01804
                    # 634' , 'https://dailymed.nlm.nih.gov/dailymed/drugInfo.cfm?setid=9f89cb91-bd3b-4103-a8a1-c1c1512fe
                    # 514' , 'https://clinicaltrials.gov/search?term=NCT01525927' , 'https://clinicaltrials.gov/search?t
                    # erm=NCT03838042'
                }
            },
            'max_phase_for_ind': 'NUMERIC',
            # EXAMPLES:
            # '3.0' , '1.0' , '2.0' , '1.0' , '1.0' , '2.0' , '2.0' , '4.0' , '3.0' , '1.0'
            'mesh_heading': 'TEXT',
            # EXAMPLES:
            # 'Osteoarthritis, Hip' , 'Parathyroid Neoplasms' , 'Cholestasis, Intrahepatic' , 'Glomerulonephritis, Membr
            # anoproliferative' , 'Wounds and Injuries' , 'Complex Regional Pain Syndromes' , 'Neoplasms' , 'Osteoporosi
            # s' , 'Oropharyngeal Neoplasms' , 'Glioma'
            'mesh_id': 'TEXT',
            # EXAMPLES:
            # 'D015207' , 'D010282' , 'D002780' , 'D015432' , 'D014947' , 'D020918' , 'D009369' , 'D010024' , 'D009959' 
            # , 'D005910'
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL4303669' , 'CHEMBL1351' , 'CHEMBL2387408' , 'CHEMBL2108086' , 'CHEMBL402001' , 'CHEMBL4303669' , 'C
            # HEMBL269732' , 'CHEMBL1364' , 'CHEMBL1351' , 'CHEMBL27759'
            'parent_molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL924' , 'CHEMBL1351' , 'CHEMBL2387408' , 'CHEMBL2108086' , 'CHEMBL402001' , 'CHEMBL924' , 'CHEMBL269
            # 732' , 'CHEMBL1364' , 'CHEMBL1351' , 'CHEMBL27759'
        }
    }
