# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'related_target_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL2096619' , 'CHEMBL2096667' , 'CHEMBL4296098' , 'CHEMBL2093868' , 'CHEMBL2111324' , 'CHEMBL3831290' 
            # , 'CHEMBL3542438' , 'CHEMBL4748231' , 'CHEMBL4524013' , 'CHEMBL4523739'
            'relationship': 'TEXT',
            # EXAMPLES:
            # 'SUBSET OF' , 'SUBSET OF' , 'SUBSET OF' , 'SUBSET OF' , 'SUBSET OF' , 'SUBSET OF' , 'SUBSET OF' , 'SUBSET 
            # OF' , 'SUBSET OF' , 'SUBSET OF'
            'target_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL2251' , 'CHEMBL2276' , 'CHEMBL2276' , 'CHEMBL2281' , 'CHEMBL2349' , 'CHEMBL2401' , 'CHEMBL2469' , '
            # CHEMBL2469' , 'CHEMBL2496' , 'CHEMBL2508'
        }
    }
