# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'class_level': 'NUMERIC',
            # EXAMPLES:
            # '0' , '1' , '1' , '1' , '1' , '1' , '2' , '1' , '1' , '1'
            'definition': 'TEXT',
            # EXAMPLES:
            # 'Root of the ChEMBL protein family classification' , 'Biological molecules that possess catalytic activity
            # . They may occur naturally or be synthetically created. Enzymes are usually proteins, however CATALYTIC RN
            # A and CATALYTIC DNA molecules have also been identified. [MESH:D004798]' , 'Antigens on surfaces of cells,
            #  including infectious or foreign cells or viruses. They are usually protein-containing groups on cell memb
            # ranes or walls and may be isolated. [MESH:D000954]' , 'Endogenous substances, usually proteins, which are 
            # effective in the initiation, stimulation, or termination of the genetic transcription process. [MESH:D0141
            # 57]' , 'A carboxypeptidase that is specific for proteins that contain two ALANINE residues on their C-term
            # inal. Enzymes in this class play an important role in bacterial CELL WALL biosynthesis. [MESH:D046929]' , 
            # 'A carboxypeptidase that is specific for proteins that contain two ALANINE residues on their C-terminal. E
            # nzymes in this class play an important role in bacterial CELL WALL biosynthesis. [MESH:D046929]' , 'A subc
            # lass of EXOPEPTIDASES that act on the free N terminus end of a polypeptide liberating a single amino acid 
            # residue. EC 3.4.11. [MESH:D000626]' , 'A thermostable extracellular metalloendopeptidase containing four c
            # alcium ions. (Enzyme Nomenclature, 1992) 3.4.24.27. [MESH:D013820]' , 'A ZINC-dependent membrane-bound ami
            # nopeptidase that catalyzes the N-terminal peptide cleavage of GLUTAMATE (and to a lesser extent ASPARTATE)
            # . The enzyme appears to play a role in the catabolic pathway of the RENIN-ANGIOTENSIN SYSTEM. [MESH:D04338
            # 4]' , 'A serine endopeptidase secreted by the pancreas as its zymogen, CHYMOTRYPSINOGEN and carried in the
            #  pancreatic juice to the duodenum where it is activated by TRYPSIN. It selectively cleaves aromatic amino 
            # acids on the carboxyl side. [MESH:D002918]'
            'parent_id': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '1' , '0' , '0' , '0' , '1'
            'pref_name': 'TEXT',
            # EXAMPLES:
            # 'Protein class' , 'Enzyme' , 'Adhesion' , 'Secreted protein' , 'Structural protein' , 'Other nuclear prote
            # in' , 'Kinase' , 'Other membrane protein' , 'Other cytosolic protein' , 'Surface antigen'
            'protein_class_desc': 'TEXT',
            # EXAMPLES:
            # 'protein class' , 'enzyme' , 'adhesion' , 'secreted' , 'structural' , 'nuclear other' , 'enzyme  kinase' ,
            #  'membrane other' , 'cytosolic other' , 'surface antigen'
            'protein_class_id': 'NUMERIC',
            # EXAMPLES:
            # '0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9'
            'short_name': 'TEXT',
            # EXAMPLES:
            # 'Protein class' , 'Enzyme' , 'Adhesion' , 'Secreted' , 'Structural' , 'Nuclear other' , 'Kinase' , 'Membra
            # ne other' , 'Cytosolic other' , 'Surface antigen'
            'sort_order': 'NUMERIC',
            # EXAMPLES:
            # '1' , '6' , '9' , '10' , '14' , '13' , '12' , '11' , '2' , '5'
        }
    }
