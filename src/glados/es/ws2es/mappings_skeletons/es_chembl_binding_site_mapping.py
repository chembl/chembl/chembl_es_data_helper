# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'site_components': 
            {
                'properties': 
                {
                    'component_id': 'NUMERIC',
                    # EXAMPLES:
                    # '16859' , '16950' , '3361' , '4709' , '4251' , '4588' , '938' , '3608' , '2432' , '16284'
                    'domain': 
                    {
                        'properties': 
                        {
                            'domain_id': 'NUMERIC',
                            # EXAMPLES:
                            # '2951' , '2681' , '2891' , '2683' , '3093' , '2683' , '2627' , '4003' , '4377' , '2627'
                            'domain_name': 'TEXT',
                            # EXAMPLES:
                            # 'Ion_trans' , 'p450' , 'Peptidase_M10' , 'Pkinase' , 'DSPc' , 'Pkinase' , '7tm_1' , 'Pkina
                            # se_Tyr' , 'Beta-lactamase2' , '7tm_1'
                            'domain_type': 'TEXT',
                            # EXAMPLES:
                            # 'Pfam-A' , 'Pfam-A' , 'Pfam-A' , 'Pfam-A' , 'Pfam-A' , 'Pfam-A' , 'Pfam-A' , 'Pfam-A' , 'P
                            # fam-A' , 'Pfam-A'
                            'source_domain_id': 'TEXT',
                            # EXAMPLES:
                            # 'PF00520' , 'PF00067' , 'PF00413' , 'PF00069' , 'PF00782' , 'PF00069' , 'PF00001' , 'PF077
                            # 14' , 'PF13354' , 'PF00001'
                        }
                    },
                    'sitecomp_id': 'NUMERIC',
                    # EXAMPLES:
                    # '3302' , '3320' , '3317' , '3362' , '3371' , '3366' , '3331' , '3332' , '3378' , '3354'
                }
            },
            'site_id': 'NUMERIC',
            # EXAMPLES:
            # '3287' , '3288' , '3289' , '3290' , '3291' , '3292' , '3293' , '3294' , '3295' , '3296'
            'site_name': 'TEXT',
            # EXAMPLES:
            # 'Transient receptor potential cation channel subfamily M member 7, Ion_trans domain' , 'Steroid 17-alpha-h
            # ydroxylase/17,20 lyase, p450 domain' , 'Matrix metalloproteinase-24, Peptidase_M10 domain' , 'G protein-co
            # upled receptor kinase 6, Pkinase domain' , 'Protein tyrosine phosphatase type IVA 1, DSPc domain' , 'PAS d
            # omain-containing serine/threonine-protein kinase, Pkinase domain' , 'Adenosine A3 receptor, 7tm_1 domain' 
            # , 'Serine/threonine-protein kinase receptor R3, Pkinase_Tyr domain' , 'Beta-lactamase, Beta-lactamase2 dom
            # ain' , 'G-protein coupled receptor 6, 7tm_1 domain'
        }
    }
