# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL172137' , 'CHEMBL1721373' , 'CHEMBL1721379' , 'CHEMBL1721380' , 'CHEMBL1721386' , 'CHEMBL1721399' ,
            #  'CHEMBL17214' , 'CHEMBL172140' , 'CHEMBL1721408' , 'CHEMBL1721415'
            'chembl_id_number': 'NUMERIC',
            # EXAMPLES:
            # '172137' , '1721373' , '1721379' , '1721380' , '1721386' , '1721399' , '17214' , '172140' , '1721408' , '1
            # 721415'
            'entity_type': 'TEXT',
            # EXAMPLES:
            # 'COMPOUND' , 'COMPOUND' , 'COMPOUND' , 'COMPOUND' , 'COMPOUND' , 'COMPOUND' , 'COMPOUND' , 'COMPOUND' , 'C
            # OMPOUND' , 'COMPOUND'
            'last_active': 'NUMERIC',
            # EXAMPLES:
            # '35' , '35' , '35' , '35' , '35' , '35' , '35' , '35' , '35' , '35'
            'resource_url': 'TEXT',
            # EXAMPLES:
            # '/chembl/api/data/molecule/CHEMBL172137' , '/chembl/api/data/molecule/CHEMBL1721373' , '/chembl/api/data/m
            # olecule/CHEMBL1721379' , '/chembl/api/data/molecule/CHEMBL1721380' , '/chembl/api/data/molecule/CHEMBL1721
            # 386' , '/chembl/api/data/molecule/CHEMBL1721399' , '/chembl/api/data/molecule/CHEMBL17214' , '/chembl/api/
            # data/molecule/CHEMBL172140' , '/chembl/api/data/molecule/CHEMBL1721408' , '/chembl/api/data/molecule/CHEMB
            # L1721415'
            'status': 'TEXT',
            # EXAMPLES:
            # 'ACTIVE' , 'ACTIVE' , 'ACTIVE' , 'ACTIVE' , 'ACTIVE' , 'ACTIVE' , 'ACTIVE' , 'ACTIVE' , 'ACTIVE' , 'ACTIVE
            # '
        }
    }
