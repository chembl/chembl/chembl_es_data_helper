# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            '_metadata': 
            {
                'properties': 
                {
                    'compound_records': 
                    {
                        'properties': 
                        {
                        }
                    },
                    'drug': 
                    {
                        'properties': 
                        {
                            'drug_data': 
                            {
                                'properties': 
                                {
                                    'atc_classification': 
                                    {
                                        'properties': 
                                        {
                                        }
                                    },
                                    'biotherapeutic': 
                                    {
                                        'properties': 
                                        {
                                            'biocomponents': 
                                            {
                                                'properties': 
                                                {
                                                }
                                            },
                                        }
                                    },
                                    'molecule_properties': 
                                    {
                                        'properties': 
                                        {
                                        }
                                    },
                                    'molecule_structures': 
                                    {
                                        'properties': 
                                        {
                                        }
                                    },
                                    'molecule_synonyms': 
                                    {
                                        'properties': 
                                        {
                                        }
                                    },
                                }
                            },
                        }
                    },
                    'es_completion': 'TEXT',
                    # EXAMPLES:
                    # '{'input': 'CHEMBL67701', 'weight': 10}' , '{'input': 'CHEMBL69986', 'weight': 10}' , '{'input': '
                    # CHEMBL306425', 'weight': 10}' , '{'input': 'CHEMBL302579', 'weight': 10}' , '{'input': 'CHEMBL3073
                    # 18', 'weight': 10}' , '{'input': 'CHEMBL312301', 'weight': 10}' , '{'input': 'CHEMBL73215', 'weigh
                    # t': 10}' , '{'input': 'CHEMBL73696', 'weight': 10}' , '{'input': 'CHEMBL73153', 'weight': 10}' , '
                    # {'input': 'CHEMBL73034', 'weight': 10}'
                    'related_targets': 
                    {
                        'properties': 
                        {
                        }
                    },
                }
            },
            'atc_classifications': 'TEXT',
            # EXAMPLES:
            # 'N07XX10' , 'D09AA07' , 'N06AX09' , 'N06AX28' , 'R06AX28' , 'M01CC02' , 'L01DB06' , 'V04CX07' , 'N03AX03' 
            # , 'J01DB02'
            'availability_type': 'NUMERIC',
            # EXAMPLES:
            # '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1'
            'biotherapeutic': 
            {
                'properties': 
                {
                    'biocomponents': 
                    {
                        'properties': 
                        {
                            'component_id': 'NUMERIC',
                            # EXAMPLES:
                            # '20641' , '20632' , '20533' , '21656' , '21482' , '21510' , '20785' , '21553' , '20895' , 
                            # '20878'
                            'component_type': 'TEXT',
                            # EXAMPLES:
                            # 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'Nucleic Acid' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , '
                            # PROTEIN' , 'PROTEIN' , 'PROTEIN'
                            'description': 'TEXT',
                            # EXAMPLES:
                            # 'Sequence' , 'Sequence' , 'Sequence' , 'Light Chain' , 'Sequence' , 'Light Chain' , 'Heavy
                            #  Chain' , 'Sequence' , 'Sequence' , 'Sequence'
                            'organism': 'TEXT',
                            # EXAMPLES:
                            # 'Mus musculus' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo
                            #  sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens'
                            'sequence': 'TEXT',
                            # EXAMPLES:
                            # 'APKLNYSAFA' , 'IMDQVPFSV' , 'KFWAWH' , 'MPKINSFNYNDPVNDRTILYIKPGGCQEFYKSFNIMKNIWIIPERNVIG
                            # TTPQDFHPPTSLKNGDSSYYDPNYLQSDEEKDRFLKIVTKIFNRINNNLSGGILLEELSKANPYLGNDNTPDNQFHIGDASAVEIKFSNG
                            # SQHILLPNVIIMGAEPDLFETNSSNISLRNNYMPSNHGFGSIAIVTFSPEYSFRFNDNSINEFIQDPALTLMHELIHSLHGLYGAKGITT
                            # TCIITQQQNPLITNRKGINIEEFLTFGGNDLNIITVAQYNDIYTNLLNDYRKIASKLSKVQVSNPQLNPYKDIFQEKYGLDKDASGIYSV
                            # NINKFDDILKKLYSFTEFDLATKFQVKCRETYIGQYKYFKLSNLLNDSIYNISEGYNINNLKVNFRGQNANLNPRIIKPITGRGLVKKII
                            # RFCKNIVSVKGIRKSICIEINNGELFFVASENSYNDDNINTPKEIDDTVTSNNNYENDLDQVILNFNSESAPGLSDEKLNLTIQNDAYIP
                            # KYDSNGTSDIEQHDVNELNVFFYLDAQKVPEGENNVNLTSSIDTALLEQPKIYTFFSSEFINNVNKPVQAALFVSWIQQVLVDFTTEANQ
                            # KSTVDKIADISIVVPYIGLALNIGNEAQKGNFKDALELLGAGILLEFEPELLIPTILVFTIKSFLGSSDNKNKVIKAINNALKERDEKWK
                            # EVYSFIVSNWMTKINTQFNKRKEQMYQALQNQVNAIKTIIESKYNSYTLEEKNELTNKYDIKQIENELNQKVSIAMNNIDRFLTESSISY
                            # LMKLINEVKINKLREYDENVKTYLLNYIIQHGSILGESQQELNSMVTDTLNNSIPFKLSSYTDDKILISYFYNDKLSEVNISVLNMRYKN
                            # DKYVDTSGYDSNININGDVYKYPTNKNQFGINKFFKRIKSSSQNDYIIYDNKYKNFSISFWVRIPNYDNKIVNVNNEYTIINCMRDNNSG
                            # WKVSLNHNEIIWTLQDNAGINQKLAFNYGNANGISDYINKWIFVTITNDRLGDSKLYINGNLIDQKSILNLGNIHVSDNILFKIVNCSYT
                            # RYIGIRYFNIFDKELDETEIQTLYSNEPNTNILKDFWGNYLLYDKEYYLLNVLKPNNFIDRRKDSTLSINNIRSTILLANRLYSGIKVKI
                            # QRVNNSSTNDNLVRKNDQVYINFVASKTHLFPLYADTATTNKEKTIKISSSGNRFNQVVVMNSVGNNCTMNFKNNNGNNIGLLGFKADTV
                            # VASTWYYTHMRDHTNSNGCFWNFISEEHGWQEK' , 'HGSPVDICTAKPRDIPMNPMCIYRSPEKKATEDEGSEQKIPEATNRRVWELS
                            # KANSRFATTFYQHLADSKNDNDNIFLSPLSISTAFAMTKLGACNDTLQQLMEVFKFDTISEKTSDQIHFFFAKLNCRLYRKANKSSKLVS
                            # ANRLFGDKSLTFNETYQDISELVYGAKLQPLDFKENAEQSRAAINKWVSNKTEGRITDVIPSEAINELTVLVLVNTIYFKGLWKSKFSPE
                            # NTRKELFYKADGESCSASMMYQEGKFRYRRVAEGTQVLELPFKGDDITMVLILPKPEKSLAKVEKELTPEVLQEWLDELEEMMLVVHMPR
                            # FRIEDGFSLKEQLQDMGLVDLFSPEKSKLPGIVAEGRDDLYVSDAFHKAFLEVNEEGSEAAASTAVVIAGRSLNPNRVTFKANRPFLVFI
                            # REVPLNTIIFMGRVANPCVK' , 'DAHKSEVAHRFKDLGEENFKALVLIAFAQYLQQCPFEDHVKLVNEVTEFAKTCVADESAENCDKS
                            # LHTLFGDKLCTVATLRETYGEMADCCAKQEPERNECFLQHKDDNPNLPRLVRPEVDVMCTAFHDNEETFLKKYLYEIARRHPYFYAPELL
                            # FFAKRYKAAFTECCQAADKAACLLPKLDELRDEGKASSAKQRLKCASLQKFGERAFKAWAVARLSQRFPKAEFAEVSKLVTDLTKVHTEC
                            # CHGDLLECADDRADLAKYICENQDSISSKLKECCEKPLLEKSHCIAEVENDEMPADLPSLAADFVESKDVCKNYAEAKDVFLGMFLYEYA
                            # RRHPDYSVVLLLRLAKTYETTLEKCCAAADPHECYAKVFDEFKPLVEEPQNLIKQNCELFEQLGEYKFQNALLVRYTKKVPQVSTPTLVE
                            # VSRNLGKVGSKCCKHPEAKRMPCAEDYLSVVLNQLCVLHEKTPVSDRVTKCCTESLVNRRPCFSALEVDETYVPKEFNAETFTFHADICT
                            # LSEKERQIKKQTALVELVKHKPKATKEQLKAVMDDFAAFVEKCCKADDKETCFAEEGKKLVAASQAALGL' , 'QLQQSGTVLARPGAS
                            # VKMSCKASGYSFTRYWMHWIKQRPGQGLEWIGAIYPGNSDTSYNQKFEGKAKLTAVTSASTAYMELSSLTHEDSAVYYCSRDYGYYFDFW
                            # GQGTTLTVSSASTKGPSVFPLAPSSKSTSGGTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPAVLQSSGLYSLSSVVTVPSSSLGTQT
                            # YICNVNHKPSNTKVDKRVEPPKSCDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHN
                            # AKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSRDELTKNQVSLTCLVKGFYPSD
                            # IAVEWESNGQPENNYKTTPPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPGK' , 'SYSMEHFRWGKPVG
                            # KKRRPVKVYPDAGEDQSAEAFPLEF' , 'FVNQHLCGSHLVEALYLVCGERGFFYTDKT' , 'APPRLICDSRVLERYLLEAKEAENI
                            # TTGCNETCSLNENITVPDTKVNFYAWKRMEVGQQAVEVWQGLALLSEAVLRGQALLVNSSQVNETLQLHVDKAVSGLRSLTTLLRALGAQ
                            # KEAISPPDAASAAPLRTITADTFRKLFRVYSNFLRGKLKLYTGEACRTGD'
                            'tax_id': 'NUMERIC',
                            # EXAMPLES:
                            # '10090' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606'
                        }
                    },
                    'description': 'TEXT',
                    # EXAMPLES:
                    # 'PFDEDQHTQIAKV' , 'BRADYKININ' , 'SPYSSDTTPA' , 'PROCTOLIN' , 'RIP' , 'PROLYLPROLYLTHREONYLPROLINA
                    # MIDE(PPTPNH2)' , 'ABARELIX' , 'YADAIFTNSYRKVLGQ CYCLO (ESAK) KLLQDIMSR' , 'ALMPLYACI' , 'DRIGAQSGL
                    # GCNSFRY'
                    'helm_notation': 'TEXT',
                    # EXAMPLES:
                    # 'PEPTIDE1{[X1042].P.[X2324]}$$$$' , 'PEPTIDE1{[Cbz_V].P.[X2368]}$$$$' , 'PEPTIDE1{[dF].C.Y.W.K.T.[
                    # Pen].[am]}$PEPTIDE1,PEPTIDE1,7:R3-2:R3$$$' , 'PEPTIDE1{[ac].H.W.A.V.G.H.L.M.[am]}$$$$' , 'PEPTIDE1
                    # {[X578].[Nva].L.[am]}$$$$' , 'PEPTIDE1{[X1033].[Nva].L.[am]}$$$$' , 'PEPTIDE1{A}|PEPTIDE2{A.A.E}|P
                    # EPTIDE3{[dA].L.D}$PEPTIDE2,PEPTIDE1,3:R3-1:R1|PEPTIDE3,PEPTIDE2,3:R3-1:R1$$$' , 'PEPTIDE1{[Orn].[a
                    # m]}|PEPTIDE2{Y.[dD].F}$PEPTIDE2,PEPTIDE1,3:R2-1:R1|PEPTIDE2,PEPTIDE1,2:R3-1:R3$$$' , 'PEPTIDE1{[Et
                    # CO_F].A.A.L.[am]}$$$$' , 'PEPTIDE1{C.[dE].A.[dA].L.[dD].A}$$$$'
                    'molecule_chembl_id': 'TEXT',
                    # EXAMPLES:
                    # 'CHEMBL305259' , 'CHEMBL306203' , 'CHEMBL308471' , 'CHEMBL274874' , 'CHEMBL417735' , 'CHEMBL68003'
                    #  , 'CHEMBL63351' , 'CHEMBL68177' , 'CHEMBL65081' , 'CHEMBL304313'
                }
            },
            'black_box_warning': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'chebi_par_id': 'NUMERIC',
            # EXAMPLES:
            # '8695' , '18152' , '8277' , '73288' , '32914' , '75305' , '8416' , '75902' , '10037' , '29200'
            'chemical_probe': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'chirality': 'NUMERIC',
            # EXAMPLES:
            # '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1'
            'cross_references': 
            {
                'properties': 
                {
                    'xref_id': 'TEXT',
                    # EXAMPLES:
                    # 'cefotaxime%20sodium' , 'lenalidomide' , 'clavulanate%20potassium' , 'droperidol' , 'emtricitabine
                    # ' , 'losartan%20potassium' , 'sodium%20nitrite' , 'bumetanide' , 'ambrisentan' , 'metyrapone'
                    'xref_name': 'TEXT',
                    # EXAMPLES:
                    # 'cefotaxime sodium' , 'lenalidomide' , 'clavulanate potassium' , 'droperidol' , 'emtricitabine' , 
                    # 'losartan potassium' , 'sodium nitrite' , 'bumetanide' , 'ambrisentan' , 'metyrapone'
                    'xref_src': 'TEXT',
                    # EXAMPLES:
                    # 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyMed' , 'DailyM
                    # ed' , 'DailyMed' , 'DailyMed'
                }
            },
            'dosed_ingredient': 'BOOLEAN',
            # EXAMPLES:
            # 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False'
            'first_approval': 'NUMERIC',
            # EXAMPLES:
            # '2021' , '2013' , '2001' , '1990' , '1981' , '1951' , '1961' , '1996' , '1942' , '1980'
            'first_in_class': 'NUMERIC',
            # EXAMPLES:
            # '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1'
            'helm_notation': 'TEXT',
            # EXAMPLES:
            # 'PEPTIDE1{[X1042].P.[X2324]}$$$$' , 'PEPTIDE1{[Cbz_V].P.[X2368]}$$$$' , 'PEPTIDE1{[dF].C.Y.W.K.T.[Pen].[am
            # ]}$PEPTIDE1,PEPTIDE1,7:R3-2:R3$$$' , 'PEPTIDE1{[ac].H.W.A.V.G.H.L.M.[am]}$$$$' , 'PEPTIDE1{[X578].[Nva].L.
            # [am]}$$$$' , 'PEPTIDE1{[X1033].[Nva].L.[am]}$$$$' , 'PEPTIDE1{A}|PEPTIDE2{A.A.E}|PEPTIDE3{[dA].L.D}$PEPTID
            # E2,PEPTIDE1,3:R3-1:R1|PEPTIDE3,PEPTIDE2,3:R3-1:R1$$$' , 'PEPTIDE1{[Orn].[am]}|PEPTIDE2{Y.[dD].F}$PEPTIDE2,
            # PEPTIDE1,3:R2-1:R1|PEPTIDE2,PEPTIDE1,2:R3-1:R3$$$' , 'PEPTIDE1{[EtCO_F].A.A.L.[am]}$$$$' , 'PEPTIDE1{C.[dE
            # ].A.[dA].L.[dD].A}$$$$'
            'indication_class': 'TEXT',
            # EXAMPLES:
            # 'Cardiac Depressant (anti-arrhythmic)' , 'Anti-Infective, Topical; Pharmaceutic Aid (preservative)' , 'Ant
            # idepressant' , 'Antagonist (to histamine-H2 receptors)' , 'Anti-Arthritic; Anti-Inflammatory,Immunomodulat
            # or; Anti-Arthritic; Anti-Inflammatory' , 'Antipsoriatic; Inhibitor (5-lipoxygenase)' , 'Antineoplastic' , 
            # 'Antiviral' , 'Enzyme Inhibitor (aldose reductase)' , 'Antibacterial'
            'inorganic_flag': 'NUMERIC',
            # EXAMPLES:
            # '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1'
            'max_phase': 'NUMERIC',
            # EXAMPLES:
            # '2.0' , '2.0' , '3.0' , '-1.0' , '2.0' , '2.0' , '4.0' , '4.0' , '-1.0' , '4.0'
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL67701' , 'CHEMBL69986' , 'CHEMBL306425' , 'CHEMBL302579' , 'CHEMBL307318' , 'CHEMBL312301' , 'CHEMB
            # L73215' , 'CHEMBL73696' , 'CHEMBL73153' , 'CHEMBL73034'
            'molecule_hierarchy': 
            {
                'properties': 
                {
                    'active_chembl_id': 'TEXT',
                    # EXAMPLES:
                    # 'CHEMBL67701' , 'CHEMBL69986' , 'CHEMBL306425' , 'CHEMBL302579' , 'CHEMBL307318' , 'CHEMBL312301' 
                    # , 'CHEMBL73215' , 'CHEMBL73696' , 'CHEMBL73153' , 'CHEMBL73034'
                    'molecule_chembl_id': 'TEXT',
                    # EXAMPLES:
                    # 'CHEMBL67701' , 'CHEMBL69986' , 'CHEMBL306425' , 'CHEMBL302579' , 'CHEMBL307318' , 'CHEMBL312301' 
                    # , 'CHEMBL73215' , 'CHEMBL73696' , 'CHEMBL73153' , 'CHEMBL73034'
                    'parent_chembl_id': 'TEXT',
                    # EXAMPLES:
                    # 'CHEMBL67701' , 'CHEMBL69986' , 'CHEMBL306425' , 'CHEMBL302579' , 'CHEMBL307318' , 'CHEMBL312301' 
                    # , 'CHEMBL73215' , 'CHEMBL73696' , 'CHEMBL73153' , 'CHEMBL73034'
                }
            },
            'molecule_properties': 
            {
                'properties': 
                {
                    'alogp': 'NUMERIC',
                    # EXAMPLES:
                    # '4.69' , '4.17' , '2.45' , '8.32' , '7.04' , '7.90' , '2.53' , '1.51' , '2.78' , '3.94'
                    'aromatic_rings': 'NUMERIC',
                    # EXAMPLES:
                    # '3' , '3' , '3' , '5' , '4' , '5' , '2' , '3' , '3' , '4'
                    'cx_logd': 'NUMERIC',
                    # EXAMPLES:
                    # '3.79' , '4.76' , '1.25' , '8.31' , '6.80' , '8.18' , '-0.39' , '0.36' , '2.01' , '-1.23'
                    'cx_logp': 'NUMERIC',
                    # EXAMPLES:
                    # '6.11' , '4.76' , '1.25' , '7.99' , '6.47' , '7.85' , '-0.39' , '0.36' , '2.01' , '3.47'
                    'cx_most_apka': 'NUMERIC',
                    # EXAMPLES:
                    # '4.93' , '10.56' , '10.86' , '2.91' , '2.91' , '2.91' , '3.40' , '13.44' , '13.09' , '3.90'
                    'cx_most_bpka': 'NUMERIC',
                    # EXAMPLES:
                    # '1.81' , '4.44' , '4.45' , '4.44' , '10.36' , '1.46' , '1.41' , '10.12' , '9.32' , '6.58'
                    'full_molformula': 'TEXT',
                    # EXAMPLES:
                    # 'C24H17NO4S2' , 'C32H32O11' , 'C13H12ClN5O' , 'C42H45FN4O7S' , 'C37H43FN4O7S' , 'C41H44FN3O8S2' , 
                    # 'C28H32N4O5' , 'C9H6ClN5' , 'C13H14ClN5' , 'C30H26Cl2N6O4'
                    'full_mwt': 'NUMERIC',
                    # EXAMPLES:
                    # '447.54' , '592.60' , '289.73' , '768.91' , '706.84' , '789.95' , '504.59' , '219.63' , '275.74' ,
                    #  '605.48'
                    'hba': 'NUMERIC',
                    # EXAMPLES:
                    # '6' , '11' , '5' , '9' , '9' , '10' , '5' , '5' , '5' , '8'
                    'hba_lipinski': 'NUMERIC',
                    # EXAMPLES:
                    # '5' , '11' , '6' , '11' , '11' , '11' , '9' , '5' , '5' , '10'
                    'hbd': 'NUMERIC',
                    # EXAMPLES:
                    # '2' , '1' , '1' , '2' , '2' , '1' , '2' , '1' , '0' , '2'
                    'hbd_lipinski': 'NUMERIC',
                    # EXAMPLES:
                    # '2' , '1' , '1' , '2' , '2' , '1' , '2' , '2' , '0' , '2'
                    'heavy_atoms': 'NUMERIC',
                    # EXAMPLES:
                    # '31' , '43' , '20' , '55' , '50' , '55' , '37' , '15' , '19' , '42'
                    'molecular_species': 'TEXT',
                    # EXAMPLES:
                    # 'ACID' , 'NEUTRAL' , 'NEUTRAL' , 'ACID' , 'ACID' , 'ACID' , 'ZWITTERION' , 'NEUTRAL' , 'NEUTRAL' ,
                    #  'BASE'
                    'mw_freebase': 'NUMERIC',
                    # EXAMPLES:
                    # '447.54' , '592.60' , '289.73' , '768.91' , '706.84' , '789.95' , '504.59' , '219.63' , '275.74' ,
                    #  '605.48'
                    'mw_monoisotopic': 'NUMERIC',
                    # EXAMPLES:
                    # '447.0599' , '592.1945' , '289.0730' , '768.2993' , '706.2836' , '789.2554' , '504.2373' , '219.03
                    # 12' , '275.0938' , '604.1393'
                    'np_likeness_score': 'NUMERIC',
                    # EXAMPLES:
                    # '-0.87' , '0.41' , '-2.25' , '-1.15' , '-1.20' , '-1.00' , '-0.25' , '-1.93' , '-2.15' , '-0.54'
                    'num_lipinski_ro5_violations': 'NUMERIC',
                    # EXAMPLES:
                    # '0' , '2' , '0' , '3' , '3' , '3' , '1' , '0' , '0' , '1'
                    'num_ro5_violations': 'NUMERIC',
                    # EXAMPLES:
                    # '0' , '2' , '0' , '2' , '2' , '2' , '1' , '0' , '0' , '1'
                    'psa': 'NUMERIC',
                    # EXAMPLES:
                    # '75.63' , '128.21' , '72.18' , '145.69' , '145.69' , '150.73' , '110.26' , '69.10' , '46.32' , '12
                    # 4.60'
                    'qed_weighted': 'NUMERIC',
                    # EXAMPLES:
                    # '0.32' , '0.24' , '0.79' , '0.10' , '0.09' , '0.10' , '0.61' , '0.62' , '0.74' , '0.15'
                    'ro3_pass': 'TEXT',
                    # EXAMPLES:
                    # 'N' , 'N' , 'N' , 'N' , 'N' , 'N' , 'N' , 'N' , 'N' , 'N'
                    'rtb': 'NUMERIC',
                    # EXAMPLES:
                    # '6' , '12' , '2' , '16' , '17' , '16' , '5' , '0' , '3' , '11'
                }
            },
            'molecule_structures': 
            {
                'properties': 
                {
                    'canonical_smiles': 'TEXT',
                    # EXAMPLES:
                    # 'O=C1NC(=S)S/C1=C\c1ccc(COc2ccc(C(=O)c3ccccc3)c(O)c2)cc1' , 'COC(=O)CCCOc1cc(CC2=C(c3ccc4c(c3)OCO4
                    # )C(=O)OC2(O)c2ccc(OC)cc2)cc(OC)c1OC' , 'CCc1nnc2c(NC(C)=O)nc3cc(Cl)ccc3n12' , 'CCCc1nc(CC)c(C(=O)O
                    # Cc2ccccc2NC(=O)c2ccccc2)n1Cc1ccc(-c2ccccc2S(=O)(=O)NC(=O)OCCC(C)C)cc1F' , 'CCCCCOC(=O)NS(=O)(=O)c1
                    # ccccc1-c1ccc(Cn2c(CCC)nc(CC)c2C(=O)OCCC(=O)Nc2ccccc2)c(F)c1' , 'CCCc1nc(CC)c(C(=O)OCc2ccccc2S(=O)(
                    # =O)c2ccccc2)n1Cc1ccc(-c2ccccc2S(=O)(=O)NC(=O)OCCC(C)C)cc1F' , 'O=C(O)[C@@H]1[C@@H](CC2CCNCC2)C(=O)
                    # N1C(=O)N1CCN(C(=O)c2ccc(-c3ccccc3)cc2)CC1' , 'Nc1nc2ccc(Cl)cc2n2cnnc12' , 'CCN(CC)c1nc2cc(Cl)ccc2n
                    # 2cnnc12' , 'O=C1c2cccc3nc(Cl)cc(c23)C(=O)N1CCNCCCCNCCN1C(=O)c2cccc3nc(Cl)cc(c23)C1=O'
                    'standard_inchi': 'TEXT',
                    # EXAMPLES:
                    # 'InChI=1S/C24H17NO4S2/c26-20-13-18(10-11-19(20)22(27)17-4-2-1-3-5-17)29-14-16-8-6-15(7-9-16)12-21-
                    # 23(28)25-24(30)31-21/h1-13,26H,14H2,(H,25,28,30)/b21-12-' , 'InChI=1S/C32H32O11/c1-36-22-10-8-21(9
                    # -11-22)32(35)23(29(31(34)43-32)20-7-12-24-25(17-20)42-18-41-24)14-19-15-26(37-2)30(39-4)27(16-19)4
                    # 0-13-5-6-28(33)38-3/h7-12,15-17,35H,5-6,13-14,18H2,1-4H3' , 'InChI=1S/C13H12ClN5O/c1-3-11-17-18-13
                    # -12(15-7(2)20)16-9-6-8(14)4-5-10(9)19(11)13/h4-6H,3H2,1-2H3,(H,15,16,20)' , 'InChI=1S/C42H45FN4O7S
                    # /c1-5-14-38-44-35(6-2)39(41(49)54-27-32-17-10-12-19-36(32)45-40(48)29-15-8-7-9-16-29)47(38)26-31-2
                    # 2-21-30(25-34(31)43)33-18-11-13-20-37(33)55(51,52)46-42(50)53-24-23-28(3)4/h7-13,15-22,25,28H,5-6,
                    # 14,23-24,26-27H2,1-4H3,(H,45,48)(H,46,50)' , 'InChI=1S/C37H43FN4O7S/c1-4-7-13-22-49-37(45)41-50(46
                    # ,47)32-18-12-11-17-29(32)26-19-20-27(30(38)24-26)25-42-33(14-5-2)40-31(6-3)35(42)36(44)48-23-21-34
                    # (43)39-28-15-9-8-10-16-28/h8-12,15-20,24H,4-7,13-14,21-23,25H2,1-3H3,(H,39,43)(H,41,45)' , 'InChI=
                    # 1S/C41H44FN3O8S2/c1-5-14-38-43-35(6-2)39(40(46)53-27-31-15-10-12-19-36(31)54(48,49)32-16-8-7-9-17-
                    # 32)45(38)26-30-22-21-29(25-34(30)42)33-18-11-13-20-37(33)55(50,51)44-41(47)52-24-23-28(3)4/h7-13,1
                    # 5-22,25,28H,5-6,14,23-24,26-27H2,1-4H3,(H,44,47)' , 'InChI=1S/C28H32N4O5/c33-25(22-8-6-21(7-9-22)2
                    # 0-4-2-1-3-5-20)30-14-16-31(17-15-30)28(37)32-24(27(35)36)23(26(32)34)18-19-10-12-29-13-11-19/h1-9,
                    # 19,23-24,29H,10-18H2,(H,35,36)/t23-,24+/m1/s1' , 'InChI=1S/C9H6ClN5/c10-5-1-2-6-7(3-5)15-4-12-14-9
                    # (15)8(11)13-6/h1-4H,(H2,11,13)' , 'InChI=1S/C13H14ClN5/c1-3-18(4-2)12-13-17-15-8-19(13)11-6-5-9(14
                    # )7-10(11)16-12/h5-8H,3-4H2,1-2H3' , 'InChI=1S/C30H26Cl2N6O4/c31-23-15-19-25-17(5-3-7-21(25)35-23)2
                    # 7(39)37(29(19)41)13-11-33-9-1-2-10-34-12-14-38-28(40)18-6-4-8-22-26(18)20(30(38)42)16-24(32)36-22/
                    # h3-8,15-16,33-34H,1-2,9-14H2'
                    'standard_inchi_key': 'TEXT',
                    # EXAMPLES:
                    # 'MTDSTNVHLGWBKL-MTJSOVHGSA-N' , 'KNCYVPWSDRUANJ-UHFFFAOYSA-N' , 'HWXCHJLCJWXJNR-UHFFFAOYSA-N' , 'K
                    # BOMALZRTSYMGE-UHFFFAOYSA-N' , 'ODRDBMOWIAECPK-UHFFFAOYSA-N' , 'GYKVTFQQIXNTGJ-UHFFFAOYSA-N' , 'ZPT
                    # YDNODWOJNDO-RPWUZVMVSA-N' , 'WDPOQVSYBCXTBT-UHFFFAOYSA-N' , 'MWNBMNAPXAGFDF-UHFFFAOYSA-N' , 'XAPSD
                    # QONRKOJGY-UHFFFAOYSA-N'
                }
            },
            'molecule_synonyms': 
            {
                'properties': 
                {
                    'molecule_synonym': 'TEXT',
                    # EXAMPLES:
                    # 'JWH-030' , 'BMS-354326' , 'Spiropiperidine' , '2-Phenyl-Ethenesulfonic Acid Dimethylamide' , '2-P
                    # hthalimidoglutaramic Acid' , 'Ro-25-6981' , '3,4-Dichloro-Phenylsemicarbazone' , 'Makaluvamine I' 
                    # , 'Zoloperona' , 'CI-1031'
                    'syn_type': 'TEXT',
                    # EXAMPLES:
                    # 'RESEARCH_CODE' , 'RESEARCH_CODE' , 'OTHER' , 'OTHER' , 'OTHER' , 'RESEARCH_CODE' , 'OTHER' , 'OTH
                    # ER' , 'INN_SPANISH' , 'RESEARCH_CODE'
                    'synonyms': 'TEXT',
                    # EXAMPLES:
                    # 'JWH-030' , 'BMS-354326' , 'Spiropiperidine' , '2-Phenyl-Ethenesulfonic Acid Dimethylamide' , '2-P
                    # hthalimidoglutaramic Acid (In Rats, Rabbits, Dogs, Mice, And Guinea Pigs)' , 'Ro-25-6981' , '3,4-D
                    # ichloro-Phenylsemicarbazone' , 'Makaluvamine I' , 'ZOLOPERONA' , 'CI-1031'
                }
            },
            'molecule_type': 'TEXT',
            # EXAMPLES:
            # 'Small molecule' , 'Small molecule' , 'Small molecule' , 'Small molecule' , 'Small molecule' , 'Small mole
            # cule' , 'Small molecule' , 'Small molecule' , 'Small molecule' , 'Small molecule'
            'natural_product': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'oral': 'BOOLEAN',
            # EXAMPLES:
            # 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False'
            'orphan': 'NUMERIC',
            # EXAMPLES:
            # '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1'
            'parenteral': 'BOOLEAN',
            # EXAMPLES:
            # 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False'
            'polymer_flag': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'pref_name': 'TEXT',
            # EXAMPLES:
            # 'ISOCAFFIENE' , '7-Amino-4-Methyl-Coumarin Platinum (II) Complex' , 'N-[(1E)-1-(5-methylpyrazin-2-yl)ethyl
            # idene]-3-azabicyclo[3.2.2]nonane-3-carbohydrazonothioic acid.chloro Cu(II)complex' , 'Auranofin analogue' 
            # , 'Auranofin analogue' , 'SPIROPIPERIDINE' , '2-PHTHALIMIDOGLUTARAMIC ACID' , 'Platinum complex' , 'Cis-[P
            # tCl2(gamma-pic)2] complex' , 'Fe (II) complex'
            'prodrug': 'NUMERIC',
            # EXAMPLES:
            # '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1' , '-1'
            'structure_type': 'TEXT',
            # EXAMPLES:
            # 'MOL' , 'MOL' , 'MOL' , 'MOL' , 'MOL' , 'MOL' , 'MOL' , 'MOL' , 'MOL' , 'MOL'
            'therapeutic_flag': 'BOOLEAN',
            # EXAMPLES:
            # 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False'
            'topical': 'BOOLEAN',
            # EXAMPLES:
            # 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False'
            'usan_stem': 'TEXT',
            # EXAMPLES:
            # '-perone' , '-xaban' , '-imod' , '-ium' , '-isomide' , '-ium' , '-' , '-tidine' , '-bulin' , '-imod'
            'usan_stem_definition': 'TEXT',
            # EXAMPLES:
            # 'antianxiety agents/neuroleptics (4'-fluoro-4-piperidinobutyrophenone derivatives)' , 'antithrombotics, bl
            # ood coagulation factor XA inhibitors' , 'immunomodulators' , 'quaternary ammonium derivatives: neuromuscul
            # ar blocking agents' , 'antiarrhythmics (disopyramide derivatives)' , 'quaternary ammonium derivatives' , '
            # -' , 'H2-receptor antagonists (cimetidine type)' , 'antineoplastics (mitotic inhibitors; tubulin binders)'
            #  , 'immunomodulators'
            'usan_substem': 'TEXT',
            # EXAMPLES:
            # '-perone' , '-xaban' , '-imod' , '-ium (-curium)' , '-isomide' , '-ium' , '-' , '-tidine' , '-bulin' , '-i
            # mod'
            'usan_year': 'NUMERIC',
            # EXAMPLES:
            # '2004' , '2007' , '2004' , '1990' , '1976' , '1986' , '2010' , '2000' , '1996' , '1992'
            'withdrawn_flag': 'BOOLEAN',
            # EXAMPLES:
            # 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False'
        }
    }
