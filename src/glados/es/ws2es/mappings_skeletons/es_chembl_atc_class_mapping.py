# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'level1': 'TEXT',
            # EXAMPLES:
            # 'A' , 'A' , 'A' , 'A' , 'A' , 'A' , 'A' , 'A' , 'A' , 'A'
            'level1_description': 'TEXT',
            # EXAMPLES:
            # 'ALIMENTARY TRACT AND METABOLISM' , 'ALIMENTARY TRACT AND METABOLISM' , 'ALIMENTARY TRACT AND METABOLISM' 
            # , 'ALIMENTARY TRACT AND METABOLISM' , 'ALIMENTARY TRACT AND METABOLISM' , 'ALIMENTARY TRACT AND METABOLISM
            # ' , 'ALIMENTARY TRACT AND METABOLISM' , 'ALIMENTARY TRACT AND METABOLISM' , 'ALIMENTARY TRACT AND METABOLI
            # SM' , 'ALIMENTARY TRACT AND METABOLISM'
            'level2': 'TEXT',
            # EXAMPLES:
            # 'A02' , 'A02' , 'A02' , 'A02' , 'A02' , 'A02' , 'A02' , 'A02' , 'A02' , 'A02'
            'level2_description': 'TEXT',
            # EXAMPLES:
            # 'DRUGS FOR ACID RELATED DISORDERS' , 'DRUGS FOR ACID RELATED DISORDERS' , 'DRUGS FOR ACID RELATED DISORDER
            # S' , 'DRUGS FOR ACID RELATED DISORDERS' , 'DRUGS FOR ACID RELATED DISORDERS' , 'DRUGS FOR ACID RELATED DIS
            # ORDERS' , 'DRUGS FOR ACID RELATED DISORDERS' , 'DRUGS FOR ACID RELATED DISORDERS' , 'DRUGS FOR ACID RELATE
            # D DISORDERS' , 'DRUGS FOR ACID RELATED DISORDERS'
            'level3': 'TEXT',
            # EXAMPLES:
            # 'A02A' , 'A02A' , 'A02A' , 'A02B' , 'A02B' , 'A02B' , 'A02B' , 'A02B' , 'A02B' , 'A02B'
            'level3_description': 'TEXT',
            # EXAMPLES:
            # 'ANTACIDS' , 'ANTACIDS' , 'ANTACIDS' , 'DRUGS FOR PEPTIC ULCER AND GASTRO-OESOPHAGEAL REFLUX DISEASE (GORD
            # )' , 'DRUGS FOR PEPTIC ULCER AND GASTRO-OESOPHAGEAL REFLUX DISEASE (GORD)' , 'DRUGS FOR PEPTIC ULCER AND G
            # ASTRO-OESOPHAGEAL REFLUX DISEASE (GORD)' , 'DRUGS FOR PEPTIC ULCER AND GASTRO-OESOPHAGEAL REFLUX DISEASE (
            # GORD)' , 'DRUGS FOR PEPTIC ULCER AND GASTRO-OESOPHAGEAL REFLUX DISEASE (GORD)' , 'DRUGS FOR PEPTIC ULCER A
            # ND GASTRO-OESOPHAGEAL REFLUX DISEASE (GORD)' , 'DRUGS FOR PEPTIC ULCER AND GASTRO-OESOPHAGEAL REFLUX DISEA
            # SE (GORD)'
            'level4': 'TEXT',
            # EXAMPLES:
            # 'A02AD' , 'A02AF' , 'A02AF' , 'A02BA' , 'A02BA' , 'A02BA' , 'A02BA' , 'A02BA' , 'A02BA' , 'A02BA'
            'level4_description': 'TEXT',
            # EXAMPLES:
            # 'Combinations and complexes of aluminium, calcium and magnesium compounds' , 'Antacids with antiflatulents
            # ' , 'Antacids with antiflatulents' , 'H2-receptor antagonists' , 'H2-receptor antagonists' , 'H2-receptor 
            # antagonists' , 'H2-receptor antagonists' , 'H2-receptor antagonists' , 'H2-receptor antagonists' , 'H2-rec
            # eptor antagonists'
            'level5': 'TEXT',
            # EXAMPLES:
            # 'A02AD05' , 'A02AF01' , 'A02AF02' , 'A02BA01' , 'A02BA02' , 'A02BA03' , 'A02BA04' , 'A02BA05' , 'A02BA06' 
            # , 'A02BA07'
            'who_name': 'TEXT',
            # EXAMPLES:
            # 'almasilate' , 'magaldrate and antiflatulents' , 'ordinary salt combinations and antiflatulents' , 'cimeti
            # dine' , 'ranitidine' , 'famotidine' , 'nizatidine' , 'niperotidine' , 'roxatidine' , 'ranitidine bismuth c
            # itrate'
        }
    }
