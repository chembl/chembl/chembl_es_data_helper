# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            '_metadata': 
            {
                'properties': 
                {
                    'es_completion': 'TEXT',
                    # EXAMPLES:
                    # '{'input': 'Histamine N-methyltransferase', 'weight': 100}' , '{'input': 'SOS1-KRAS', 'weight': 10
                    # 0}' , '{'input': 'Angiopoietin-1', 'weight': 100}' , '{'input': 'Heme oxygenase 1', 'weight': 100}
                    # ' , '{'input': 'Fibroblast growth factor receptor 4', 'weight': 100}' , '{'input': 'Integrin alpha
                    # 4/beta7 complex', 'weight': 100}' , '{'input': 'C-C chemokine receptor type 5', 'weight': 100}' , 
                    # '{'input': 'Vitamin D3 receptor A', 'weight': 100}' , '{'input': 'DCAF16-BRD4', 'weight': 100}' , 
                    # '{'input': 'Serine palmitoyltransferase', 'weight': 100}'
                    'related_compounds': 
                    {
                        'properties': 
                        {
                        }
                    },
                }
            },
            'cross_references': 
            {
                'properties': 
                {
                    'xref_id': 'TEXT',
                    # EXAMPLES:
                    # 'CPX-678' , 'CPX-632' , 'CPX-5986' , 'CPX-3035' , 'CPX-3116' , 'CPX-1817' , 'CPX-2954' , 'CPX-5842
                    # ' , 'CPX-5787' , 'CPX-5790'
                    'xref_name': 'TEXT',
                    # EXAMPLES:
                    # 'RXRalpha-LXRbeta nuclear hormone receptor complex' , 'RXRalpha-LXRalpha nuclear hormone receptor 
                    # complex' , 'Phosphatidylinositol 3-kinase complex class IB, p110gamma/p101' , 'Integrin alphav-bet
                    # a3 complex' , 'Integrin alphaIIb-beta3 complex' , 'Integrin alpha10-beta1 complex' , 'GABA-A recep
                    # tor, alpha4-beta3-delta' , 'AMPK complex, alpha1-beta1-gamma3 variant' , 'AMPK complex, alpha2-bet
                    # a1-gamma1 variant' , 'AMPK complex, alpha2-beta2-gamma1 variant'
                    'xref_src': 'TEXT',
                    # EXAMPLES:
                    # 'ComplexPortal' , 'ComplexPortal' , 'ComplexPortal' , 'ComplexPortal' , 'ComplexPortal' , 'Complex
                    # Portal' , 'ComplexPortal' , 'ComplexPortal' , 'ComplexPortal' , 'ComplexPortal'
                }
            },
            'organism': 'TEXT',
            # EXAMPLES:
            # 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Mus musculus' , 'Homo sapiens' , 'Mac
            # aca mulatta' , 'Danio rerio' , 'Homo sapiens' , 'Sphingomonas paucimobilis'
            'pref_name': 'TEXT',
            # EXAMPLES:
            # 'Histamine N-methyltransferase' , 'SOS1-KRAS' , 'Angiopoietin-1' , 'Heme oxygenase 1' , 'Fibroblast growth
            #  factor receptor 4' , 'Integrin alpha4/beta7 complex' , 'C-C chemokine receptor type 5' , 'Vitamin D3 rece
            # ptor A' , 'DCAF16-BRD4' , 'Serine palmitoyltransferase'
            'species_group_flag': 'BOOLEAN',
            # EXAMPLES:
            # 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False' , 'False'
            'target_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL2190' , 'CHEMBL5465393' , 'CHEMBL3217395' , 'CHEMBL2823' , 'CHEMBL3839' , 'CHEMBL5465395' , 'CHEMBL
            # 3217397' , 'CHEMBL3217399' , 'CHEMBL5465397' , 'CHEMBL3217400'
            'target_components': 
            {
                'properties': 
                {
                    'accession': 'TEXT',
                    # EXAMPLES:
                    # 'P50135' , 'Q07889' , 'Q15389' , 'P09601' , 'Q03142' , 'P48169' , 'P61813' , 'Q9PTN2' , 'O60885' ,
                    #  'Q93UV0'
                    'component_description': 'TEXT',
                    # EXAMPLES:
                    # 'Histamine N-methyltransferase' , 'Son of sevenless homolog 1' , 'Angiopoietin-1' , 'Heme oxygenas
                    # e 1' , 'Fibroblast growth factor receptor 4' , 'Gamma-aminobutyric acid receptor subunit alpha-4' 
                    # , 'C-C chemokine receptor type 5' , 'Vitamin D3 receptor A' , 'Bromodomain-containing protein 4' ,
                    #  'Serine palmitoyltransferase'
                    'component_id': 'NUMERIC',
                    # EXAMPLES:
                    # '531' , '5880' , '8497' , '1153' , '2156' , '813' , '8499' , '8501' , '5501' , '8502'
                    'component_type': 'TEXT',
                    # EXAMPLES:
                    # 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'P
                    # ROTEIN' , 'PROTEIN'
                    'relationship': 'TEXT',
                    # EXAMPLES:
                    # 'SINGLE PROTEIN' , 'PROTEIN SUBUNIT' , 'SINGLE PROTEIN' , 'SINGLE PROTEIN' , 'SINGLE PROTEIN' , 'P
                    # ROTEIN SUBUNIT' , 'SINGLE PROTEIN' , 'SINGLE PROTEIN' , 'INTERACTING PROTEIN' , 'SINGLE PROTEIN'
                    'target_component_synonyms': 
                    {
                        'properties': 
                        {
                            'component_synonym': 'TEXT',
                            # EXAMPLES:
                            # '2.1.1.8' , 'Son of sevenless homolog 1' , 'ANG-1' , '1.14.14.18' , '2.7.10.1' , 'GABA(A) 
                            # receptor subunit alpha-4' , 'C-C chemokine receptor type 5' , '1,25-dihydroxyvitamin D3 re
                            # ceptor A' , 'BRD4' , '2.3.1.50'
                            'syn_type': 'TEXT',
                            # EXAMPLES:
                            # 'EC_NUMBER' , 'UNIPROT' , 'UNIPROT' , 'EC_NUMBER' , 'EC_NUMBER' , 'UNIPROT' , 'UNIPROT' , 
                            # 'UNIPROT' , 'GENE_SYMBOL' , 'EC_NUMBER'
                        }
                    },
                    'target_component_xrefs': 
                    {
                        'properties': 
                        {
                            'xref_id': 'TEXT',
                            # EXAMPLES:
                            # 'P50135' , 'Q07889' , 'Q15389' , 'P09601' , 'Q03142' , 'P48169' , 'P61813' , 'Q9PTN2' , 'O
                            # 60885' , 'Q93UV0'
                            'xref_name': 'TEXT',
                            # EXAMPLES:
                            # 'nucleoplasm' , 'cytoplasm' , 'extracellular region' , 'extracellular space' , 'endosome' 
                            # , 'plasma membrane' , 'cytoplasm' , 'nucleus' , 'chromatin' , 'cytoplasm'
                            'xref_src_db': 'TEXT',
                            # EXAMPLES:
                            # 'AlphaFoldDB' , 'AlphaFoldDB' , 'AlphaFoldDB' , 'AlphaFoldDB' , 'AlphaFoldDB' , 'AlphaFold
                            # DB' , 'AlphaFoldDB' , 'AlphaFoldDB' , 'AlphaFoldDB' , 'AlphaFoldDB'
                        }
                    }
                }
            },
            'target_type': 'TEXT',
            # EXAMPLES:
            # 'SINGLE PROTEIN' , 'PROTEIN COMPLEX' , 'SINGLE PROTEIN' , 'SINGLE PROTEIN' , 'SINGLE PROTEIN' , 'PROTEIN C
            # OMPLEX' , 'SINGLE PROTEIN' , 'SINGLE PROTEIN' , 'PROTEIN-PROTEIN INTERACTION' , 'SINGLE PROTEIN'
            'tax_id': 'NUMERIC',
            # EXAMPLES:
            # '9606' , '9606' , '9606' , '9606' , '10090' , '9606' , '9544' , '7955' , '9606' , '13689'
        }
    }
