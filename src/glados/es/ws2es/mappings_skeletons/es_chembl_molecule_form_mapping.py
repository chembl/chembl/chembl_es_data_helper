# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'is_parent': 'BOOLEAN',
            # EXAMPLES:
            # 'True' , 'True' , 'True' , 'True' , 'True' , 'True' , 'True' , 'True' , 'True' , 'True'
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL441018' , 'CHEMBL10309' , 'CHEMBL10251' , 'CHEMBL280216' , 'CHEMBL415969' , 'CHEMBL10057' , 'CHEMBL
            # 10083' , 'CHEMBL277938' , 'CHEMBL276635' , 'CHEMBL274930'
            'parent_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL441018' , 'CHEMBL10309' , 'CHEMBL10251' , 'CHEMBL280216' , 'CHEMBL415969' , 'CHEMBL10057' , 'CHEMBL
            # 10083' , 'CHEMBL277938' , 'CHEMBL276635' , 'CHEMBL274930'
        }
    }
