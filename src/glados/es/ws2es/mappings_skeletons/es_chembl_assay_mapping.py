# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            '_metadata': 
            {
                'properties': 
                {
                    'es_completion': 'TEXT',
                    # EXAMPLES:
                    # '{'input': 'CHEMBL1930581', 'weight': 10}' , '{'input': 'Malme-3M', 'weight': 10}' , '{'input': 'M
                    # DA-MB-435', 'weight': 10}' , '{'input': 'CHEMBL1930585', 'weight': 10}' , '{'input': 'CHEMBL193782
                    # 1', 'weight': 10}' , '{'input': 'SK-MEL-5', 'weight': 10}' , '{'input': 'Huh-7', 'weight': 10}' , 
                    # '{'input': 'OVCAR-4', 'weight': 10}' , '{'input': 'CHEMBL1937829', 'weight': 10}' , '{'input': 'HE
                    # K293', 'weight': 10}'
                }
            },
            'aidx': 'TEXT',
            # EXAMPLES:
            # 'CLD0' , 'CLD0' , 'CLD0' , 'CLD0' , 'CLD0' , 'CLD0' , 'CLD0' , 'CLD0' , 'CLD0' , 'CLD0'
            'assay_category': 'TEXT',
            # EXAMPLES:
            # 'confirmatory' , 'confirmatory' , 'confirmatory' , 'confirmatory' , 'confirmatory' , 'confirmatory' , 'con
            # firmatory' , 'confirmatory' , 'confirmatory' , 'confirmatory'
            'assay_cell_type': 'TEXT',
            # EXAMPLES:
            # 'Malme-3M' , 'MDA-MB-435' , 'SK-MEL-5' , 'Huh-7' , 'OVCAR-4' , 'HEK293' , 'OVCAR-8' , 'ACHN' , 'TK-10' , '
            # A549'
            'assay_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL1930581' , 'CHEMBL1921098' , 'CHEMBL1921100' , 'CHEMBL1930585' , 'CHEMBL1937821' , 'CHEMBL1921103' 
            # , 'CHEMBL1930592' , 'CHEMBL1921108' , 'CHEMBL1937829' , 'CHEMBL1937830'
            'assay_classifications': 
            {
                'properties': 
                {
                    'assay_class_id': 'NUMERIC',
                    # EXAMPLES:
                    # '341' , '341' , '115' , '115' , '296' , '388' , '388' , '388' , '32' , '405'
                    'class_type': 'TEXT',
                    # EXAMPLES:
                    # 'In vivo efficacy' , 'In vivo efficacy' , 'In vivo efficacy' , 'In vivo efficacy' , 'In vivo effic
                    # acy' , 'In vivo efficacy' , 'In vivo efficacy' , 'In vivo efficacy' , 'In vivo efficacy' , 'In viv
                    # o efficacy'
                    'l1': 'TEXT',
                    # EXAMPLES:
                    # 'NERVOUS SYSTEM' , 'NERVOUS SYSTEM' , 'ANTINEOPLASTIC AND IMMUNOMODULATING AGENTS' , 'ANTINEOPLAST
                    # IC AND IMMUNOMODULATING AGENTS' , 'MUSCULO-SKELETAL SYSTEM' , 'NERVOUS SYSTEM' , 'NERVOUS SYSTEM' 
                    # , 'NERVOUS SYSTEM' , 'ALIMENTARY TRACT AND METABOLISM' , 'NERVOUS SYSTEM'
                    'l2': 'TEXT',
                    # EXAMPLES:
                    # 'Anti-Epileptic Activity' , 'Anti-Epileptic Activity' , 'Neoplasm Oncology Models' , 'Neoplasm Onc
                    # ology Models' , 'Anti-Inflammatory Activity' , 'Effects on Behavior and Muscle Coordination' , 'Ef
                    # fects on Behavior and Muscle Coordination' , 'Effects on Behavior and Muscle Coordination' , 'Gene
                    # tically Diabetic Animals' , 'Learning and Memory'
                    'l3': 'TEXT',
                    # EXAMPLES:
                    # 'General Anti-Convulsant Activity' , 'General Anti-Convulsant Activity' , 'Neoplasms' , 'Neoplasms
                    # ' , 'General Anti-Inflammatory Models' , 'Open Field Test' , 'Open Field Test' , 'Open Field Test'
                    #  , 'Spontaneously Diabetic Mice: KK Mouse, KK-AY Mouse, NOD Mouse, Obese Hyperglycemic Mice, Diabe
                    # tic db/db Mice, Diabetes Obesity Syndrome in CBA/Ca Mice, Wellesley Mouse' , 'General Learning and
                    #  Memory Models'
                    'source': 'TEXT',
                    # EXAMPLES:
                    # 'phenotype' , 'phenotype' , 'phenotype' , 'phenotype' , 'phenotype' , 'Hock_2016' , 'Hock_2016' , 
                    # 'Hock_2016' , 'Vogel_2008' , 'phenotype'
                }
            },
            'assay_group': 'TEXT',
            # EXAMPLES:
            # 'Dog liver microsomal stability by Concept Life Sciences - v1' , 'CYP3A4 inhibition by Concept Life Scienc
            # es - v1' , 'iPSC pneumocytes cytotoxicity, no P-gp, by Icahn School of Medicine at Mount Sinai - v1' , 'Ve
            # ro 76 MERS-CoV cytotoxicity, no P-gp, by Utah State University - v1' , 'MDCK-MDR1 permeability by Bienta -
            #  v1' , 'MDCK-MDR1 permeability (Verapamil) by Concept Life Sciences - v1' , 'Human plasma protein binding 
            # by Concept Life Sciences - v1' , 'Fetal calf serum protein binding by Concept Life Sciences - v1' , 'BZD ,
            # SafetyScreen by Eurofins Discovery - v1' , 'CB2 ,SafetyScreen by Eurofins Discovery - v1'
            'assay_organism': 'TEXT',
            # EXAMPLES:
            # 'Saccharomyces cerevisiae' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Rattus norvegicus' , 'Hom
            # o sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Rattus norvegicus' , 'Homo sapiens'
            'assay_parameters': 
            {
                'properties': 
                {
                    'active': 'NUMERIC',
                    # EXAMPLES:
                    # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
                    'comments': 'TEXT',
                    # EXAMPLES:
                    # '+/- 0.0000033 (Mca-RPKPVE-Nval-WRK(Dnp)-NH2; ES002 from R&D Systems)' , '+/- 0.0000033 (Mca-RPKPV
                    # E-Nval-WRK(Dnp)-NH2; ES002 from R&D Systems)' , '+/- 0.0000033 (Mca-RPKPVE-Nval-WRK(Dnp)-NH2; ES00
                    # 2 from R&D Systems)' , '+/- 0.0000033 (Mca-RPKPVE-Nval-WRK(Dnp)-NH2; ES002 from R&D Systems)' , 'I
                    # s the measured interaction considered due to direct binding to target?' , 'Is the measured interac
                    # tion considered due to direct binding to target?' , 'Is the measured interaction considered due to
                    #  direct binding to target?' , 'Is the measured interaction considered due to direct binding to tar
                    # get?' , 'Is the measured interaction considered due to direct binding to target?' , 'Is the measur
                    # ed interaction considered due to direct binding to target?'
                    'relation': 'TEXT',
                    # EXAMPLES:
                    # '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '='
                    'standard_relation': 'TEXT',
                    # EXAMPLES:
                    # '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '='
                    'standard_text_value': 'TEXT',
                    # EXAMPLES:
                    # 'Oral' , 'Intraperitoneal' , 'Intraperitoneal' , 'Intraperitoneal' , 'Subcutaneous' , 'Oral' , 'Or
                    # al' , 'Intravenous' , 'Oral' , 'Oral'
                    'standard_type': 'TEXT',
                    # EXAMPLES:
                    # 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE'
                    'standard_type_fixed': 'NUMERIC',
                    # EXAMPLES:
                    # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
                    'standard_units': 'TEXT',
                    # EXAMPLES:
                    # 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'm
                    # g.kg-1' , 'mg.kg-1'
                    'standard_value': 'NUMERIC',
                    # EXAMPLES:
                    # '10.0' , '100.0' , '100.0' , '300.0' , '300.0' , '30.0' , '5.0' , '10.0' , '1.0' , '1.0'
                    'text_value': 'TEXT',
                    # EXAMPLES:
                    # 'Oral' , 'Intraperitoneal' , 'Intraperitoneal' , 'Intraperitoneal' , 'Subcutaneous' , 'Oral' , 'Or
                    # al' , 'Intravenous' , 'Oral' , 'Oral'
                    'type': 'TEXT',
                    # EXAMPLES:
                    # 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE'
                    'units': 'TEXT',
                    # EXAMPLES:
                    # 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg'
                    'value': 'NUMERIC',
                    # EXAMPLES:
                    # '10.0' , '100.0' , '100.0' , '300.0' , '300.0' , '30.0' , '5.0' , '10.0' , '1.0' , '1.0'
                }
            },
            'assay_strain': 'TEXT',
            # EXAMPLES:
            # 'ATCC 24657' , 'Sprague-Dawley' , 'Sprague-Dawley' , '10-1' , '10-2' , 'C57BL/6' , '10-2' , '09-2' , 'albi
            # no CF-1' , '10-4'
            'assay_subcellular_fraction': 'TEXT',
            # EXAMPLES:
            # 'Microsome' , 'Microsome' , 'Microsome' , 'Microsome' , 'Lysosome' , 'Mitochondria' , 'Microsome' , 'Micro
            # some' , 'Membrane' , 'Microsome'
            'assay_tax_id': 'NUMERIC',
            # EXAMPLES:
            # '4932' , '9606' , '9606' , '9606' , '10116' , '9606' , '9606' , '9606' , '10116' , '9606'
            'assay_test_type': 'TEXT',
            # EXAMPLES:
            # 'In vivo' , 'In vitro' , 'In vivo' , 'In vitro' , 'In vivo' , 'In vivo' , 'In vivo' , 'In vivo' , 'In vitr
            # o' , 'In vitro'
            'assay_tissue': 'TEXT',
            # EXAMPLES:
            # 'Blood/Plasma' , 'Plasma' , 'Plasma' , 'Plasma' , 'Liver' , 'Plasma' , 'Plasma' , 'Plasma' , 'Plasma' , 'P
            # lasma'
            'assay_type': 'TEXT',
            # EXAMPLES:
            # 'F' , 'F' , 'F' , 'A' , 'A' , 'F' , 'A' , 'F' , 'A' , 'F'
            'assay_type_description': 'TEXT',
            # EXAMPLES:
            # 'Functional' , 'Functional' , 'Functional' , 'ADME' , 'ADME' , 'Functional' , 'ADME' , 'Functional' , 'ADM
            # E' , 'Functional'
            'bao_format': 'TEXT',
            # EXAMPLES:
            # 'BAO_0000218' , 'BAO_0000219' , 'BAO_0000219' , 'BAO_0000357' , 'BAO_0000225' , 'BAO_0000219' , 'BAO_00002
            # 19' , 'BAO_0000219' , 'BAO_0000218' , 'BAO_0000219'
            'bao_label': 'TEXT',
            # EXAMPLES:
            # 'organism-based format' , 'cell-based format' , 'cell-based format' , 'single protein format' , 'nucleic a
            # cid format' , 'cell-based format' , 'cell-based format' , 'cell-based format' , 'organism-based format' , 
            # 'cell-based format'
            'cell_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL3307507' , 'CHEMBL3307686' , 'CHEMBL3307744' , 'CHEMBL3307515' , 'CHEMBL3307517' , 'CHEMBL3307715' 
            # , 'CHEMBL3307975' , 'CHEMBL3307633' , 'CHEMBL3308453' , 'CHEMBL3307651'
            'confidence_description': 'TEXT',
            # EXAMPLES:
            # 'Target assigned is non-molecular' , 'Target assigned is non-molecular' , 'Target assigned is non-molecula
            # r' , 'Direct single protein target assigned' , 'Target assigned is molecular non-protein target' , 'Target
            #  assigned is non-molecular' , 'Target assigned is non-molecular' , 'Target assigned is non-molecular' , 'T
            # arget assigned is non-molecular' , 'Direct single protein target assigned'
            'confidence_score': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '9' , '3' , '1' , '1' , '1' , '1' , '9'
            'description': 'TEXT',
            # EXAMPLES:
            # 'Fungistatic activity against Saccharomyces cerevisiae ATCC 24657 assessed as cell proliferation compound 
            # after 24 to 48 hrs by spectrophotometric bioassay' , 'Anticancer activity against human MALME-3M cells aft
            # er 48 hrs by SRB assay' , 'Anticancer activity against human MDA-MB-435 cells after 48 hrs by SRB assay' ,
            #  'Inhibition of human recombinant CYP2C19' , 'Ratio of drug level in blood to plasma in Sprague-Dawley rat
            # ' , 'Anticancer activity against human SK-MEL-5 cells after 48 hrs by SRB assay' , 'Cytotoxicity against h
            # uman HuH7 cells infected with HCV1b after 3 days by MTS assay' , 'Anticancer activity against human OVCAR4
            #  cells after 48 hrs by SRB assay' , 'Plasma clearance in bile duct cannulated Sprague-Dawley rat' , 'Antag
            # onist activity at human recombinant FPR1 in expressed in HEK293 cells assessed as inhibition of FMLP-stimu
            # lated intracellular calcium mobilisation at 30 uM after 1 hr by FLIPR assay'
            'document_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL1926617' , 'CHEMBL1914423' , 'CHEMBL1914423' , 'CHEMBL1926617' , 'CHEMBL1932964' , 'CHEMBL1914423' 
            # , 'CHEMBL1926658' , 'CHEMBL1914423' , 'CHEMBL1932964' , 'CHEMBL1932964'
            'relationship_description': 'TEXT',
            # EXAMPLES:
            # 'Non-molecular target assigned' , 'Non-molecular target assigned' , 'Non-molecular target assigned' , 'Dir
            # ect protein target assigned' , 'Molecular target other than protein assigned' , 'Non-molecular target assi
            # gned' , 'Non-molecular target assigned' , 'Non-molecular target assigned' , 'Non-molecular target assigned
            # ' , 'Direct protein target assigned'
            'relationship_type': 'TEXT',
            # EXAMPLES:
            # 'N' , 'N' , 'N' , 'D' , 'M' , 'N' , 'N' , 'N' , 'N' , 'D'
            'src_assay_id': 'NUMERIC',
            # EXAMPLES:
            # '420053' , '410520' , '410522' , '420057' , '424466' , '410525' , '422376' , '410530' , '424474' , '424475
            # '
            'src_id': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
            'target_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL361' , 'CHEMBL614021' , 'CHEMBL614697' , 'CHEMBL3622' , 'CHEMBL345' , 'CHEMBL614922' , 'CHEMBL61403
            # 9' , 'CHEMBL614051' , 'CHEMBL613652' , 'CHEMBL3359'
            'tissue_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL3987301' , 'CHEMBL3559721' , 'CHEMBL3559721' , 'CHEMBL3559721' , 'CHEMBL3559723' , 'CHEMBL3559721' 
            # , 'CHEMBL3559721' , 'CHEMBL3559721' , 'CHEMBL3559721' , 'CHEMBL3559721'
            'variant_sequence': 
            {
                'properties': 
                {
                    'accession': 'TEXT',
                    # EXAMPLES:
                    # 'Q07817' , 'P32238' , 'Q07817' , 'P00519' , 'P00519' , 'P00533' , 'P42336' , 'P10721' , 'P00519' ,
                    #  'P00519'
                    'isoform': 'NUMERIC',
                    # EXAMPLES:
                    # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
                    'mutation': 'TEXT',
                    # EXAMPLES:
                    # 'Y101H' , 'M195A' , 'G196A' , 'Q252H' , 'T315I' , 'G719C' , 'UNDEFINED MUTATION' , 'H1047L' , 'V55
                    # 9D,T670I' , 'Q252H'
                    'organism': 'TEXT',
                    # EXAMPLES:
                    # 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens
                    # ' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens'
                    'sequence': 'TEXT',
                    # EXAMPLES:
                    # 'MSQSNRELVVDFLSYKLSQKGYSWSQFSDVEENRTEAPEGTESEMETPSAINGNPSWHLADSPAVNGATGHSSSLDAREVIPMAAVKQALREAGDEF
                    # ELRHRRAFSDLTSQLHITPGTAYQSFEQVVNELFRDGVNWGRIVAFFSFGGALCVESVDKEMQVLVSRIAAWMATYLNDHLEPWIQENGGWDTFVELY
                    # GNNAAAESRKGQERFNRWFLTGMTVAGVVLLGSLFSRK' , 'MDVVDSLLVNGSNITPPCELGLENETLFCLDQPRPSKEWQPAVQILLYSLIFLLS
                    # VLGNTLVITVLIRNKRMRTVTNIFLLSLAVSDLMLCLFCMPFNLIPNLLKDFIFGSAVCKTTTYFMGTSVSVSTFNLVAISLERYGAICKPLQSRVWQ
                    # TKSHALKVIAATWCLSFTIMTPYPIYSNLVPFTKNNNQTANACRFLLPNDVMQQSWHTFLLLILFLIPGIVMMVAYGLISLELYQGIKFEASQKKSAK
                    # ERKPSTTSSGKYEDSDGCYLQKTRPPRKLELRQLSTGSSSRANRIRSNSSAANLMAKKRVIRMLIVIVVLFFLCWMPIFSANAWRAYDTASAERRLSG
                    # TPISFILLLSYTSSCVNPIIYCFMNKRFRLGFMATFPCCPNPGPPGARGEVGEEEEGGTTGASLSRFSYSHMSASVPPQ' , 'MSQSNRELVVDFLS
                    # YKLSQKGYSWSQFSDVEENRTEAPEGTESEMETPSAINGNPSWHLADSPAVNGATGHSSSLDAREVIPMAAVKQALREAGDEFELRYRRAFSDLTSQL
                    # HITPGTAYQSFEQVVNELFRDGVNWGRIVAFFSFGGALCVESVDKEMQVLVSRIAAWMATYLNDHLEPWIQENGGWDTFVELYANNAAAESRKGQERF
                    # NRWFLTGMTVAGVVLLGSLFSRK' , 'MLEICLKLVGCKSKKGLSSSSSCYLEEALQRPVASDFEPQGLSEAARWNSKENLLAGPSENDPNLFVALY
                    # DFVASGDNTLSITKGEKLRVLGYNHNGEWCEAQTKNGQGWVPSNYITPVNSLEKHSWYHGPVSRNAAEYLLSSGINGSFLVRESESSPGQRSISLRYE
                    # GRVYHYRINTASDGKLYVSSESRFNTLAELVHHHSTVADGLITTLHYPAPKRNKPTVYGVSPNYDKWEMERTDITMKHKLGGGHYGEVYEGVWKKYSL
                    # TVAVKTLKEDTMEVEEFLKEAAVMKEIKHPNLVQLLGVCTREPPFYIITEFMTYGNLLDYLRECNRQEVNAVVLLYMATQISSAMEYLEKKNFIHRDL
                    # AARNCLVGENHLVKVADFGLSRLMTGDTYTAHAGAKFPIKWTAPESLAYNKFSIKSDVWAFGVLLWEIATYGMSPYPGIDLSQVYELLEKDYRMERPE
                    # GCPEKVYELMRACWQWNPSDRPSFAEIHQAFETMFQESSISDEVEKELGKQGVRGAVSTLLQAPELPTKTRTSRRAAEHRDTTDVPEMPHSKGQGESD
                    # PLDHEPAVSPLLPRKERGPPEGGLNEDERLLPKDKKTNLFSALIKKKKKTAPTPPKRSSSFREMDGQPERRGAGEEEGRDISNGALAFTPLDTADPAK
                    # SPKPSNGAGVPNGALRESGGSGFRSPHLWKKSSTLTSSRLATGEEEGGGSSSKRFLRSCSASCVPHGAKDTEWRSVTLPRDLQSTGRQFDSSTFGGHK
                    # SEKPALPRKRAGENRSDQVTRGTVTPPPRLVKKNEEAADEVFKDIMESSPGSSPPNLTPKPLRRQVTVAPASGLPHKEEAGKGSALGTPAAAEPVTPT
                    # SKAGSGAPGGTSKGPAEESRVRRHKHSSESPGRDKGKLSRLKPAPPPPPAASAGKAGGKPSQSPSQEAAGEAVLGAKTKATSLVDAVNSDAAKPSQPG
                    # EGLKKPVLPATPKPQSAKPSGTPISPAPVPSTLPSASSALAGDQPSSTAFIPLISTRVSLRKTRQPPERIASGAITKGVVLDSTEALCLAISRNSEQM
                    # ASHSAVLEAGKNLYTFCVSYVDSIQQMRNKFAFREAINKLENNLRELQICPATAGSGPAATQDFSKLLSSVKEISDIVQR' , 'MLEICLKLVGCKS
                    # KKGLSSSSSCYLEEALQRPVASDFEPQGLSEAARWNSKENLLAGPSENDPNLFVALYDFVASGDNTLSITKGEKLRVLGYNHNGEWCEAQTKNGQGWV
                    # PSNYITPVNSLEKHSWYHGPVSRNAAEYLLSSGINGSFLVRESESSPGQRSISLRYEGRVYHYRINTASDGKLYVSSESRFNTLAELVHHHSTVADGL
                    # ITTLHYPAPKRNKPTVYGVSPNYDKWEMERTDITMKHKLGGGQYGEVYEGVWKKYSLTVAVKTLKEDTMEVEEFLKEAAVMKEIKHPNLVQLLGVCTR
                    # EPPFYIIIEFMTYGNLLDYLRECNRQEVNAVVLLYMATQISSAMEYLEKKNFIHRDLAARNCLVGENHLVKVADFGLSRLMTGDTYTAHAGAKFPIKW
                    # TAPESLAYNKFSIKSDVWAFGVLLWEIATYGMSPYPGIDLSQVYELLEKDYRMERPEGCPEKVYELMRACWQWNPSDRPSFAEIHQAFETMFQESSIS
                    # DEVEKELGKQGVRGAVSTLLQAPELPTKTRTSRRAAEHRDTTDVPEMPHSKGQGESDPLDHEPAVSPLLPRKERGPPEGGLNEDERLLPKDKKTNLFS
                    # ALIKKKKKTAPTPPKRSSSFREMDGQPERRGAGEEEGRDISNGALAFTPLDTADPAKSPKPSNGAGVPNGALRESGGSGFRSPHLWKKSSTLTSSRLA
                    # TGEEEGGGSSSKRFLRSCSASCVPHGAKDTEWRSVTLPRDLQSTGRQFDSSTFGGHKSEKPALPRKRAGENRSDQVTRGTVTPPPRLVKKNEEAADEV
                    # FKDIMESSPGSSPPNLTPKPLRRQVTVAPASGLPHKEEAGKGSALGTPAAAEPVTPTSKAGSGAPGGTSKGPAEESRVRRHKHSSESPGRDKGKLSRL
                    # KPAPPPPPAASAGKAGGKPSQSPSQEAAGEAVLGAKTKATSLVDAVNSDAAKPSQPGEGLKKPVLPATPKPQSAKPSGTPISPAPVPSTLPSASSALA
                    # GDQPSSTAFIPLISTRVSLRKTRQPPERIASGAITKGVVLDSTEALCLAISRNSEQMASHSAVLEAGKNLYTFCVSYVDSIQQMRNKFAFREAINKLE
                    # NNLRELQICPATAGSGPAATQDFSKLLSSVKEISDIVQR' , 'MRPSGTAGAALLALLAALCPASRALEEKKVCQGTSNKLTQLGTFEDHFLSLQRM
                    # FNNCEVVLGNLEITYVQRNYDLSFLKTIQEVAGYVLIALNTVERIPLENLQIIRGNMYYENSYALAVLSNYDANKTGLKELPMRNLQEILHGAVRFSN
                    # NPALCNVESIQWRDIVSSDFLSNMSMDFQNHLGSCQKCDPSCPNGSCWGAGEENCQKLTKIICAQQCSGRCRGKSPSDCCHNQCAAGCTGPRESDCLV
                    # CRKFRDEATCKDTCPPLMLYNPTTYQMDVNPEGKYSFGATCVKKCPRNYVVTDHGSCVRACGADSYEMEEDGVRKCKKCEGPCRKVCNGIGIGEFKDS
                    # LSINATNIKHFKNCTSISGDLHILPVAFRGDSFTHTPPLDPQELDILKTVKEITGFLLIQAWPENRTDLHAFENLEIIRGRTKQHGQFSLAVVSLNIT
                    # SLGLRSLKEISDGDVIISGNKNLCYANTINWKKLFGTSGQKTKIISNRGENSCKATGQVCHALCSPEGCWGPEPRDCVSCRNVSRGRECVDKCNLLEG
                    # EPREFVENSECIQCHPECLPQAMNITCTGRGPDNCIQCAHYIDGPHCVKTCPAGVMGENNTLVWKYADAGHVCHLCHPNCTYGCTGPGLEGCPTNGPK
                    # IPSIATGMVGALLLLLVVALGIGLFMRRRHIVRKRTLRRLLQERELVEPLTPSGEAPNQALLRILKETEFKKIKVLCSGAFGTVYKGLWIPEGEKVKI
                    # PVAIKELREATSPKANKEILDEAYVMASVDNPHVCRLLGICLTSTVQLITQLMPFGCLLDYVREHKDNIGSQYLLNWCVQIAKGMNYLEDRRLVHRDL
                    # AARNVLVKTPQHVKITDFGLAKLLGAEEKEYHAEGGKVPIKWMALESILHRIYTHQSDVWSYGVTVWELMTFGSKPYDGIPASEISSILEKGERLPQP
                    # PICTIDVYMIMVKCWMIDADSRPKFRELIIEFSKMARDPQRYLVIQGDERMHLPSPTDSNFYRALMDEEDMDDVVDADEYLIPQQGFFSSPSTSRTPL
                    # LSSLSATSNNSTVACIDRNGLQSCPIKEDSFLQRYSSDPTGALTEDSIDDTFLPVPEYINQSVPKRPAGSVQNPVYHNQPLNPAPSRDPHYQDPHSTA
                    # VGNPEYLNTVQPTCVNSTFDSPAHWAQKGSHQISLDNPDYQQDFFPKEAKPNGIFKGSTAENAEYLRVAPQSSEFIGA' , '' , 'MPPRPSSGEL
                    # WGIHLMPPRILVECLLPNGMIVTLECLREATLITIKHELFKEARKYPLHQLLQDESSYIFVSVTQEAEREEFFDETRRLCDLRLFQPFLKVIEPVGNR
                    # EEKILNREIGFAIGMPVCEFDMVKDPEVQDFRRNILNVCKEAVDLRDLNSPHSRAMYVYPPNVESSPELPKHIYNKLDKGQIIVVIWVIVSPNNDKQK
                    # YTLKINHDCVPEQVIAEAIRKKTRSMLLSSEQLKLCVLEYQGKYILKVCGCDEYFLEKYPLSQYKYIRSCIMLGRMPNLMLMAKESLYSQLPMDCFTM
                    # PSYSRRISTATPYMNGETSTKSLWVINSALRIKILCATYVNVNIRDIDKIYVRTGIYHGGEPLCDNVNTQRVPCSNPRWNEWLNYDIYIPDLPRAARL
                    # CLSICSVKGRKGAKEEHCPLAWGNINLFDYTDTLVSGKMALNLWPVPHGLEDLLNPIGVTGSNPNKETPCLELEFDWFSSVVKFPDMSVIEEHANWSV
                    # SREAGFSYSHAGLSNRLARDNELRENDKEQLKAISTRDPLSEITEQEKDFLWSHRHYCVTIPEILPKLLLSVKWNSRDEVAQMYCLVKDWPPIKPEQA
                    # MELLDCNYPDPMVRGFAVRCLEKYLTDDKLSQYLIQLVQVLKYEQYLDNLLVRFLLKKALTNQRIGHFFFWHLKSEMHNKTVSQRFGLLLESYCRACG
                    # MYLKHLNRQVEAMEKLINLTDILKQEKKDETQKVQMKFLVEQMRRPDFMDALQGFLSPLNPAHQLGNLRLEECRIMSSAKRPLWLNWENPDIMSELLF
                    # QNNEIIFKNGDDLRQDMLTLQIIRIMENIWQNQGLDLRMLPYGCLSIGDCVGLIEVVRNSHTIMQIQCKGGLKGALQFNSHTLHQWLKDKNKGEIYDA
                    # AIDLFTRSCAGYCVATFILGIGDRHNSNIMVKDDGQLFHIDFGHFLDHKKKKFGYKRERVPFVLTQDFLIVISKGAQECTKTREFERFQEMCYKAYLA
                    # IRQHANLFINLFSMMLGSGMPELQSFDDIAYIRKTLALDKTEQEALEYFMKQMNDALHGGWTTKMDWIFHTIKQHALN' , 'MRGARGAWDFLCVLL
                    # LLLRVQTGSSQPSVSPGEPSPPSIHPGKSDLIVRVGDEIRLLCTDPGFVKWTFEILDETNENKQNEWITEKAEATNTGKYTCTNKHGLSNSIYVFVRD
                    # PAKLFLVDRSLYGKEDNDTLVRCPLTDPEVTNYSLKGCQGKPLPKDLRFIPDPKAGIMIKSVKRAYHRLCLHCSVDQEGKSVLSEKFILKVRPAFKAV
                    # PVVSVSKASYLLREGEEFTVTCTIKDVSSSVYSTWKRENSQTKLQEKYNSWHHGDFNYERQATLTISSARVNDSGVFMCYANNTFGSANVTTTLEVVD
                    # KGFINIFPMINTTVFVNDGENVDLIVEYEAFPKPEHQQWIYMNRTFTDKWEDYPKSENESNIRYVSELHLTRLKGTEGGTYTFLVSNSDVNAAIAFNV
                    # YVNTKPEILTYDRLVNGMLQCVAAGFPEPTIDWYFCPGTEQRCSASVLPVDVQTLNSSGPPFGKLVVQSSIDSSAFKHNGTVECKAYNDVGKTSAYFN
                    # FAFKGNNKEQIHPHTLFTPLLIGFVIVAGMMCIIVMILTYKYLQKPMYEVQWKDVEEINGNNYVYIDPTQLPYDHKWEFPRNRLSFGKTLGAGAFGKV
                    # VEATAYGLIKSDAAMTVAVKMLKPSAHLTEREALMSELKVLSYLGNHMNIVNLLGACTIGGPTLVIIEYCCYGDLLNFLRRKRDSFICSKQEDHAEAA
                    # LYKNLLHSKESSCSDSTNEYMDMKPGVSYVVPTKADKRRSVRIGSYIERDVTPAIMEDDELALDLEDLLSFSYQVAKGMAFLASKNCIHRDLAARNIL
                    # LTHGRITKICDFGLARDIKNDSNYVVKGNARLPVKWMAPESIFNCVYTFESDVWSYGIFLWELFSLGSSPYPGMPVDSKFYKMIKEGFRMLSPEHAPA
                    # EMYDIMKTCWDADPLKRPTFKQIVQLIEKQISESTNHIYSNLANCSPNRQKPVVDHSVRINSVGSTASSSQPLLVHDDV' , 'MLEICLKLVGCKSK
                    # KGLSSSSSCYLEEALQRPVASDFEPQGLSEAARWNSKENLLAGPSENDPNLFVALYDFVASGDNTLSITKGEKLRVLGYNHNGEWCEAQTKNGQGWVP
                    # SNYITPVNSLEKHSWYHGPVSRNAAEYLLSSGINGSFLVRESESSPGQRSISLRYEGRVYHYRINTASDGKLYVSSESRFNTLAELVHHHSTVADGLI
                    # TTLHYPAPKRNKPTVYGVSPNYDKWEMERTDITMKHKLGGGHYGEVYEGVWKKYSLTVAVKTLKEDTMEVEEFLKEAAVMKEIKHPNLVQLLGVCTRE
                    # PPFYIITEFMTYGNLLDYLRECNRQEVNAVVLLYMATQISSAMEYLEKKNFIHRDLAARNCLVGENHLVKVADFGLSRLMTGDTYTAHAGAKFPIKWT
                    # APESLAYNKFSIKSDVWAFGVLLWEIATYGMSPYPGIDLSQVYELLEKDYRMERPEGCPEKVYELMRACWQWNPSDRPSFAEIHQAFETMFQESSISD
                    # EVEKELGKQGVRGAVSTLLQAPELPTKTRTSRRAAEHRDTTDVPEMPHSKGQGESDPLDHEPAVSPLLPRKERGPPEGGLNEDERLLPKDKKTNLFSA
                    # LIKKKKKTAPTPPKRSSSFREMDGQPERRGAGEEEGRDISNGALAFTPLDTADPAKSPKPSNGAGVPNGALRESGGSGFRSPHLWKKSSTLTSSRLAT
                    # GEEEGGGSSSKRFLRSCSASCVPHGAKDTEWRSVTLPRDLQSTGRQFDSSTFGGHKSEKPALPRKRAGENRSDQVTRGTVTPPPRLVKKNEEAADEVF
                    # KDIMESSPGSSPPNLTPKPLRRQVTVAPASGLPHKEEAGKGSALGTPAAAEPVTPTSKAGSGAPGGTSKGPAEESRVRRHKHSSESPGRDKGKLSRLK
                    # PAPPPPPAASAGKAGGKPSQSPSQEAAGEAVLGAKTKATSLVDAVNSDAAKPSQPGEGLKKPVLPATPKPQSAKPSGTPISPAPVPSTLPSASSALAG
                    # DQPSSTAFIPLISTRVSLRKTRQPPERIASGAITKGVVLDSTEALCLAISRNSEQMASHSAVLEAGKNLYTFCVSYVDSIQQMRNKFAFREAINKLEN
                    # NLRELQICPATAGSGPAATQDFSKLLSSVKEISDIVQR'
                    'tax_id': 'NUMERIC',
                    # EXAMPLES:
                    # '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606'
                    'version': 'NUMERIC',
                    # EXAMPLES:
                    # '1' , '1' , '1' , '4' , '4' , '2' , '2' , '1' , '4' , '4'
                }
            }
        }
    }
