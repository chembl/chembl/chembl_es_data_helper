# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            '_metadata': 
            {
                'properties': 
                {
                    'es_completion': 'TEXT',
                    # EXAMPLES:
                    # '{'input': 'The universal template approach to drug design foresees that a polyamine can be modifi
                    # ed in such a way to recognize any neurotransmitter receptor. Thus, hybrids of polymethylene tetraa
                    # mines and philanthotoxins, exemplified by methoctramine (1) and PhTX-343 (2), respectively, were s
                    # ynthesized to produce novel inhibitors of muscular nicotinic acetylcholine receptors. Polyamines 3
                    # -25 were synthesized and their biological profiles were evaluated at frog rectus abdominis muscle 
                    # nicotinic receptors and guinea pig left atria (M(2)) and ileum longitudinal muscle (M(3)) muscarin
                    # ic acetylcholine receptors. All of the compounds, like prototypes 1 and 2, were noncompetitive ant
                    # agonists of nicotinic receptors while being, like 1, competitive antagonists at muscarinic M(2) an
                    # d M(3) receptor subtypes. Interestingly, polyamines bearing a low number of methylenes between the
                    #  nitrogen atoms, as in 3, 6, and 7, displayed a biological profile similar to that of 2: a noncomp
                    # etitive antagonism at nicotinic receptors in the 7-25 microM range while not showing any antagonis
                    # m for muscarinic receptors up to 10 microM. Increasing the number of methylenes separating these n
                    # itrogen atoms in methoctramine-related tetraamines resulted in a significant improvement in potenc
                    # y at nicotinic receptors. The most potent tetraamine was 19, bearing a 12 methylene spacer between
                    #  the nitrogen atoms, which was 12-fold and 250-fold more potent than prototypes 1 and 2, respectiv
                    # ely. Tetraamines 9-11, bearing a rather rigid spacer between the nitrogen atoms instead of the ver
                    # y flexible polymethylene chain, displayed a profile similar to that of 1 at nicotinic receptors, w
                    # hereas a significant decrease in potency was observed at muscarinic M(2) receptors. This finding m
                    # ay have relevance in understanding the mode of interaction with these receptors. Similarly, the co
                    # nstrained analogue 12 of methoctramine showed a decrease in potency at nicotinic and muscarinic M(
                    # 2) receptors, revealing that the tricyclic system, which incorporates the 2-methoxybenzylamine moi
                    # ety of 1, does not represent a good pharmacophore for activity at these sites. A most intriguing f
                    # inding was the observation that the photolabile tetraamine 22 was more potent than methoctramine a
                    # t nicotinic receptors and, what is more important, it inhibited a closed state of the receptor.', 
                    # 'weight': 60}' , '{'input': 'Philanthotoxin-433 (PhTX-433), a natural polyamine wasp toxin, is a n
                    # oncompetitive antagonist of certain ionotropic receptors. Six analogues of PhTX-343 (a synthetic a
                    # nalogue of the natural product), in which the secondary amino groups are systematically replaced b
                    # y oxygen or methylene groups, have been synthesized by coupling of N-(1-oxobutyl)tyrosine with 1,1
                    # 2-dodecanediamine, 4,9-dioxa-1, 12-dodecanediamine, or appropriately protected di- and triamines, 
                    # the latter being obtained by multistep syntheses. The resulting PhTX-343 analogues were purified a
                    # nd characterized, and their protolytic properties (stepwise macroscopic pK(a) values) were determi
                    # ned by (13)C NMR titrations. All analogues are fully protonated at physiological pH. The effects o
                    # f these compounds on acetylcholine-induced currents in TE671 cells clamped at various holding pote
                    # ntials were determined. All of the analogues noncompetitively antagonized the nicotinic acetylchol
                    # ine receptor (nAChR) in a concentration-, time-, and voltage-dependent manner. The amplitudes of a
                    # cetylcholine-induced currents were compared at their peaks and at the end of a 1 s application in 
                    # the presence or absence of the analogues. Most of the analogues were equipotent with or more poten
                    # t than PhTX-343. The dideaza analogue PhTX-12 [IC(50) of 0.3 microM (final current value)] was the
                    #  most potent, representing the highest potency improvement (about 50-fold) yet achieved by modific
                    # ation of the parent compound (PhTX-343). Thus, the presence of multiple positive charges in the Ph
                    # TX-343 molecule is not necessary for antagonism of nAChR. In contrast, the compounds were much les
                    # s potent than PhTX-343 at locust muscle ionotropic glutamate receptors sensitive to quisqualate (q
                    # GluR). The results demonstrate that the selectivity for different types of ionotropic receptors ca
                    # n be achieved by manipulating the polyamine moiety of PhTX-343.', 'weight': 60}' , '{'input': 'Two
                    #  series with the general formula of 4,6-diaryl-2-oxo-1,2 dihydropyridine-3-carbonitriles and their
                    #  isosteric 4,6-diaryl-2-imino-1,2-dihydropyridine-3-carbonitrile were synthesized through one pot 
                    # reaction of the appropriate acetophenone, aldehyde, ammonium acetate with ethyl cyanoacetate or ma
                    # lononitrile, respectively. The synthesized compounds were evaluated for their tumor cell growth in
                    # hibitory activity against the human HT-29 colon tumor cell line, as well as their PDE3 inhibitory 
                    # activity. Compound 4-(2-Ethoxyphenyl)-2-oxo-6-thiophen-3-yl-1,2-dihydropyridine-3 carbonitrile (21
                    # ) showed tumor cell growth inhibitory activity with an IC50 value of 1.25 microM. Meanwhile, 4-(4-
                    # Ethoxyphenyl)-2-imino-6-(thiophen-3-yl)-1,2-dihydropyridine-3-carbonitrile (26) showed inhibitory 
                    # effect upon PDE3 using cAMP or cGMP as substrate. No correlation exists between PDE3 inhibition an
                    # d the tumor cell growth inhibitory activity. Docking compound 21 to other possible molecular targe
                    # ts showed the potential to bind PIM1 Kinase.', 'weight': 60}' , '{'input': 'The cross talk between
                    #  different membrane receptors is the source of increasing research. We designed and synthesized a 
                    # new hetero-bivalent ligand that has antagonist properties on both A(1) adenosine and mu opiate rec
                    # eptors with a K(i) of 0.8+/-0.05 and 0.7+/-0.03 microM, respectively. This hybrid molecule increas
                    # es cAMP production in cells that over express the mu receptor as well as those over expressing the
                    #  A(1) adenosine receptor and reverses the antalgic effects of mu and A(1) adenosine receptor agoni
                    # sts in animals.', 'weight': 60}' , '{'input': 'All of the existing dopamine receptor models recogn
                    # ize the amine nitrogen of agonist and antagonist drugs as playing a crucial role in receptor inter
                    # actions. However, there has been some controversy as to which molecular form of the amine, charged
                    #  or uncharged, is most important in these interactions. We have synthesized and examined the biolo
                    # gical activity of permanently charged and permanently uncharged analogues of the dopaminergic anta
                    # gonist, sulpiride. Sulpiride and the permanently charged pyrrolidinium (6,7) and tetrahydrothiophe
                    # nium (9) analogues were able to antagonize the inhibitory effect of apomorphine on the K+-induced 
                    # release of [3H]acetylcholine from striatal slices. In contrast, the permanently uncharged tetrahyd
                    # rothiophene analogue 8 was inactive at concentrations up to 100 microM. Additionally, both sulpiri
                    # de and the tetrahydrothiophenium analogue were able to displace [3H]spiperone from D2 binding site
                    # s, while the tetrahydrothiophene analogue was unable to produce any significant displacement. Thes
                    # e results are consistent with our previous observations on permanently charged chlorpromazine anal
                    # ogues and provide further evidence that dopaminergic antagonists bind in their charged molecular f
                    # orms to anionic sites on the D2 receptor.', 'weight': 60}' , '{'input': "[1-(beta,beta-Pentamethyl
                    # ene-beta-mercaptopropionic acid),2-(O-ethyl)-D- tyrosine,4-valine,9-desglycine]arginine-vasopressi
                    # n (SK&F 101926, 1), a potent in vivo and in vitro vasopressin V2 receptor antagonist, was recently
                    #  tested in human volunteers and shown to be a full antidiuretic agonist. A new animal model for va
                    # sopressin activity has been developed in dogs that duplicates the clinical agonist findings exhibi
                    # ted with SK&F 101926. In this model we have discovered that substitution of a cis-4'-methyl group 
                    # on the Pmp moiety at residue 1 of vasopressin antagonists results in substantially reduced agonist
                    #  activity compared to the unsubstituted molecule (SK&F 101926). The corresponding analogue with a 
                    # trans-4'-methyl group exhibits more agonist activity than the cis molecule. These findings can be 
                    # explained by viewing the biological activities of compounds such as 1 as the interaction of the va
                    # sopressin receptor with a number of discrete molecular entities, conformers of 1, which present di
                    # fferent pharmacophores. Models have been developed to assist in the understanding of these results
                    # .", 'weight': 60}' , '{'input': "The synthesis and pharmacological properties of a novel type of v
                    # asorelaxant hybrid compounds are described. The investigated compounds originate from fluorinated 
                    # 4-aryl-1,4-dihydropyridines, which are known calcium channel blockers, and/or from fluorinated ana
                    # logues of pinacidil, which is an opener of ATP-sensitive potassium channels. In particular, we stu
                    # died the most potent hybrid, 2,6-dimethyl-3,5-dicarbomethoxy-4-(2-difluoromethoxy-5-N-(N' '-cyano-
                    # N'-1,2,2-trimethyl-propylguanidyl)-phenyl)-1, 4-dihydropyridine (4a), together with its parent com
                    # pounds, the dihydropyridine 1b and the pinacidil analogue 3. In isolated rat mesenteric arteries, 
                    # micromolar concentrations of 4a relaxed contractions exerted by K(+)-depolarization or by norepine
                    # phrine. The latter effect was sensitive to the potassium channel blocker glibenclamide. Micromolar
                    #  4a also inhibited [(3)H](+)-isradipine and [(3)H]P1075 binding to rat cardiac membranes, and it b
                    # locked L-type calcium channels expressed in a mammalian cell line. The respective parent compounds
                    #  1b and 3 were always more potent and more selective regarding calcium channel or potassium channe
                    # l interaction, respectively. In contrast, 4a combined both effects within the same concentration r
                    # ange, indicating that it may represent a lead structure for a novel class of pharmacological hybri
                    # d compounds.", 'weight': 60}' , '{'input': 'A new series of N-deacetyl-N-(N-trifluoroacetylaminoac
                    # yl)thiocolchicine derivatives 9-15 have been synthesized starting from the corresponding N-deacety
                    # lthiocolchicine (3) and the N-trifluoroacetylamino acids 5-8 which were used as a racemic mixture.
                    #  The trifluoroacetyl protecting group has been removed easily, giving the corresponding N-deacetyl
                    # -N-aminoacylthiocolchicines 16-22. Optical pure compounds 9-22 were isolated from the diastereoiso
                    # meric mixture or were prepared starting from compound 3 and an optical pure amino acid derivative;
                    #  the configuration of each compound was assigned unequivocally. The diastereoisomeric couples of a
                    # mino acids synthesized were tested, and their antiproliferative activity on MDR-positive and MDR-n
                    # egative human cancer cell lines was evaluated.', 'weight': 60}' , '{'input': 'New prodrugs of daun
                    # orubicin and doxorubicin designed for selective activation by the serine protease plasmin are desc
                    # ribed. The low toxic prodrugs 3, 4, and 5 are converted to the corresponding cytotoxic drugs upon 
                    # proteolysis by the tumor-associated protease plasmin. Application of a self-eliminating spacer was
                    #  essential for enzyme activation. A prodrug containing a chloro-substituted spacer was synthesized
                    #  with the aim of enhancing the rate of conversion by plasmin. All prodrugs were highly stable in b
                    # uffer solution and in serum and on the average 15-fold less cytotoxic than the parent drugs in sev
                    # en human tumor cell lines. A marked in vitro selectivity was demonstrated by incubation of the dox
                    # orubicin prodrugs with a plasmin generating MCF-7 breast cancer cell line transfected with urokina
                    # se-type plasminogen activator (u-PA) in comparison with the nontransfected nonplasmin generating c
                    # ell line. Prodrugs 4 and 5 showed the same cytotoxic effect as the free parent drug doxorubicin in
                    #  the u-PA transfected cells, indicating complete conversion of the prodrug by plasmin. Addition of
                    #  the plasmin inhibitor Trasylol drastically increased the ID(50) values in the u-PA transfected MC
                    # F-7 cells for both prodrugs 4 and 5.', 'weight': 60}' , '{'input': 'A series of 1-hydroxy-3-¿3-hyd
                    # roxy-7-phenyl-1-hepten-1-yl cyclohexane acetic acid derivatives was designed based on postulated a
                    # ctive conformation of leukotriene B(4) (LTB(4)) and evaluated as human cell surface LTB(4) recepto
                    # r (BLTR) antagonists. Binding was determined through ¿(3)HLTB(4) displacement from human neutrophi
                    # ls and receptor antagonistic assays by in vitro measurements of inhibition of leukocyte chemotaxis
                    #  induced by LTB(4). On the basis of these assays, a structure-affinity relationship was investigat
                    # ed. Optimization of the acid chain length and omega-substitution of a phenyl group on the lipophil
                    # ic tail were shown to be critical for binding activity. These modifications led to the discovery o
                    # f compounds with submicromolar potency and selective BLTR antagonism. The most potent compound 3ba
                    # lpha (IC(50) = 250 nM) was found to significantly inhibit oedema formation in a topical model of p
                    # horbolester-induced inflammation. Substantial improvement of in vitro potency was achieved by modi
                    # fication of the carboxylic acid function leading to the identification of the N,N-dimethylamide se
                    # ries. Compound 5balpha, free of agonist activity, displayed higher potency in receptor binding wit
                    # h an IC(50) of 40 nM. These results support the hypothesis that the spatial relationship between t
                    # he carboxylic acid and allylic hydroxyl functions is crucial for high binding affinity with BLTR.'
                    # , 'weight': 60}'
                }
            },
            'abstract': 'TEXT',
            # EXAMPLES:
            # 'Tramadol is a centrally acting opioid analgesic structurally related to codeine and morphine. Analogs of 
            # tramadol with deuterium-for-hydrogen replacement at metabolically active sites were prepared and evaluated
            #  in vitro and in vivo.' , '' , '' , 'New aromatic and heteroaromatic analogues of polyunsaturated fatty ac
            # id metabolites have been prepared using short and versatile strategies. Preliminary studies of their activ
            # ity as inhibitors of platelet aggregation are reported.' , 'Several new pyridazino[1',6':1,2]pyrido[3,4-b]
            # indol-5-inium derivatives were synthesised from beta-carboline derivatives and their cytotoxic activity an
            # d effect on the cell cycle were evaluated against L1210 cancer cells.' , 'A novel non-imidazole fluorene o
            # xime 1a has been identified as a histamine H(3) inhibitor, and its structure-activity relationship has bee
            # n evaluated.' , 'A group of isomers possessing a 2-, 3-, or 4-acetoxy moiety on the 3-phenyl substituent o
            # f rofecoxib were synthesized that exhibit highly potent, and selective, COX-2 inhibitory activity that hav
            # e the potential to acetylate the COX-2 isozyme.' , '' , '' , 'A number of novel 3',4'-fused dioxolane and 
            # dioxane sordarin derivatives were synthesised for structure-activity relationship studies. Many of these d
            # erivatives exhibit high activity against Candida spp. and Cryptococcus neoformans.'
            'authors': 'TEXT',
            # EXAMPLES:
            # 'Rosini M, Budriesi R, Bixel MG, Bolognesi ML, Chiarini A, Hucho F, Krogsgaard-Larsen P, Mellor IR, Minari
            # ni A, Tumiatti V, Usherwood PN, Melchiorre C.' , 'Stromgaard K, Brierley MJ, Andersen K, Sløk FA, Mellor I
            # R, Usherwood PN, Krogsgaard-Larsen P, Jaroszewski JW.' , 'Abadi AH, Abouel-Ella DA, Lehmann J, Tinsley HN,
            #  Gary BD, Piazza GA, Abdel-Fattah MA.' , 'Mathew SC, Ghosh N, By Y, Berthault A, Virolleaud MA, Carrega L,
            #  Chouraqui G, Commeiras L, Condo J, Attolini M, Gaudel-Siri A, Ruf J, Parrain JL, Rodriguez J, Guieu R.' ,
            #  'Harrold MW, Wallace RA, Farooqui T, Wallace LJ, Uretsky N, Miller DD.' , 'Huffman WF, Albrightson-Winslo
            # w C, Brickson B, Bryan HG, Caldwell N, Dytko G, Eggleston DS, Kinter LB, Moore ML, Newlander KA.' , 'Yagup
            # olskii LM, Antepohl W, Artunc F, Handrock R, Klebanov BM, Maletina II, Marxen B, Petko KI, Quast U, Vogt A
            # , Weiss C, Zibold J, Herzig S.' , 'Gelmi ML, Mottadelli S, Pocar D, Riva A, Bombardelli E, De Vincenzo R, 
            # Scambia G.' , 'de Groot FM, de Bart AC, Verheijen JH, Scheeren HW.' , 'Poudrel JM, Hullot P, Vidal JP, Gir
            # ard JP, Rossi JC, Muller A, Bonne C, Bezuglov V, Serkov I, Renard P, Pfeiffer B.'
            'chembl_release': 
            {
                'properties': 
                {
                    'chembl_release': 'TEXT',
                    # EXAMPLES:
                    # 'CHEMBL_1' , 'CHEMBL_1' , 'CHEMBL_4' , 'CHEMBL_4' , 'CHEMBL_1' , 'CHEMBL_1' , 'CHEMBL_1' , 'CHEMBL
                    # _1' , 'CHEMBL_1' , 'CHEMBL_1'
                    'creation_date': 'TEXT',
                    # EXAMPLES:
                    # '2009-09-03' , '2009-09-03' , '2010-05-18' , '2010-05-18' , '2009-09-03' , '2009-09-03' , '2009-09
                    # -03' , '2009-09-03' , '2009-09-03' , '2009-09-03'
                }
            },
            'contact': 'TEXT',
            # EXAMPLES:
            # 'https://orcid.org/0000-0003-2402-4157, https://orcid.org/0000-0001-6518-3344' , 'https://orcid.org/0000-0
            # 003-2402-4157, https://orcid.org/0000-0001-6518-3344' , 'https://orcid.org/0000-0003-2402-4157, https://or
            # cid.org/0000-0001-6518-3344' , 'https://orcid.org/0000-0003-2402-4157, https://orcid.org/0000-0001-6518-33
            # 44' , 'https://orcid.org/0000-0003-2402-4157, https://orcid.org/0000-0001-6518-3344' , 'https://orcid.org/
            # 0000-0003-2402-4157, https://orcid.org/0000-0001-6518-3344' , 'https://orcid.org/0000-0003-2402-4157, http
            # s://orcid.org/0000-0001-6518-3344' , 'https://orcid.org/0000-0003-2402-4157, https://orcid.org/0000-0001-6
            # 518-3344' , 'https://orcid.org/0000-0003-2402-4157, https://orcid.org/0000-0001-6518-3344' , 'https://orci
            # d.org/0000-0003-2402-4157, https://orcid.org/0000-0001-6518-3344'
            'doc_type': 'TEXT',
            # EXAMPLES:
            # 'PUBLICATION' , 'PUBLICATION' , 'PUBLICATION' , 'PUBLICATION' , 'PUBLICATION' , 'PUBLICATION' , 'PUBLICATI
            # ON' , 'PUBLICATION' , 'PUBLICATION' , 'PUBLICATION'
            'document_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL1132540' , 'CHEMBL1132165' , 'CHEMBL1154630' , 'CHEMBL1154631' , 'CHEMBL1124686' , 'CHEMBL1124672' 
            # , 'CHEMBL1132542' , 'CHEMBL1132543' , 'CHEMBL1132544' , 'CHEMBL1132545'
            'doi': 'TEXT',
            # EXAMPLES:
            # '10.1021/jm991110n' , '10.1021/jm9903747' , '10.1016/j.ejmech.2009.09.029' , '10.1016/j.bmcl.2009.09.112' 
            # , '10.1021/jm00124a024' , '10.1021/jm00124a025' , '10.1021/jm990443h' , '10.1021/jm981134e' , '10.1021/jm9
            # 910472' , '10.1021/jm9910573'
            'first_page': 'NUMERIC',
            # EXAMPLES:
            # '5212' , '5224' , '90' , '6736' , '874' , '880' , '5266' , '5272' , '5277' , '5289'
            'issue': 'NUMERIC',
            # EXAMPLES:
            # '25' , '25' , '1' , '23' , '4' , '4' , '25' , '25' , '25' , '26'
            'journal': 'TEXT',
            # EXAMPLES:
            # 'J Med Chem' , 'J Med Chem' , 'Eur J Med Chem' , 'Bioorg Med Chem Lett' , 'J Med Chem' , 'J Med Chem' , 'J
            #  Med Chem' , 'J Med Chem' , 'J Med Chem' , 'J Med Chem'
            'journal_full_title': 'TEXT',
            # EXAMPLES:
            # 'Journal of medicinal chemistry.' , 'Journal of medicinal chemistry.' , 'European journal of medicinal che
            # mistry.' , 'Bioorganic & medicinal chemistry letters.' , 'Journal of medicinal chemistry.' , 'Journal of m
            # edicinal chemistry.' , 'Journal of medicinal chemistry.' , 'Journal of medicinal chemistry.' , 'Journal of
            #  medicinal chemistry.' , 'Journal of medicinal chemistry.'
            'last_page': 'NUMERIC',
            # EXAMPLES:
            # '5223' , '5234' , '97' , '6739' , '880' , '884' , '5271' , '5276' , '5283' , '5310'
            'patent_id': 'TEXT',
            # EXAMPLES:
            # 'US-9061041-B2' , 'US-9062066-B2' , 'US-7288556-B2' , 'US-9067888-B2' , 'US-8394808-B2' , 'US-8507484-B2' 
            # , 'WO-2012035171-A2' , 'US-9067924-B2' , 'WO-2013072694-A1' , 'US-9067949-B2'
            'pubmed_id': 'NUMERIC',
            # EXAMPLES:
            # '10602706' , '10602707' , '19836860' , '19836950' , '2522993' , '2522994' , '10602711' , '10602712' , '106
            # 02713' , '10639274'
            'src_id': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
            'title': 'TEXT',
            # EXAMPLES:
            # 'Design, synthesis, and biological evaluation of symmetrically and unsymmetrically substituted methoctrami
            # ne-related polyamines as muscular nicotinic receptor noncompetitive antagonists.' , 'Analogues of neuroact
            # ive polyamine wasp toxins that lack inner basic sites exhibit enhanced antagonism toward a muscle-type mam
            # malian nicotinic acetylcholine receptor.' , 'Discovery of colon tumor cell growth inhibitory agents throug
            # h a combinatorial approach.' , 'Design, synthesis and biological evaluation of a bivalent micro opiate and
            #  adenosine A1 receptor antagonist.' , 'Synthesis and D2 dopaminergic activity of pyrrolidinium, tetrahydro
            # thiophenium, and tetrahydrothiophene analogues of sulpiride.' , 'A minor modification of residue 1 in pote
            # nt vasopressin antagonists dramatically reduces agonist activity.' , 'Vasorelaxation by new hybrid compoun
            # ds containing dihydropyridine and pinacidil-like moieties.' , 'N-deacetyl-N-aminoacylthiocolchicine deriva
            # tives: synthesis and biological evaluation on MDR-positive and MDR-negative human cancer cell lines.' , 'S
            # ynthesis and biological evaluation of novel prodrugs of anthracyclines for selective activation by the tum
            # or-associated protease plasmin.' , 'Synthesis and structure-activity relationships of new 1, 3-disubstitut
            # ed cyclohexanes as structurally rigid leukotriene B(4) receptor antagonists.'
            'volume': 'NUMERIC',
            # EXAMPLES:
            # '42' , '42' , '45' , '19' , '32' , '32' , '42' , '42' , '42' , '42'
            'year': 'NUMERIC',
            # EXAMPLES:
            # '1999' , '1999' , '2010' , '2009' , '1989' , '1989' , '1999' , '1999' , '1999' , '1999'
        }
    }
