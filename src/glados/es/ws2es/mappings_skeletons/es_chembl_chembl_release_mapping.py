# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'chembl_release': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL_1' , 'CHEMBL_2' , 'CHEMBL_3' , 'CHEMBL_4' , 'CHEMBL_5' , 'CHEMBL_6' , 'CHEMBL_7' , 'CHEMBL_8' , 'C
            # HEMBL_9' , 'CHEMBL_10'
            'creation_date': 'TEXT',
            # EXAMPLES:
            # '2009-09-03' , '2009-11-30' , '2010-04-16' , '2010-05-18' , '2010-06-24' , '2010-08-27' , '2010-09-29' , '
            # 2010-10-26' , '2011-01-20' , '2011-05-26'
        }
    }
