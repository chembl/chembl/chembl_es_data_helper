# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'action_type': 
            {
                'properties': 
                {
                    'action_type': 'TEXT',
                    # EXAMPLES:
                    # 'ANTAGONIST' , 'ANTAGONIST' , 'ANTAGONIST' , 'ANTAGONIST' , 'ANTAGONIST' , 'ANTAGONIST' , 'ANTAGON
                    # IST' , 'ANTAGONIST' , 'ANTAGONIST' , 'ANTAGONIST'
                    'description': 'TEXT',
                    # EXAMPLES:
                    # 'Binds to a receptor and prevents activation by an agonist through competing for the binding site'
                    #  , 'Binds to a receptor and prevents activation by an agonist through competing for the binding si
                    # te' , 'Binds to a receptor and prevents activation by an agonist through competing for the binding
                    #  site' , 'Binds to a receptor and prevents activation by an agonist through competing for the bind
                    # ing site' , 'Binds to a receptor and prevents activation by an agonist through competing for the b
                    # inding site' , 'Binds to a receptor and prevents activation by an agonist through competing for th
                    # e binding site' , 'Binds to a receptor and prevents activation by an agonist through competing for
                    #  the binding site' , 'Binds to a receptor and prevents activation by an agonist through competing 
                    # for the binding site' , 'Binds to a receptor and prevents activation by an agonist through competi
                    # ng for the binding site' , 'Binds to a receptor and prevents activation by an agonist through comp
                    # eting for the binding site'
                    'parent_type': 'TEXT',
                    # EXAMPLES:
                    # 'NEGATIVE MODULATOR' , 'NEGATIVE MODULATOR' , 'NEGATIVE MODULATOR' , 'NEGATIVE MODULATOR' , 'NEGAT
                    # IVE MODULATOR' , 'NEGATIVE MODULATOR' , 'NEGATIVE MODULATOR' , 'NEGATIVE MODULATOR' , 'NEGATIVE MO
                    # DULATOR' , 'NEGATIVE MODULATOR'
                }
            },
            'activity_comment': 'TEXT',
            # EXAMPLES:
            # 'Not Active' , 'Not Active' , 'Not Active' , 'Active' , 'Active' , 'Not Determined' , 'Not Determined' , '
            # Not Determined' , 'Not Determined' , 'Not Determined'
            'activity_id': 'NUMERIC',
            # EXAMPLES:
            # '1930036' , '1930041' , '1929606' , '1929610' , '1929614' , '1930050' , '1930070' , '1929649' , '1929657' 
            # , '1930090'
            'activity_properties': 
            {
                'properties': 
                {
                    'comments': 'TEXT',
                    # EXAMPLES:
                    # 'extracted from assay description on may 2023' , 'extracted from assay description on may 2023' , 
                    # 'extracted from assay description on may 2023' , 'extracted from assay description on may 2023' , 
                    # 'extracted from assay description on may 2023' , 'extracted from assay description on may 2023' , 
                    # 'extracted from assay description on may 2023' , 'extracted from assay description on may 2023' , 
                    # 'extracted from assay description on may 2023' , 'extracted from assay description on may 2023'
                    'relation': 'TEXT',
                    # EXAMPLES:
                    # '=' , '=' , '=' , '=' , '<=' , '=' , '<=' , '=' , '=' , '='
                    'result_flag': 'NUMERIC',
                    # EXAMPLES:
                    # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
                    'standard_relation': 'TEXT',
                    # EXAMPLES:
                    # '=' , '=' , '=' , '=' , '<=' , '=' , '<=' , '=' , '=' , '='
                    'standard_text_value': 'TEXT',
                    # EXAMPLES:
                    # 'ORAL' , 'ORAL' , 'ORAL' , 'INTRAVENOUS' , 'ORAL' , 'INTRAPERITONEAL' , 'INTRAPERITONEAL' , 'ORAL'
                    #  , 'ORAL' , 'ORAL'
                    'standard_type': 'TEXT',
                    # EXAMPLES:
                    # 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'TIME' , 'DOSE' , 'TIME' , 'DOSE' , 'DOSE' , 'DOSE'
                    'standard_units': 'TEXT',
                    # EXAMPLES:
                    # 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'hr' , 'mg.kg-1' , 'hr' , 'mg.kg-1' , 'mg.kg-1' , 
                    # 'mg.kg-1'
                    'standard_value': 'NUMERIC',
                    # EXAMPLES:
                    # '10.0' , '10.0' , '10.0' , '30.0' , '4.0' , '50.0' , '6.0' , '10.0' , '10.0' , '3.0'
                    'text_value': 'TEXT',
                    # EXAMPLES:
                    # 'ORAL' , 'ORAL' , 'ORAL' , 'INTRAVENOUS' , 'ORAL' , 'INTRAPERITONEAL' , 'INTRAPERITONEAL' , 'ORAL'
                    #  , 'ORAL' , 'ORAL'
                    'type': 'TEXT',
                    # EXAMPLES:
                    # 'DOSE' , 'DOSE' , 'DOSE' , 'DOSE' , 'TIME' , 'DOSE' , 'TIME' , 'DOSE' , 'DOSE' , 'DOSE'
                    'units': 'TEXT',
                    # EXAMPLES:
                    # 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'hr' , 'mg/kg' , 'hr' , 'mg/kg' , 'mg/kg' , 'mg/kg'
                    'value': 'NUMERIC',
                    # EXAMPLES:
                    # '10.0' , '10.0' , '10.0' , '30.0' , '4.0' , '50.0' , '6.0' , '10.0' , '10.0' , '3.0'
                }
            },
            'assay_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL893433' , 'CHEMBL893433' , 'CHEMBL896910' , 'CHEMBL892737' , 'CHEMBL892737' , 'CHEMBL893434' , 'CHE
            # MBL894191' , 'CHEMBL892615' , 'CHEMBL892434' , 'CHEMBL894191'
            'assay_description': 'TEXT',
            # EXAMPLES:
            # 'Antibacterial activity against penicillin-resistant Streptococcus pneumoniae by microdilution broth metho
            # d' , 'Antibacterial activity against penicillin-resistant Streptococcus pneumoniae by microdilution broth 
            # method' , 'Survival of Plasmodium berghei KBG 173 infected CD1 mice (Mus musculus) at 320 mg/kg/day' , 'An
            # timicrobial activity against Candida tropicalis IP 2031 by broth microdilution method' , 'Antimicrobial ac
            # tivity against Candida tropicalis IP 2031 by broth microdilution method' , 'Antibacterial activity against
            #  methicillin-resistant Staphylococcus aureus BAA-42 infected po dosed KunMing mouse' , 'Toxicity in ip dos
            # ed B6D2F1 mouse' , 'Inhibition of TPA-induced EBV early antigen activation in Raji cells at 32 nmol relati
            # ve to control' , 'Bacteriostatic activity against Staphylococcus aureus ATCC 9144 after 24 hrs by serial d
            # ilution microtiter plate method' , 'Toxicity in ip dosed B6D2F1 mouse'
            'assay_type': 'TEXT',
            # EXAMPLES:
            # 'F' , 'F' , 'A' , 'F' , 'F' , 'F' , 'T' , 'F' , 'F' , 'T'
            'assay_variant_accession': 'TEXT',
            # EXAMPLES:
            # 'Q9WJQ2' , 'Q9WJQ2' , 'Q9WJQ2' , 'Q9WJQ2' , 'Q9WJQ2' , 'Q9WJQ2' , 'Q72547' , 'Q9WJQ2' , 'Q72547' , 'Q9WJQ2
            # '
            'assay_variant_mutation': 'TEXT',
            # EXAMPLES:
            # 'K103N,Y181C' , 'K103N,Y181C' , 'K103N,Y181C' , 'K103N,Y181C' , 'K103N,Y181C' , 'K103N,Y181C' , 'M184I' , 
            # 'K103N' , 'Y181C' , 'K103N'
            'bao_endpoint': 'TEXT',
            # EXAMPLES:
            # 'BAO_0002146' , 'BAO_0002146' , 'BAO_0000179' , 'BAO_0002146' , 'BAO_0002146' , 'BAO_0003036' , 'BAO_00001
            # 79' , 'BAO_0000201' , 'BAO_0000190' , 'BAO_0000179'
            'bao_format': 'TEXT',
            # EXAMPLES:
            # 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_00002
            # 18' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218'
            'bao_label': 'TEXT',
            # EXAMPLES:
            # 'organism-based format' , 'organism-based format' , 'organism-based format' , 'organism-based format' , 'o
            # rganism-based format' , 'organism-based format' , 'organism-based format' , 'organism-based format' , 'org
            # anism-based format' , 'organism-based format'
            'canonical_smiles': 'TEXT',
            # EXAMPLES:
            # 'O=C1O[C@@H](Cn2ccnn2)CN1c1ccc(N2CCN(S(=O)(=O)c3ccc(F)cc3)CC2)c(F)c1' , 'O=C1O[C@@H](Cn2ccnn2)CN1c1ccc(N2C
            # CNCC2)c(F)c1' , 'CC(=O)O[C@H]1C[C@H]2[C@@H]([C@H](OC(C)=O)C[C@@H]3CC4(CC[C@@]32C)OOC2(CCC(C)CC2)OO4)[C@@H]
            # 2CC[C@H]([C@H](C)CCC(=O)NCCN(C)C)[C@@]12C' , 'O=C(CS(=O)(=O)c1ccc(I)cc1)c1ccc([N+](=O)[O-])cc1' , 'Cc1ccc(
            # S(=O)(=O)CC(=O)c2ccc([N+](=O)[O-])cc2)cc1' , 'O=C1O[C@@H](Cn2ccnn2)CN1c1ccc(N2CCN(S(=O)(=O)c3cccc(F)c3)CC2
            # )c(F)c1' , 'COc1cc(/C=N\c2cc3c4c(cccc4c2)C(=O)N(CCN(C)C)C3=O)ccc1O' , 'COc1ccc(C/C=C/c2ccccc2)c(O)c1OC' , 
            # 'COc1ccc2oc(=O)sc2c1C(=O)/C=C/c1ccc(OCCN2CCOCC2)cc1' , 'CN(C)CCN1C(=O)c2cccc3cc(NC(=O)CCCCl)cc(c23)C1=O'
            'data_validity_comment': 'TEXT',
            # EXAMPLES:
            # 'Outside typical range' , 'Outside typical range' , 'Outside typical range' , 'Outside typical range' , 'O
            # utside typical range' , 'Potential transcription error' , 'Outside typical range' , 'Outside typical range
            # ' , 'Outside typical range' , 'Outside typical range'
            'data_validity_description': 'TEXT',
            # EXAMPLES:
            # 'Values for this activity type are unusually large/small, so may not be accurate' , 'Values for this activ
            # ity type are unusually large/small, so may not be accurate' , 'Values for this activity type are unusually
            #  large/small, so may not be accurate' , 'Values for this activity type are unusually large/small, so may n
            # ot be accurate' , 'Values for this activity type are unusually large/small, so may not be accurate' , 'Val
            # ues appear to be an order of magnitude different from previously reported, so units may be incorrect' , 'V
            # alues for this activity type are unusually large/small, so may not be accurate' , 'Values for this activit
            # y type are unusually large/small, so may not be accurate' , 'Values for this activity type are unusually l
            # arge/small, so may not be accurate' , 'Values for this activity type are unusually large/small, so may not
            #  be accurate'
            'document_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL1147903' , 'CHEMBL1147903' , 'CHEMBL1148828' , 'CHEMBL1147845' , 'CHEMBL1147845' , 'CHEMBL1147903' 
            # , 'CHEMBL1148725' , 'CHEMBL1147206' , 'CHEMBL1147190' , 'CHEMBL1148725'
            'document_journal': 'TEXT',
            # EXAMPLES:
            # 'Eur J Med Chem' , 'Eur J Med Chem' , 'J Med Chem' , 'Eur J Med Chem' , 'Eur J Med Chem' , 'Eur J Med Chem
            # ' , 'J Med Chem' , 'Eur J Med Chem' , 'Eur J Med Chem' , 'J Med Chem'
            'document_year': 'NUMERIC',
            # EXAMPLES:
            # '2007' , '2007' , '2007' , '2007' , '2007' , '2007' , '2007' , '2007' , '2007' , '2007'
            'ligand_efficiency': 
            {
                'properties': 
                {
                    'bei': 'NUMERIC',
                    # EXAMPLES:
                    # '20.75' , '17.17' , '13.84' , '13.43' , '13.08' , '14.78' , '9.58' , '17.61' , '16.22' , '16.21'
                    'le': 'NUMERIC',
                    # EXAMPLES:
                    # '0.40' , '0.34' , '0.26' , '0.26' , '0.25' , '0.28' , '0.18' , '0.33' , '0.31' , '0.31'
                    'lle': 'NUMERIC',
                    # EXAMPLES:
                    # '6.24' , '1.63' , '-0.29' , '-0.11' , '-0.27' , '0.05' , '-2.51' , '2.47' , '1.48' , '1.82'
                    'sei': 'NUMERIC',
                    # EXAMPLES:
                    # '8.59' , '12.48' , '9.00' , '8.47' , '8.25' , '11.38' , '4.75' , '7.94' , '6.87' , '6.95'
                }
            },
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL244493' , 'CHEMBL244304' , 'CHEMBL410156' , 'CHEMBL242153' , 'CHEMBL244889' , 'CHEMBL244492' , 'CHE
            # MBL244508' , 'CHEMBL243673' , 'CHEMBL244082' , 'CHEMBL243854'
            'molecule_pref_name': 'TEXT',
            # EXAMPLES:
            # 'DOXORUBICIN' , 'ZILEUTON' , 'FENOPROFEN' , 'DEXAMETHASONE' , 'VASOPRESSIN' , 'RELCOVAPTAN' , '6-O-BENZYLG
            # UANINE' , 'STANOLONE' , '(R)-BICALUTAMIDE' , '(R)-BICALUTAMIDE'
            'parent_molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL244493' , 'CHEMBL244304' , 'CHEMBL410156' , 'CHEMBL242153' , 'CHEMBL244889' , 'CHEMBL244492' , 'CHE
            # MBL244508' , 'CHEMBL243673' , 'CHEMBL244082' , 'CHEMBL243854'
            'pchembl_value': 'NUMERIC',
            # EXAMPLES:
            # '8.14' , '8.85' , '6.39' , '6.01' , '5.85' , '6.82' , '5.89' , '8.59' , '7.22' , '8.16'
            'potential_duplicate': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'qudt_units': 'TEXT',
            # EXAMPLES:
            # 'http://www.openphacts.org/units/MicrogramPerMilliliter' , 'http://www.openphacts.org/units/MicrogramPerMi
            # lliliter' , 'http://qudt.org/vocab/unit#Day' , 'http://www.openphacts.org/units/MicrogramPerMilliliter' , 
            # 'http://www.openphacts.org/units/MicrogramPerMilliliter' , 'http://qudt.org/vocab/unit#Percent' , 'http://
            # www.openphacts.org/units/MicrogramPerMilliliter' , 'http://www.openphacts.org/units/MicrogramPerMilliliter
            # ' , 'http://qudt.org/vocab/unit#Hour' , 'http://www.openphacts.org/units/MicrogramPerMilliliter'
            'record_id': 'NUMERIC',
            # EXAMPLES:
            # '650838' , '650833' , '656627' , '649862' , '649858' , '650837' , '653823' , '649567' , '649297' , '653803
            # '
            'relation': 'TEXT',
            # EXAMPLES:
            # '=' , '=' , '=' , '>' , '>' , '>' , '>' , '=' , '=' , '>'
            'src_id': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
            'standard_flag': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '0' , '1' , '1' , '1' , '0' , '1' , '1' , '0'
            'standard_relation': 'TEXT',
            # EXAMPLES:
            # '=' , '=' , '=' , '>' , '>' , '>' , '>' , '=' , '=' , '>'
            'standard_text_value': 'TEXT',
            # EXAMPLES:
            # 'Compound NOT metabolized' , 'Compound NOT metabolized' , 'Compound NOT metabolized' , 'Compound NOT metab
            # olized' , 'Compound NOT metabolized' , 'Compound NOT metabolized' , 'Compound NOT metabolized' , 'Compound
            #  NOT metabolized' , 'Compound NOT metabolized' , 'Compound NOT metabolized'
            'standard_type': 'TEXT',
            # EXAMPLES:
            # 'MIC' , 'MIC' , 'Survival' , 'MIC' , 'MIC' , 'ED50' , 'MTD' , 'Inhibition' , 'IC50' , 'MTD'
            'standard_units': 'TEXT',
            # EXAMPLES:
            # 'ug.mL-1' , 'ug.mL-1' , 'day' , 'ug.mL-1' , 'ug.mL-1' , 'mg.kg-1' , 'mg kg-1' , '%' , 'ug.mL-1' , 'mg kg-1
            # '
            'standard_value': 'NUMERIC',
            # EXAMPLES:
            # '0.5' , '1.0' , '17.0' , '100.0' , '100.0' , '100.0' , '80.0' , '13.3' , '31.2' , '80.0'
            'target_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL347' , 'CHEMBL347' , 'CHEMBL375' , 'CHEMBL612870' , 'CHEMBL612870' , 'CHEMBL352' , 'CHEMBL375' , 'C
            # HEMBL613124' , 'CHEMBL352' , 'CHEMBL375'
            'target_organism': 'TEXT',
            # EXAMPLES:
            # 'Streptococcus pneumoniae' , 'Streptococcus pneumoniae' , 'Mus musculus' , 'Candida tropicalis' , 'Candida
            #  tropicalis' , 'Staphylococcus aureus' , 'Mus musculus' , 'Human gammaherpesvirus 4' , 'Staphylococcus aur
            # eus' , 'Mus musculus'
            'target_pref_name': 'TEXT',
            # EXAMPLES:
            # 'Streptococcus pneumoniae' , 'Streptococcus pneumoniae' , 'Mus musculus' , 'Candida tropicalis' , 'Candida
            #  tropicalis' , 'Staphylococcus aureus' , 'Mus musculus' , 'Human gammaherpesvirus 4' , 'Staphylococcus aur
            # eus' , 'Mus musculus'
            'target_tax_id': 'NUMERIC',
            # EXAMPLES:
            # '1313' , '1313' , '10090' , '5482' , '5482' , '1280' , '10090' , '10376' , '1280' , '10090'
            'text_value': 'TEXT',
            # EXAMPLES:
            # 'Compound NOT metabolized' , 'Compound NOT metabolized' , 'Compound NOT metabolized' , 'Compound NOT metab
            # olized' , 'Compound NOT metabolized' , 'Compound NOT metabolized' , 'Compound NOT metabolized' , 'Compound
            #  NOT metabolized' , 'Compound NOT metabolized' , 'Compound NOT metabolized'
            'toid': 'NUMERIC',
            # EXAMPLES:
            # '9210' , '9211' , '9212' , '9212' , '11154' , '11156' , '11155' , '11156' , '9213' , '9213'
            'type': 'TEXT',
            # EXAMPLES:
            # 'MIC' , 'MIC' , 'Survival' , 'MIC' , 'MIC' , 'ED50' , 'MTD' , 'Inhibition' , 'IC50' , 'MTD'
            'units': 'TEXT',
            # EXAMPLES:
            # 'ug ml-1' , 'ug ml-1' , 'day' , 'mg/ml' , 'mg/ml' , 'mg kg-1' , 'mg kg-1' , '%' , 'ug ml-1' , 'mg kg-1'
            'uo_units': 'TEXT',
            # EXAMPLES:
            # 'UO_0000274' , 'UO_0000274' , 'UO_0000033' , 'UO_0000274' , 'UO_0000274' , 'UO_0000308' , 'UO_0000308' , '
            # UO_0000187' , 'UO_0000274' , 'UO_0000308'
            'upper_value': 'NUMERIC',
            # EXAMPLES:
            # '12.0' , '345.0' , '1000.0' , '1000.0' , '1000.0' , '1000.0' , '1000.0' , '5.0' , '5.0' , '5.41'
            'value': 'NUMERIC',
            # EXAMPLES:
            # '0.5' , '1.0' , '17.0' , '0.1' , '0.1' , '100.0' , '80.0' , '13.3' , '31.2' , '80.0'
        }
    }
