# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'applicants': 'TEXT',
            # EXAMPLES:
            # 'Promius Pharma Llc' , 'Amneal Eu Ltd' , 'Hoffmann La Roche Inc' , 'Leadiant Biosciences Inc' , 'Hetero La
            # bs Ltd Unit Iii' , 'Par Pharmaceutical Inc' , 'Wyeth Ayerst Laboratories' , 'Novel Laboratories Inc' , 'An
            # chen Pharmaceuticals Inc' , 'Cheplapharm Arzneimittel Gmbh'
            'atc_classification': 
            {
                'properties': 
                {
                    'code': 'TEXT',
                    # EXAMPLES:
                    # 'N06AX01' , 'C01DA14' , 'N02CA07' , 'G04BX16' , 'N02CX05' , 'C04AX10' , 'L01XB01' , 'J05AE10' , 'N
                    # 06BX16' , 'N04BX01'
                    'description': 'TEXT',
                    # EXAMPLES:
                    # 'NERVOUS SYSTEM: PSYCHOANALEPTICS: ANTIDEPRESSANTS: Other antidepressants' , 'CARDIOVASCULAR SYSTE
                    # M: CARDIAC THERAPY: VASODILATORS USED IN CARDIAC DISEASES: Organic nitrates' , 'NERVOUS SYSTEM: AN
                    # ALGESICS: ANTIMIGRAINE PREPARATIONS: Ergot alkaloid' , 'GENITO URINARY SYSTEM AND SEX HORMONES: UR
                    # OLOGICALS: UROLOGICALS: Other urologicals' , 'NERVOUS SYSTEM: ANALGESICS: ANTIMIGRAINE PREPARATION
                    # S: Other antimigraine preparations' , 'CARDIOVASCULAR SYSTEM: PERIPHERAL VASODILATORS: PERIPHERAL 
                    # VASODILATORS: Other peripheral vasodilator' , 'ANTINEOPLASTIC AND IMMUNOMODULATING AGENTS: ANTINEO
                    # PLASTIC AGENTS: OTHER ANTINEOPLASTIC AGENTS: Methylhydrazines' , 'ANTIINFECTIVES FOR SYSTEMIC USE:
                    #  ANTIVIRALS FOR SYSTEMIC USE: DIRECT ACTING ANTIVIRALS: Protease inhibitors' , 'NERVOUS SYSTEM: PS
                    # YCHOANALEPTICS: PSYCHOSTIMULANTS, AGENTS USED FOR ADHD AND NOOTROPICS: Other psychostimulants and 
                    # nootropics' , 'NERVOUS SYSTEM: ANTI-PARKINSON DRUGS: DOPAMINERGIC AGENTS: Other dopaminergic agent
                    # s'
                }
            },
            'availability_type': 'NUMERIC',
            # EXAMPLES:
            # '-1' , '1' , '-1' , '1' , '0' , '-1' , '-2' , '1' , '1' , '1'
            'biotherapeutic': 
            {
                'properties': 
                {
                    'biocomponents': 
                    {
                        'properties': 
                        {
                            'component_id': 'NUMERIC',
                            # EXAMPLES:
                            # '20534' , '21483' , '20814' , '22144' , '20750' , '20752' , '20753' , '20924' , '20693' , 
                            # '20647'
                            'component_type': 'TEXT',
                            # EXAMPLES:
                            # 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTEIN' , 'PROTE
                            # IN' , 'PROTEIN' , 'PROTEIN'
                            'description': 'TEXT',
                            # EXAMPLES:
                            # 'Light chain' , 'Fusion Protein' , 'Light Chain' , 'Sequence' , 'Light chain' , 'Sequence'
                            #  , 'Light chain' , 'Light chain' , 'Light chain' , 'Light chain'
                            'organism': 'TEXT',
                            # EXAMPLES:
                            # 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo
                            #  sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens' , 'Homo sapiens'
                            'sequence': 'TEXT',
                            # EXAMPLES:
                            # 'DIQMTQSPSSLSASVGDRVTITCKASESVDNYGKSLMHWYQQKPGKAPKLLIYRASNLESGVPSRFSGSGSGTDFTLTISSLQPEDFAT
                            # YYCQQSNEDPWTFGGGTKVEIKRTVAAPSVFIFPPSDEQLKSGTASVVCLLNNFYPREAKVQWKVDNALQSGNSQESVTEQDSKDSTYSL
                            # SSTLTLSKADYEKHKVYACEVTHQGLSSPVTKSFNRGEC' , 'AMRSCPEEQYWDPLLGTCMSCKTICNHQSQRTCAAFCRSLSCRKEQ
                            # GKFYDHLLRDCISCASICGQHPKQCAYFCENKLRSEPKSSDKTHTCPPCPAPEAEGAPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHED
                            # PEVKFNWYVDGVEVHNAKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPSSIEKTISKAKGQPREPQVYTLPPSRDELTK
                            # NQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPGK' ,
                            #  'EVQLVESGGGLVQPGGSLRLSCAASGFTFSNYWMSWVRQAPGKGLEWVATIKQDGSQKNYVDSVKGRFTISRDNAKNSLYLRLNSLRA
                            # EDTAVYYCATELFDLWGRGSLVTVSSASTKGPSVFPLAPCSRSTSESTAALGCLVKDYFPEPVTVSWNSGALTSGVHTFPAVLQSSGLYS
                            # LSSVVTVPSSSLGTKTYTCNVDHKPSNTKVDKRVESKYGPPCPSCPAPEFLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSQEDPEVQ
                            # FNWYVDGVEVHNAKTKPREEQFNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKGLPSSIEKTISKAKGQPREPQVYTLPPSQEEMTKNQVS
                            # LTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLDSDGSFFLYSRLTVDKSRWQEGNVFSCSVMHEALHNHYTQKSLSLSLGK' , 'AV
                            # PPYASENQTCRDQEKEYYEPQHRICCSRCPPGTYVSAKCSRIRDTVCATCAENSYNEHWNYLTICQLCRPCDPVMGLEEIAPCTSKRKTQ
                            # CRCQPGMFCAAWALECTHCELLSDCPPGTEAELKDEVGKGNNHCVPCKAGHFQNTSSPSARCQPHTRCENQGLVEAAPGTAQSDTTCKNP
                            # LEPLPPEMSGTMVDKTHTCPPCPAPELLGGPSVFLFPPKPKDTLMISRTPEVTCVVVDVSHEDPEVKFNWYVDGVEVHNAKTKPREEQYN
                            # STYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQVYTLPPSRDELTKNQVSLTCLVKGFYPSDIAVEWESNGQP
                            # ENNYKTTPPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYTQKSLSLSPG' , 'DIQMTQSPSSLSASLGERVSLTCRAS
                            # QDIGSSLNWLQQGPDGTIKRLIYATSSLDSGVPKRFSGSRSGSDYSLTISSLESEDFVDYYCLQYVSSPPTFGAGTKLELKRADAAPSVF
                            # IFPPSDEQLKSGTASVVCLLNNFYPREAKVQWKVDNALQSGNSQESVTEQDSKDSTYSLSSTLTLSKADYEKHKVYACEVTHQGLSSPVT
                            # KSFNRGEC' , 'MHVAQPAVVLASSRGIASFVCEYASPGKYTEVRVTVLRQADSQVTEVCAATYMMGNELTFLDDSICTGTSSGNQVNL
                            # TIQGLRAMDTGLYICKVELMYPPPYYEGIGNGTQIYVIDPEPCPDSDQEPKSSDKTHTSPPSPAPELLGGSSVFLFPPKPKDTLMISRTP
                            # EVTCVVVDVSHEDPEVKFNWYVDGVEVHNAKTKPREEQYNSTYRVVSVLTVLHQDWLNGKEYKCKVSNKALPAPIEKTISKAKGQPREPQ
                            # VYTLPPSRDELTKNQVSLTCLVKGFYPSDIAVEWESNGQPENNYKTTPPVLDSDGSFFLYSKLTVDKSRWQQGNVFSCSVMHEALHNHYT
                            # QKSLSLSPGK' , 'DIQMTQSPSSLSASVGDRVTITCGTSEDIINYLNWYQQKPGKAPKLLIYHTSRLQSGVPSRFSGSGSGTDFTLTI
                            # SSLQPEDFATYYCQQGYTLPYTFGQGTKVEIKRTVAAPSVFIFPPSDEQLKSGTASVVCLLNNFYPREAKVQWKVDNALQSGNSQESVTE
                            # QDSKDSTYSLSSTLTLSKADYEKHKVYACEVTHQGLSSPVTKSFNRGEC' , 'DIQLTQSPASLAVSLGQRATISCKASQSVDYDGDSY
                            # LNWYQQIPGQPPKLLIYDASNLVSGIPPRFSGSGSGTDFTLNIHPVEKVDAATYHCQQSTEDPWTFGGGTKLEIKGGGGSGGGGSGGGGS
                            # QVQLQQSGAELVRPGSSVKISCKASGYAFSSYWMNWVKQRPGQGLEWIGQIWPGDGDTNYNGKFKGKATLTADESSSTAYMQLSSLASED
                            # SAVYFCARRETTTVGRYYYAMDYWGQGTTVTVSSGGGGSDIKLQQSGAELARPGASVKMSCKTSGYTFTRYTMHWVKQRPGQGLEWIGYI
                            # NPSRGYTNYNQKFKDKATLTTDKSSSTAYMQLSSLTSEDSAVYYCARYYDDHYCLDYWGQGTTLTVSSVEGGSGGSGGSGGSGGVDDIQL
                            # TQSPAIMSASPGEKVTMTCRASSSVSYMNWYQQKSGTSPKRWIYDTSKVASGVPYRFSGSGSGTSYSLTISSMEAEDAATYYCQQWSSNP
                            # LTFGAGTKLELKHHHHHH' , 'DIQMTQSPSSLSASVGDRVTITCKASQDVHTAVAWYQQKPGKAPKLLIYWASTRWTGVPSRFSGSGS
                            # GTDFTLTISSLQPEDFATYYCQQYSDYPWTFGGGTKVEIKRTVAAPSVFIFPPSDEQLKSGTASVVCLLNNFYPREAKVQWKVDNALQSG
                            # NSQESVTEQDSKDSTYSLSSTLTLSKADYEKHKVYACEVTHQGLSSPVTKSFNRGEC' , 'DIVLTQSPASLAVSLGQRATISCKASQS
                            # VDFDGDSYMNWYQQKPGQPPKVLIYAASNLESGIPARFSGSGSGTDFTLNIHPVEEEDAATYYCQQSNEDPWTFGGGTKLEIKRTVAAPS
                            # VFIFPPSDEQLKSGTASVVCLLNNFYPREAKVQWKVDNALQSGNSQESVTEQDSKDSTYSLSSTLTLSKADYEKHKVYACEVTHQGLSSP
                            # VTKSFNRGEC'
                            'tax_id': 'NUMERIC',
                            # EXAMPLES:
                            # '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606' , '9606'
                        }
                    },
                    'description': 'TEXT',
                    # EXAMPLES:
                    # 'Anrukinzumab (humanized mab)' , 'Atacicept (immunoadhesin)' , 'Atinumab (human mab)' , 'Baminerce
                    # pt (immunoadhesin)' , 'Bavituximab (chimeric mab)' , 'Belatacept (immunoadhesin)' , 'Benralizumab 
                    # (humanized mab)' , 'Blinatumumab (human BiTE)' , 'Blosozumab (humanized mab)' , 'Brentuximab Vedot
                    # in (chimeric mab)'
                    'helm_notation': 'TEXT',
                    # EXAMPLES:
                    # 'PEPTIDE1{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G.D.R.V.T.I.T.C.K.A.S.E.S.V.D.N.Y.G.K.S.L.M.H.W.Y.Q.Q.K.P.
                    # G.K.A.P.K.L.L.I.Y.R.A.S.N.L.E.S.G.V.P.S.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.Y.C.Q.
                    # Q.S.N.E.D.P.W.T.F.G.G.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.
                    # F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.
                    # K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{E.V.Q.L.V.E.S.G.G.G.L.V.Q.P.G.G.S.
                    # L.R.L.S.C.A.A.S.G.F.T.F.I.S.Y.A.M.S.W.V.R.Q.A.P.G.K.G.L.E.W.V.A.S.I.S.S.G.G.N.T.Y.Y.P.D.S.V.K.G.R.
                    # F.T.I.S.R.D.N.A.K.N.S.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.A.R.L.D.G.Y.Y.F.G.F.A.Y.W.G.Q.G.T.L.V.T.
                    # V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.
                    # T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.
                    # K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.A.L.G.A.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.
                    # V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.
                    # Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.
                    # K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.
                    # K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{E.V.Q.L.V.
                    # E.S.G.G.G.L.V.Q.P.G.G.S.L.R.L.S.C.A.A.S.G.F.T.F.I.S.Y.A.M.S.W.V.R.Q.A.P.G.K.G.L.E.W.V.A.S.I.S.S.G.
                    # G.N.T.Y.Y.P.D.S.V.K.G.R.F.T.I.S.R.D.N.A.K.N.S.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.A.R.L.D.G.Y.Y.F.
                    # G.F.A.Y.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.
                    # E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.
                    # N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.A.L.G.A.P.S.V.F.L.F.P.P.K.P.K.D.
                    # T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.
                    # T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.
                    # V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.
                    # V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.
                    # G.K}|PEPTIDE4{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G.D.R.V.T.I.T.C.K.A.S.E.S.V.D.N.Y.G.K.S.L.M.H.W.Y.Q.Q.
                    # K.P.G.K.A.P.K.L.L.I.Y.R.A.S.N.L.E.S.G.V.P.S.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.Y.
                    # C.Q.Q.S.N.E.D.P.W.T.F.G.G.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.
                    # N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.
                    # Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE2,PEPTIDE3,230:R3-230:R3|PEPTIDE
                    # 2,PEPTIDE1,221:R3-218:R3|PEPTIDE2,PEPTIDE3,227:R3-227:R3|PEPTIDE1,PEPTIDE1,23:R3-92:R3|PEPTIDE2,PE
                    # PTIDE2,145:R3-201:R3|PEPTIDE3,PEPTIDE4,221:R3-218:R3|PEPTIDE3,PEPTIDE3,22:R3-95:R3|PEPTIDE3,PEPTID
                    # E3,145:R3-201:R3|PEPTIDE2,PEPTIDE2,262:R3-322:R3|PEPTIDE2,PEPTIDE2,22:R3-95:R3|PEPTIDE3,PEPTIDE3,2
                    # 62:R3-322:R3|PEPTIDE2,PEPTIDE2,368:R3-426:R3|PEPTIDE4,PEPTIDE4,23:R3-92:R3|PEPTIDE4,PEPTIDE4,138:R
                    # 3-198:R3|PEPTIDE3,PEPTIDE3,368:R3-426:R3|PEPTIDE1,PEPTIDE1,138:R3-198:R3$$$' , 'PEPTIDE1{D.I.Q.M.T
                    # .Q.S.P.S.S.L.S.A.S.L.G.E.R.V.S.L.T.C.R.A.S.Q.D.I.G.S.S.L.N.W.L.Q.Q.G.P.D.G.T.I.K.R.L.I.Y.A.T.S.S.L
                    # .D.S.G.V.P.K.R.F.S.G.S.R.S.G.S.D.Y.S.L.T.I.S.S.L.E.S.E.D.F.V.D.Y.Y.C.L.Q.Y.V.S.S.P.P.T.F.G.A.G.T.K
                    # .L.E.L.K.R.A.D.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N
                    # .A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L
                    # .S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{E.V.Q.L.Q.Q.S.G.P.E.L.E.K.P.G.A.S.V.K.L.S.C.K.A.S.G.Y.S.F.T.G
                    # .Y.N.M.N.W.V.K.Q.S.H.G.K.S.L.E.W.I.G.H.I.D.P.Y.Y.G.D.T.S.Y.N.Q.K.F.R.G.K.A.T.L.T.V.D.K.S.S.S.T.A.Y
                    # .M.Q.L.K.S.L.T.S.E.D.S.A.V.Y.Y.C.V.K.G.G.Y.Y.G.H.W.Y.F.D.V.W.G.A.G.T.T.V.T.V.S.S.A.S.T.K.G.P.S.V.F
                    # .P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q
                    # .S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H
                    # .T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V
                    # .K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K
                    # .V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.D.E.L.T.K.N.Q.V.S.L.T.C.L.V.K.G
                    # .F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G
                    # .N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{E.V.Q.L.Q.Q.S.G.P.E.L.E.K.P.G.A.S
                    # .V.K.L.S.C.K.A.S.G.Y.S.F.T.G.Y.N.M.N.W.V.K.Q.S.H.G.K.S.L.E.W.I.G.H.I.D.P.Y.Y.G.D.T.S.Y.N.Q.K.F.R.G
                    # .K.A.T.L.T.V.D.K.S.S.S.T.A.Y.M.Q.L.K.S.L.T.S.E.D.S.A.V.Y.Y.C.V.K.G.G.Y.Y.G.H.W.Y.F.D.V.W.G.A.G.T.T
                    # .V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G
                    # .A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K
                    # .V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V
                    # .T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V
                    # .L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.D.E
                    # .L.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L
                    # .Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{D.I.Q
                    # .M.T.Q.S.P.S.S.L.S.A.S.L.G.E.R.V.S.L.T.C.R.A.S.Q.D.I.G.S.S.L.N.W.L.Q.Q.G.P.D.G.T.I.K.R.L.I.Y.A.T.S
                    # .S.L.D.S.G.V.P.K.R.F.S.G.S.R.S.G.S.D.Y.S.L.T.I.S.S.L.E.S.E.D.F.V.D.Y.Y.C.L.Q.Y.V.S.S.P.P.T.F.G.A.G
                    # .T.K.L.E.L.K.R.A.D.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V
                    # .D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q
                    # .G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE4,PEPTIDE4,23:R3-88:R3|PEPTIDE2,PEPTIDE3,229:R3-229:R3|PEPT
                    # IDE1,PEPTIDE1,134:R3-194:R3|PEPTIDE3,PEPTIDE4,223:R3-214:R3|PEPTIDE2,PEPTIDE3,232:R3-232:R3|PEPTID
                    # E2,PEPTIDE2,147:R3-203:R3|PEPTIDE1,PEPTIDE1,23:R3-88:R3|PEPTIDE3,PEPTIDE3,264:R3-324:R3|PEPTIDE4,P
                    # EPTIDE4,134:R3-194:R3|PEPTIDE2,PEPTIDE2,264:R3-324:R3|PEPTIDE3,PEPTIDE3,370:R3-428:R3|PEPTIDE3,PEP
                    # TIDE3,22:R3-96:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE2,PEPTIDE2,370:R3-428:R3|PEPTIDE3,PEPTIDE3,
                    # 147:R3-203:R3|PEPTIDE2,PEPTIDE1,223:R3-214:R3$$$' , 'PEPTIDE1{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G.D.R.
                    # V.T.I.T.C.G.T.S.E.D.I.I.N.Y.L.N.W.Y.Q.Q.K.P.G.K.A.P.K.L.L.I.Y.H.T.S.R.L.Q.S.G.V.P.S.R.F.S.G.S.G.S.
                    # G.T.D.F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.Y.C.Q.Q.G.Y.T.L.P.Y.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.
                    # I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.
                    # Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}
                    # |PEPTIDE2{E.V.Q.L.V.Q.S.G.A.E.V.K.K.P.G.A.S.V.K.V.S.C.K.A.S.G.Y.T.F.T.S.Y.V.I.H.W.V.R.Q.R.P.G.Q.G.
                    # L.A.W.M.G.Y.I.N.P.Y.N.D.G.T.K.Y.N.E.R.F.K.G.K.V.T.I.T.S.D.R.S.T.S.T.V.Y.M.E.L.S.S.L.R.S.E.D.T.A.V.
                    # Y.L.C.G.R.E.G.I.R.Y.Y.G.L.L.G.D.Y.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.
                    # T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.
                    # V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.
                    # G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.
                    # N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.
                    # T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.D.E.L.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.
                    # N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.
                    # H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{E.V.Q.L.V.Q.S.G.A.E.V.K.K.P.G.A.S.V.K.V.S.C.K.A.S.G.Y.T.F.
                    # T.S.Y.V.I.H.W.V.R.Q.R.P.G.Q.G.L.A.W.M.G.Y.I.N.P.Y.N.D.G.T.K.Y.N.E.R.F.K.G.K.V.T.I.T.S.D.R.S.T.S.T.
                    # V.Y.M.E.L.S.S.L.R.S.E.D.T.A.V.Y.L.C.G.R.E.G.I.R.Y.Y.G.L.L.G.D.Y.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.
                    # S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.
                    # V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.
                    # K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.
                    # P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.
                    # K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.D.E.L.T.K.N.Q.V.S.L.T.C.L.
                    # V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.
                    # Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.
                    # V.G.D.R.V.T.I.T.C.G.T.S.E.D.I.I.N.Y.L.N.W.Y.Q.Q.K.P.G.K.A.P.K.L.L.I.Y.H.T.S.R.L.Q.S.G.V.P.S.R.F.S.
                    # G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.Y.C.Q.Q.G.Y.T.L.P.Y.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.
                    # P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.
                    # S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.
                    # R.G.E.C}$PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE1,PEPTIDE1,134:R3-194:R3|PEPTIDE4,PEPTIDE4,23:R3-88:
                    # R3|PEPTIDE2,PEPTIDE3,233:R3-233:R3|PEPTIDE3,PEPTIDE4,224:R3-214:R3|PEPTIDE4,PEPTIDE4,134:R3-194:R3
                    # |PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE3,PEPTIDE3,371:R3-429:R3|PEPTIDE2,PEPTIDE2,265:R3-325:R3|PEP
                    # TIDE2,PEPTIDE3,230:R3-230:R3|PEPTIDE1,PEPTIDE1,23:R3-88:R3|PEPTIDE2,PEPTIDE2,148:R3-204:R3|PEPTIDE
                    # 2,PEPTIDE1,224:R3-214:R3|PEPTIDE2,PEPTIDE2,371:R3-429:R3|PEPTIDE3,PEPTIDE3,265:R3-325:R3|PEPTIDE3,
                    # PEPTIDE3,148:R3-204:R3$$$' , 'PEPTIDE1{Q.S.V.L.T.Q.P.P.S.V.S.G.A.P.G.Q.R.V.T.I.S.C.S.G.S.R.S.N.I.G
                    # .S.N.T.V.K.W.Y.Q.Q.L.P.G.T.A.P.K.L.L.I.Y.Y.N.D.Q.R.P.S.G.V.P.D.R.F.S.G.S.K.S.G.T.S.A.S.L.A.I.T.G.L
                    # .Q.A.E.D.E.A.D.Y.Y.C.Q.S.Y.D.R.Y.T.H.P.A.L.L.F.G.T.G.T.K.V.T.V.L.G.Q.P.K.A.A.P.S.V.T.L.F.P.P.S.S.E
                    # .E.L.Q.A.N.K.A.T.L.V.C.L.I.S.D.F.Y.P.G.A.V.T.V.A.W.K.A.D.S.S.P.V.K.A.G.V.E.T.T.T.P.S.K.Q.S.N.N.K.Y
                    # .A.A.S.S.Y.L.S.L.T.P.E.Q.W.K.S.H.R.S.Y.S.C.Q.V.T.H.E.G.S.T.V.E.K.T.V.A.P.T.E.C.S}|PEPTIDE2{Q.V.Q.L
                    # .V.E.S.G.G.G.V.V.Q.P.G.R.S.L.R.L.S.C.A.A.S.G.F.T.F.S.S.Y.G.M.H.W.V.R.Q.A.P.G.K.G.L.E.W.V.A.F.I.R.Y
                    # .D.G.S.N.K.Y.Y.A.D.S.V.K.G.R.F.T.I.S.R.D.N.S.K.N.T.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.K.T.H.G.S.H
                    # .D.N.W.G.Q.G.T.M.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P
                    # .V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V
                    # .N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L
                    # .M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y
                    # .R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y
                    # .T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L
                    # .D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K
                    # }|PEPTIDE3{Q.V.Q.L.V.E.S.G.G.G.V.V.Q.P.G.R.S.L.R.L.S.C.A.A.S.G.F.T.F.S.S.Y.G.M.H.W.V.R.Q.A.P.G.K.G
                    # .L.E.W.V.A.F.I.R.Y.D.G.S.N.K.Y.Y.A.D.S.V.K.G.R.F.T.I.S.R.D.N.S.K.N.T.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V
                    # .Y.Y.C.K.T.H.G.S.H.D.N.W.G.Q.G.T.M.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C
                    # .L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L
                    # .G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L
                    # .F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P
                    # .R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K
                    # .G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N
                    # .N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q
                    # .K.S.L.S.L.S.P.G.K}|PEPTIDE4{Q.S.V.L.T.Q.P.P.S.V.S.G.A.P.G.Q.R.V.T.I.S.C.S.G.S.R.S.N.I.G.S.N.T.V.K
                    # .W.Y.Q.Q.L.P.G.T.A.P.K.L.L.I.Y.Y.N.D.Q.R.P.S.G.V.P.D.R.F.S.G.S.K.S.G.T.S.A.S.L.A.I.T.G.L.Q.A.E.D.E
                    # .A.D.Y.Y.C.Q.S.Y.D.R.Y.T.H.P.A.L.L.F.G.T.G.T.K.V.T.V.L.G.Q.P.K.A.A.P.S.V.T.L.F.P.P.S.S.E.E.L.Q.A.N
                    # .K.A.T.L.V.C.L.I.S.D.F.Y.P.G.A.V.T.V.A.W.K.A.D.S.S.P.V.K.A.G.V.E.T.T.T.P.S.K.Q.S.N.N.K.Y.A.A.S.S.Y
                    # .L.S.L.T.P.E.Q.W.K.S.H.R.S.Y.S.C.Q.V.T.H.E.G.S.T.V.E.K.T.V.A.P.T.E.C.S}$PEPTIDE1,PEPTIDE1,139:R3-1
                    # 98:R3|PEPTIDE2,PEPTIDE2,259:R3-319:R3|PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE3,PEPTIDE3,142:R3-198:R
                    # 3|PEPTIDE4,PEPTIDE4,139:R3-198:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE3,PEPTIDE3,259:R3-319:R3|PE
                    # PTIDE2,PEPTIDE3,227:R3-227:R3|PEPTIDE2,PEPTIDE3,224:R3-224:R3|PEPTIDE3,PEPTIDE4,218:R3-216:R3|PEPT
                    # IDE2,PEPTIDE2,142:R3-198:R3|PEPTIDE3,PEPTIDE3,365:R3-423:R3|PEPTIDE2,PEPTIDE1,218:R3-216:R3|PEPTID
                    # E2,PEPTIDE2,365:R3-423:R3|PEPTIDE4,PEPTIDE4,22:R3-89:R3|PEPTIDE1,PEPTIDE1,22:R3-89:R3$$$' , 'PEPTI
                    # DE1{E.I.V.L.T.Q.S.P.A.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.S.V.S.D.A.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.R.L.
                    # L.I.Y.D.A.S.S.R.A.T.G.V.P.A.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.E.P.E.D.F.A.V.Y.Y.C.H.Q.Y.I.Q.L.H.
                    # S.F.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.
                    # A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.
                    # A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{Q.V.Q.L.V.Q.S.G.A.E.V.K.K.P.G.S.S.V.K.V.S.C.
                    # K.A.S.G.G.T.F.S.S.Y.G.I.S.W.V.R.Q.A.P.G.Q.G.L.E.W.M.G.G.I.I.P.I.F.G.T.A.N.Y.A.Q.K.F.Q.G.R.V.T.I.T.
                    # A.D.E.S.T.S.T.A.Y.M.E.L.S.S.L.R.S.E.D.T.A.V.Y.Y.C.A.R.Y.D.G.I.Y.G.E.L.D.F.W.G.Q.G.T.L.V.T.V.S.S.A.
                    # S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.
                    # H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.
                    # P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.
                    # V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.
                    # N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.D.E.L.T.K.N.Q.V.
                    # S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.
                    # D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{Q.V.Q.L.V.Q.S.G.A.
                    # E.V.K.K.P.G.S.S.V.K.V.S.C.K.A.S.G.G.T.F.S.S.Y.G.I.S.W.V.R.Q.A.P.G.Q.G.L.E.W.M.G.G.I.I.P.I.F.G.T.A.
                    # N.Y.A.Q.K.F.Q.G.R.V.T.I.T.A.D.E.S.T.S.T.A.Y.M.E.L.S.S.L.R.S.E.D.T.A.V.Y.Y.C.A.R.Y.D.G.I.Y.G.E.L.D.
                    # F.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.
                    # T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.
                    # H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.
                    # I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.
                    # V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.
                    # L.P.P.S.R.D.E.L.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.
                    # S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|P
                    # EPTIDE4{E.I.V.L.T.Q.S.P.A.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.S.V.S.D.A.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.
                    # R.L.L.I.Y.D.A.S.S.R.A.T.G.V.P.A.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.E.P.E.D.F.A.V.Y.Y.C.H.Q.Y.I.Q.
                    # L.H.S.F.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.
                    # R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.
                    # V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE3,PEPTIDE3,1
                    # 46:R3-202:R3|PEPTIDE2,PEPTIDE3,231:R3-231:R3|PEPTIDE2,PEPTIDE2,146:R3-202:R3|PEPTIDE3,PEPTIDE3,369
                    # :R3-427:R3|PEPTIDE3,PEPTIDE4,222:R3-216:R3|PEPTIDE1,PEPTIDE1,136:R3-196:R3|PEPTIDE4,PEPTIDE4,23:R3
                    # -89:R3|PEPTIDE3,PEPTIDE3,263:R3-323:R3|PEPTIDE1,PEPTIDE1,23:R3-89:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3
                    # |PEPTIDE2,PEPTIDE3,228:R3-228:R3|PEPTIDE2,PEPTIDE2,263:R3-323:R3|PEPTIDE4,PEPTIDE4,136:R3-196:R3|P
                    # EPTIDE2,PEPTIDE1,222:R3-216:R3|PEPTIDE2,PEPTIDE2,369:R3-427:R3$$$' , 'PEPTIDE1{S.S.E.L.T.Q.D.P.A.V
                    # .S.V.A.L.G.Q.T.V.R.I.T.C.Q.G.D.S.L.R.S.Y.Y.A.T.W.Y.Q.Q.K.P.G.Q.A.P.I.L.V.I.Y.G.E.N.K.R.P.S.G.I.P.D
                    # .R.F.S.G.S.S.S.G.N.T.A.S.L.T.I.T.G.A.Q.A.E.D.E.A.D.Y.Y.C.K.S.R.D.G.S.G.Q.H.L.V.F.G.G.G.T.K.L.T.V.L
                    # .G.Q.P.K.A.A.P.S.V.T.L.F.P.P.S.S.E.E.L.Q.A.N.K.A.T.L.V.C.L.I.S.D.F.Y.P.G.A.V.T.V.A.W.K.A.D.S.S.P.V
                    # .K.A.G.V.E.T.T.T.P.S.K.Q.S.N.N.K.Y.A.A.S.S.Y.L.S.L.T.P.E.Q.W.K.S.H.R.S.Y.S.C.Q.V.T.H.E.G.S.T.V.E.K
                    # .T.V.A.P.A.E.C.S}|PEPTIDE2{E.V.Q.L.V.Q.S.G.A.E.V.K.K.P.G.S.S.V.K.V.S.C.K.A.S.G.G.T.F.S.S.Y.A.I.S.W
                    # .V.R.Q.A.P.G.Q.G.L.E.W.M.G.G.I.I.P.I.F.G.T.A.N.Y.A.Q.K.F.Q.G.R.V.T.I.T.A.D.K.S.T.S.T.A.Y.M.E.L.S.S
                    # .L.R.S.E.D.T.A.V.Y.Y.C.A.R.A.P.L.R.F.L.E.W.S.T.Q.D.H.Y.Y.Y.Y.Y.M.D.V.W.G.K.G.T.T.V.T.V.S.S.A.S.T.K
                    # .G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F
                    # .P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S
                    # .C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H
                    # .E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K
                    # .E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T
                    # .C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S
                    # .R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{E.V.Q.L.V.Q.S.G.A.E.V.K
                    # .K.P.G.S.S.V.K.V.S.C.K.A.S.G.G.T.F.S.S.Y.A.I.S.W.V.R.Q.A.P.G.Q.G.L.E.W.M.G.G.I.I.P.I.F.G.T.A.N.Y.A
                    # .Q.K.F.Q.G.R.V.T.I.T.A.D.K.S.T.S.T.A.Y.M.E.L.S.S.L.R.S.E.D.T.A.V.Y.Y.C.A.R.A.P.L.R.F.L.E.W.S.T.Q.D
                    # .H.Y.Y.Y.Y.Y.M.D.V.W.G.K.G.T.T.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V
                    # .K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T
                    # .Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P
                    # .P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E
                    # .E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q
                    # .P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y
                    # .K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S
                    # .L.S.L.S.P.G.K}|PEPTIDE4{S.S.E.L.T.Q.D.P.A.V.S.V.A.L.G.Q.T.V.R.I.T.C.Q.G.D.S.L.R.S.Y.Y.A.T.W.Y.Q.Q
                    # .K.P.G.Q.A.P.I.L.V.I.Y.G.E.N.K.R.P.S.G.I.P.D.R.F.S.G.S.S.S.G.N.T.A.S.L.T.I.T.G.A.Q.A.E.D.E.A.D.Y.Y
                    # .C.K.S.R.D.G.S.G.Q.H.L.V.F.G.G.G.T.K.L.T.V.L.G.Q.P.K.A.A.P.S.V.T.L.F.P.P.S.S.E.E.L.Q.A.N.K.A.T.L.V
                    # .C.L.I.S.D.F.Y.P.G.A.V.T.V.A.W.K.A.D.S.S.P.V.K.A.G.V.E.T.T.T.P.S.K.Q.S.N.N.K.Y.A.A.S.S.Y.L.S.L.T.P
                    # .E.Q.W.K.S.H.R.S.Y.S.C.Q.V.T.H.E.G.S.T.V.E.K.T.V.A.P.A.E.C.S}$PEPTIDE2,PEPTIDE1,233:R3-213:R3|PEPT
                    # IDE3,PEPTIDE3,157:R3-213:R3|PEPTIDE2,PEPTIDE2,380:R3-438:R3|PEPTIDE2,PEPTIDE3,239:R3-239:R3|PEPTID
                    # E3,PEPTIDE3,22:R3-96:R3|PEPTIDE4,PEPTIDE4,136:R3-195:R3|PEPTIDE2,PEPTIDE3,242:R3-242:R3|PEPTIDE3,P
                    # EPTIDE3,380:R3-438:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE1,PEPTIDE1,136:R3-195:R3|PEPTIDE3,PEPTI
                    # DE4,233:R3-213:R3|PEPTIDE2,PEPTIDE2,157:R3-213:R3|PEPTIDE3,PEPTIDE3,274:R3-334:R3|PEPTIDE1,PEPTIDE
                    # 1,22:R3-87:R3|PEPTIDE2,PEPTIDE2,274:R3-334:R3|PEPTIDE4,PEPTIDE4,22:R3-87:R3$$$' , 'PEPTIDE1{E.I.V.
                    # L.T.Q.S.P.G.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.G.I.S.R.S.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.S.L.L.I.Y.G.A.
                    # S.S.R.A.T.G.I.P.D.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.R.L.E.P.E.D.F.A.V.Y.Y.C.Q.Q.F.G.S.S.P.W.T.F.G.Q.
                    # G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.
                    # V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.
                    # Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{Q.V.Q.L.Q.E.S.G.P.G.L.V.K.P.S.Q.T.L.S.L.T.C.T.V.S.G.G.S.
                    # I.S.S.G.D.Y.F.W.S.W.I.R.Q.L.P.G.K.G.L.E.W.I.G.H.I.H.N.S.G.T.T.Y.Y.N.P.S.L.K.S.R.V.T.I.S.V.D.T.S.K.
                    # K.Q.F.S.L.R.L.S.S.V.T.A.A.D.T.A.V.Y.Y.C.A.R.D.R.G.G.D.Y.Y.Y.G.M.D.V.W.G.Q.G.T.T.V.T.V.S.S.A.S.T.K.
                    # G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.
                    # P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.
                    # C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.
                    # E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.
                    # E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.
                    # C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.
                    # R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{Q.V.Q.L.Q.E.S.G.P.G.L.V.
                    # K.P.S.Q.T.L.S.L.T.C.T.V.S.G.G.S.I.S.S.G.D.Y.F.W.S.W.I.R.Q.L.P.G.K.G.L.E.W.I.G.H.I.H.N.S.G.T.T.Y.Y.
                    # N.P.S.L.K.S.R.V.T.I.S.V.D.T.S.K.K.Q.F.S.L.R.L.S.S.V.T.A.A.D.T.A.V.Y.Y.C.A.R.D.R.G.G.D.Y.Y.Y.G.M.D.
                    # V.W.G.Q.G.T.T.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.
                    # T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.
                    # H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.
                    # I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.
                    # V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.
                    # L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.
                    # S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|P
                    # EPTIDE4{E.I.V.L.T.Q.S.P.G.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.G.I.S.R.S.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.
                    # S.L.L.I.Y.G.A.S.S.R.A.T.G.I.P.D.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.R.L.E.P.E.D.F.A.V.Y.Y.C.Q.Q.F.G.S.
                    # S.P.W.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.
                    # E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.
                    # Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE3,PEPTIDE3,266:R3-326:R3|PEPTIDE1,PEPTIDE1,2
                    # 3:R3-89:R3|PEPTIDE1,PEPTIDE1,135:R3-195:R3|PEPTIDE2,PEPTIDE3,231:R3-231:R3|PEPTIDE4,PEPTIDE4,23:R3
                    # -89:R3|PEPTIDE2,PEPTIDE3,234:R3-234:R3|PEPTIDE2,PEPTIDE2,372:R3-430:R3|PEPTIDE2,PEPTIDE1,225:R3-21
                    # 5:R3|PEPTIDE4,PEPTIDE4,135:R3-195:R3|PEPTIDE3,PEPTIDE4,225:R3-215:R3|PEPTIDE3,PEPTIDE3,22:R3-97:R3
                    # |PEPTIDE2,PEPTIDE2,266:R3-326:R3|PEPTIDE2,PEPTIDE2,149:R3-205:R3|PEPTIDE3,PEPTIDE3,149:R3-205:R3|P
                    # EPTIDE3,PEPTIDE3,372:R3-430:R3|PEPTIDE2,PEPTIDE2,22:R3-97:R3$$$' , 'PEPTIDE1{D.I.Q.M.T.Q.S.P.S.S.L
                    # .S.A.S.V.G.D.R.V.T.I.T.C.R.S.S.Q.S.L.V.H.S.N.G.N.T.F.L.H.W.Y.Q.Q.K.P.G.K.A.P.K.L.L.I.Y.T.V.S.N.R.F
                    # .S.G.V.P.S.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.F.C.S.Q.T.T.H.V.P.W.T.F.G.Q.G.T.K.V
                    # .E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A
                    # .L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S
                    # .S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{E.V.Q.L.V.E.S.G.G.G.L.V.Q.P.G.G.S.L.R.L.S.C.A.A.S.G.Y.S.F.T.G.Y
                    # .Y.I.H.W.V.R.Q.A.P.G.K.G.L.E.W.V.A.R.V.I.P.N.A.G.G.T.S.Y.N.Q.K.F.K.G.R.F.T.L.S.V.D.N.S.K.N.T.A.Y.L
                    # .Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.A.R.E.G.I.Y.W.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K
                    # .S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L
                    # .S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A
                    # .P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D
                    # .G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P
                    # .A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A
                    # .V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V
                    # .M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{E.V.Q.L.V.E.S.G.G.G.L.V.Q.P.G.G.S.L.R.L.S.C.A.A
                    # .S.G.Y.S.F.T.G.Y.Y.I.H.W.V.R.Q.A.P.G.K.G.L.E.W.V.A.R.V.I.P.N.A.G.G.T.S.Y.N.Q.K.F.K.G.R.F.T.L.S.V.D
                    # .N.S.K.N.T.A.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.A.R.E.G.I.Y.W.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V
                    # .F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L
                    # .Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T
                    # .H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E
                    # .V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C
                    # .K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K
                    # .G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q
                    # .G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G
                    # .D.R.V.T.I.T.C.R.S.S.Q.S.L.V.H.S.N.G.N.T.F.L.H.W.Y.Q.Q.K.P.G.K.A.P.K.L.L.I.Y.T.V.S.N.R.F.S.G.V.P.S
                    # .R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.F.C.S.Q.T.T.H.V.P.W.T.F.G.Q.G.T.K.V.E.I.K.R.T
                    # .V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N
                    # .S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K
                    # .S.F.N.R.G.E.C}$PEPTIDE2,PEPTIDE1,217:R3-219:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE1,PEPTIDE1,13
                    # 9:R3-199:R3|PEPTIDE1,PEPTIDE1,23:R3-93:R3|PEPTIDE2,PEPTIDE2,141:R3-197:R3|PEPTIDE4,PEPTIDE4,139:R3
                    # -199:R3|PEPTIDE2,PEPTIDE2,364:R3-422:R3|PEPTIDE3,PEPTIDE3,141:R3-197:R3|PEPTIDE2,PEPTIDE3,223:R3-2
                    # 23:R3|PEPTIDE3,PEPTIDE3,258:R3-318:R3|PEPTIDE4,PEPTIDE4,23:R3-93:R3|PEPTIDE3,PEPTIDE3,22:R3-96:R3|
                    # PEPTIDE2,PEPTIDE2,258:R3-318:R3|PEPTIDE3,PEPTIDE4,217:R3-219:R3|PEPTIDE2,PEPTIDE3,226:R3-226:R3|PE
                    # PTIDE3,PEPTIDE3,364:R3-422:R3$$$' , 'PEPTIDE1{D.I.V.M.T.Q.S.P.L.S.L.P.V.T.P.G.Q.P.A.S.I.S.C.R.S.S.
                    # Q.S.I.V.H.S.N.G.N.T.W.L.Q.W.W.L.Q.K.P.G.Q.S.P.Q.L.L.I.W.K.V.S.N.R.L.W.G.V.P.D.R.F.S.G.S.G.S.G.T.D.
                    # F.T.L.K.I.S.R.V.Q.A.Q.D.V.G.V.W.W.C.F.Q.G.S.H.V.P.W.T.F.G.Q.G.T.K.V.Q.I.K.R.T.V.A.A.P.S.V.F.I.F.P.
                    # P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.
                    # K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTI
                    # DE2{Q.V.Q.L.Q.Q.S.G.P.G.L.V.K.P.S.Q.T.L.S.L.T.C.T.V.S.G.W.S.I.S.G.G.W.L.W.N.W.I.R.Q.P.P.G.K.G.L.Q.
                    # W.I.G.W.I.S.W.D.G.T.N.N.W.K.P.S.L.K.D.R.V.T.I.S.V.D.T.S.K.N.Q.F.S.L.K.L.S.S.V.T.A.A.D.T.A.V.W.W.C.
                    # A.R.W.G.R.V.F.F.D.W.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.
                    # V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.
                    # T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.
                    # P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.
                    # E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.
                    # Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.
                    # Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.
                    # S.L.S.L.S.P.G.K}|PEPTIDE3{Q.V.Q.L.Q.Q.S.G.P.G.L.V.K.P.S.Q.T.L.S.L.T.C.T.V.S.G.W.S.I.S.G.G.W.L.W.N.
                    # W.I.R.Q.P.P.G.K.G.L.Q.W.I.G.W.I.S.W.D.G.T.N.N.W.K.P.S.L.K.D.R.V.T.I.S.V.D.T.S.K.N.Q.F.S.L.K.L.S.S.
                    # V.T.A.A.D.T.A.V.W.W.C.A.R.W.G.R.V.F.F.D.W.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.
                    # T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.
                    # S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.
                    # E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.
                    # V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.
                    # P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.
                    # E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.
                    # H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{D.I.V.M.T.Q.S.P.L.S.L.P.V.T.P.G.Q.P.A.S.I.S.C.R.S.
                    # S.Q.S.I.V.H.S.N.G.N.T.W.L.Q.W.W.L.Q.K.P.G.Q.S.P.Q.L.L.I.W.K.V.S.N.R.L.W.G.V.P.D.R.F.S.G.S.G.S.G.T.
                    # D.F.T.L.K.I.S.R.V.Q.A.Q.D.V.G.V.W.W.C.F.Q.G.S.H.V.P.W.T.F.G.Q.G.T.K.V.Q.I.K.R.T.V.A.A.P.S.V.F.I.F.
                    # P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.
                    # S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEP
                    # TIDE4,PEPTIDE4,139:R3-199:R3|PEPTIDE2,PEPTIDE1,220:R3-219:R3|PEPTIDE3,PEPTIDE3,367:R3-425:R3|PEPTI
                    # DE2,PEPTIDE3,226:R3-226:R3|PEPTIDE1,PEPTIDE1,23:R3-93:R3|PEPTIDE3,PEPTIDE4,220:R3-219:R3|PEPTIDE1,
                    # PEPTIDE1,139:R3-199:R3|PEPTIDE4,PEPTIDE4,23:R3-93:R3|PEPTIDE2,PEPTIDE2,367:R3-425:R3|PEPTIDE2,PEPT
                    # IDE2,144:R3-200:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE2,PEPTIDE2,261:R3-321:R3|PEPTIDE3,PEPTIDE3
                    # ,22:R3-96:R3|PEPTIDE2,PEPTIDE3,229:R3-229:R3|PEPTIDE3,PEPTIDE3,261:R3-321:R3|PEPTIDE3,PEPTIDE3,144
                    # :R3-200:R3$$$' , 'PEPTIDE1{E.I.V.L.T.Q.S.P.A.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.S.V.S.S.Y.L.A.W.Y
                    # .Q.Q.K.P.G.Q.A.P.R.L.L.I.Y.D.A.S.N.R.A.T.G.I.P.A.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.E.P.E.D.F.A.V
                    # .Y.Y.C.Q.Q.R.S.N.W.P.P.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C
                    # .L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K
                    # .A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{E.V.Q.L.L.E.S.G.G.G.L.V.Q
                    # .P.G.G.S.L.R.L.S.C.A.V.S.G.F.T.F.N.S.F.A.M.S.W.V.R.Q.A.P.G.K.G.L.E.W.V.S.A.I.S.G.S.G.G.G.T.Y.Y.A.D
                    # .S.V.K.G.R.F.T.I.S.R.D.N.S.K.N.T.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.F.C.A.K.D.K.I.L.W.F.G.E.P.V.F.D.Y
                    # .W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T
                    # .V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H
                    # .K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I
                    # .S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V
                    # .V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L
                    # .P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S
                    # .D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PE
                    # PTIDE3{E.V.Q.L.L.E.S.G.G.G.L.V.Q.P.G.G.S.L.R.L.S.C.A.V.S.G.F.T.F.N.S.F.A.M.S.W.V.R.Q.A.P.G.K.G.L.E
                    # .W.V.S.A.I.S.G.S.G.G.G.T.Y.Y.A.D.S.V.K.G.R.F.T.I.S.R.D.N.S.K.N.T.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.F
                    # .C.A.K.D.K.I.L.W.F.G.E.P.V.F.D.Y.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T
                    # .A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V
                    # .P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G
                    # .P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N
                    # .A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T
                    # .I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N
                    # .G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H
                    # .N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{E.I.V.L.T.Q.S.P.A.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.S.V.S
                    # .S.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.R.L.L.I.Y.D.A.S.N.R.A.T.G.I.P.A.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.E
                    # .P.E.D.F.A.V.Y.Y.C.Q.Q.R.S.N.W.P.P.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G
                    # .T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S
                    # .T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE4,PEPTIDE4,134:
                    # R3-194:R3|PEPTIDE3,PEPTIDE3,266:R3-326:R3|PEPTIDE3,PEPTIDE4,225:R3-214:R3|PEPTIDE2,PEPTIDE2,372:R3
                    # -430:R3|PEPTIDE1,PEPTIDE1,23:R3-88:R3|PEPTIDE2,PEPTIDE3,231:R3-231:R3|PEPTIDE2,PEPTIDE2,149:R3-205
                    # :R3|PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE1,PEPTIDE1,134:R3-194:R3|PEPTIDE2,PEPTIDE3,234:R3-234:R3|
                    # PEPTIDE3,PEPTIDE3,372:R3-430:R3|PEPTIDE4,PEPTIDE4,23:R3-88:R3|PEPTIDE2,PEPTIDE1,225:R3-214:R3|PEPT
                    # IDE3,PEPTIDE3,149:R3-205:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE2,PEPTIDE2,266:R3-326:R3$$$'
                    'molecule_chembl_id': 'TEXT',
                    # EXAMPLES:
                    # 'CHEMBL1742985' , 'CHEMBL1742986' , 'CHEMBL1742987' , 'CHEMBL1742988' , 'CHEMBL1742989' , 'CHEMBL1
                    # 742990' , 'CHEMBL1742991' , 'CHEMBL1742992' , 'CHEMBL1742993' , 'CHEMBL1742994'
                }
            },
            'black_box': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'black_box_warning': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'chirality': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '2' , '2' , '0' , '1' , '1' , '0' , '1' , '2'
            'drug_type': 'NUMERIC',
            # EXAMPLES:
            # '6' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
            'first_approval': 'NUMERIC',
            # EXAMPLES:
            # '1991' , '1988' , '1987' , '1969' , '2006' , '1997' , '1974' , '2011' , '2017' , '2014'
            'first_in_class': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'helm_notation': 'TEXT',
            # EXAMPLES:
            # 'PEPTIDE1{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G.D.R.V.T.I.T.C.K.A.S.E.S.V.D.N.Y.G.K.S.L.M.H.W.Y.Q.Q.K.P.G.K.A.P.
            # K.L.L.I.Y.R.A.S.N.L.E.S.G.V.P.S.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.Y.C.Q.Q.S.N.E.D.P.W.T.
            # F.G.G.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.
            # D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.
            # P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{E.V.Q.L.V.E.S.G.G.G.L.V.Q.P.G.G.S.L.R.L.S.C.A.A.S.G.F.T.F.I.S.Y.A.M.S.W.V.
            # R.Q.A.P.G.K.G.L.E.W.V.A.S.I.S.S.G.G.N.T.Y.Y.P.D.S.V.K.G.R.F.T.I.S.R.D.N.A.K.N.S.L.Y.L.Q.M.N.S.L.R.A.E.D.T.
            # A.V.Y.Y.C.A.R.L.D.G.Y.Y.F.G.F.A.Y.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.
            # G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.
            # Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.A.L.G.A.P.S.V.F.L.F.P.P.K.P.K.
            # D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.
            # V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.
            # R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.
            # S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{E.V.Q.L.V.E.S.G.
            # G.G.L.V.Q.P.G.G.S.L.R.L.S.C.A.A.S.G.F.T.F.I.S.Y.A.M.S.W.V.R.Q.A.P.G.K.G.L.E.W.V.A.S.I.S.S.G.G.N.T.Y.Y.P.D.
            # S.V.K.G.R.F.T.I.S.R.D.N.A.K.N.S.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.A.R.L.D.G.Y.Y.F.G.F.A.Y.W.G.Q.G.T.L.V.
            # T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.
            # V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.
            # C.D.K.T.H.T.C.P.P.C.P.A.P.E.A.L.G.A.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.
            # V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.
            # K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.
            # V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.
            # L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G.D.R.V.T.I.T.C.K.A.S.E.S.V.D.N.Y.
            # G.K.S.L.M.H.W.Y.Q.Q.K.P.G.K.A.P.K.L.L.I.Y.R.A.S.N.L.E.S.G.V.P.S.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.Q.P.E.
            # D.F.A.T.Y.Y.C.Q.Q.S.N.E.D.P.W.T.F.G.G.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.
            # L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.
            # K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE2,PEPTIDE3,230:R3-230:R3|PEPTIDE2,PEPTIDE1,2
            # 21:R3-218:R3|PEPTIDE2,PEPTIDE3,227:R3-227:R3|PEPTIDE1,PEPTIDE1,23:R3-92:R3|PEPTIDE2,PEPTIDE2,145:R3-201:R3
            # |PEPTIDE3,PEPTIDE4,221:R3-218:R3|PEPTIDE3,PEPTIDE3,22:R3-95:R3|PEPTIDE3,PEPTIDE3,145:R3-201:R3|PEPTIDE2,PE
            # PTIDE2,262:R3-322:R3|PEPTIDE2,PEPTIDE2,22:R3-95:R3|PEPTIDE3,PEPTIDE3,262:R3-322:R3|PEPTIDE2,PEPTIDE2,368:R
            # 3-426:R3|PEPTIDE4,PEPTIDE4,23:R3-92:R3|PEPTIDE4,PEPTIDE4,138:R3-198:R3|PEPTIDE3,PEPTIDE3,368:R3-426:R3|PEP
            # TIDE1,PEPTIDE1,138:R3-198:R3$$$' , 'PEPTIDE1{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.L.G.E.R.V.S.L.T.C.R.A.S.Q.D.I.G.S
            # .S.L.N.W.L.Q.Q.G.P.D.G.T.I.K.R.L.I.Y.A.T.S.S.L.D.S.G.V.P.K.R.F.S.G.S.R.S.G.S.D.Y.S.L.T.I.S.S.L.E.S.E.D.F.V
            # .D.Y.Y.C.L.Q.Y.V.S.S.P.P.T.F.G.A.G.T.K.L.E.L.K.R.A.D.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N
            # .N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K
            # .V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{E.V.Q.L.Q.Q.S.G.P.E.L.E.K.P.G.A.S.V.K.L.S.C.K.A
            # .S.G.Y.S.F.T.G.Y.N.M.N.W.V.K.Q.S.H.G.K.S.L.E.W.I.G.H.I.D.P.Y.Y.G.D.T.S.Y.N.Q.K.F.R.G.K.A.T.L.T.V.D.K.S.S.S
            # .T.A.Y.M.Q.L.K.S.L.T.S.E.D.S.A.V.Y.Y.C.V.K.G.G.Y.Y.G.H.W.Y.F.D.V.W.G.A.G.T.T.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P
            # .L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y
            # .S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E
            # .L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N
            # .A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A
            # .K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.D.E.L.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K
            # .T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P
            # .G.K}|PEPTIDE3{E.V.Q.L.Q.Q.S.G.P.E.L.E.K.P.G.A.S.V.K.L.S.C.K.A.S.G.Y.S.F.T.G.Y.N.M.N.W.V.K.Q.S.H.G.K.S.L.E
            # .W.I.G.H.I.D.P.Y.Y.G.D.T.S.Y.N.Q.K.F.R.G.K.A.T.L.T.V.D.K.S.S.S.T.A.Y.M.Q.L.K.S.L.T.S.E.D.S.A.V.Y.Y.C.V.K.G
            # .G.Y.Y.G.H.W.Y.F.D.V.W.G.A.G.T.T.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y
            # .F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V
            # .N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R
            # .T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V
            # .L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.D.E.L.T.K.N
            # .Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K
            # .S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.L
            # .G.E.R.V.S.L.T.C.R.A.S.Q.D.I.G.S.S.L.N.W.L.Q.Q.G.P.D.G.T.I.K.R.L.I.Y.A.T.S.S.L.D.S.G.V.P.K.R.F.S.G.S.R.S.G
            # .S.D.Y.S.L.T.I.S.S.L.E.S.E.D.F.V.D.Y.Y.C.L.Q.Y.V.S.S.P.P.T.F.G.A.G.T.K.L.E.L.K.R.A.D.A.A.P.S.V.F.I.F.P.P.S
            # .D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S
            # .L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE4,PEPTIDE4,23:R3-
            # 88:R3|PEPTIDE2,PEPTIDE3,229:R3-229:R3|PEPTIDE1,PEPTIDE1,134:R3-194:R3|PEPTIDE3,PEPTIDE4,223:R3-214:R3|PEPT
            # IDE2,PEPTIDE3,232:R3-232:R3|PEPTIDE2,PEPTIDE2,147:R3-203:R3|PEPTIDE1,PEPTIDE1,23:R3-88:R3|PEPTIDE3,PEPTIDE
            # 3,264:R3-324:R3|PEPTIDE4,PEPTIDE4,134:R3-194:R3|PEPTIDE2,PEPTIDE2,264:R3-324:R3|PEPTIDE3,PEPTIDE3,370:R3-4
            # 28:R3|PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE2,PEPTIDE2,370:R3-428:R3|PEPTIDE3
            # ,PEPTIDE3,147:R3-203:R3|PEPTIDE2,PEPTIDE1,223:R3-214:R3$$$' , 'PEPTIDE1{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G.D.
            # R.V.T.I.T.C.G.T.S.E.D.I.I.N.Y.L.N.W.Y.Q.Q.K.P.G.K.A.P.K.L.L.I.Y.H.T.S.R.L.Q.S.G.V.P.S.R.F.S.G.S.G.S.G.T.D.
            # F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.Y.C.Q.Q.G.Y.T.L.P.Y.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.
            # Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.
            # S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{E.V.Q.L.V.Q.S.G.A.E.
            # V.K.K.P.G.A.S.V.K.V.S.C.K.A.S.G.Y.T.F.T.S.Y.V.I.H.W.V.R.Q.R.P.G.Q.G.L.A.W.M.G.Y.I.N.P.Y.N.D.G.T.K.Y.N.E.R.
            # F.K.G.K.V.T.I.T.S.D.R.S.T.S.T.V.Y.M.E.L.S.S.L.R.S.E.D.T.A.V.Y.L.C.G.R.E.G.I.R.Y.Y.G.L.L.G.D.Y.W.G.Q.G.T.L.
            # V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.
            # G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.
            # S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.
            # E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.
            # N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.D.E.L.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.
            # A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.
            # A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{E.V.Q.L.V.Q.S.G.A.E.V.K.K.P.G.A.S.V.K.V.S.C.K.A.S.G.Y.T.F.T.S.
            # Y.V.I.H.W.V.R.Q.R.P.G.Q.G.L.A.W.M.G.Y.I.N.P.Y.N.D.G.T.K.Y.N.E.R.F.K.G.K.V.T.I.T.S.D.R.S.T.S.T.V.Y.M.E.L.S.
            # S.L.R.S.E.D.T.A.V.Y.L.C.G.R.E.G.I.R.Y.Y.G.L.L.G.D.Y.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.
            # S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.
            # T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.
            # V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.
            # E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.
            # P.Q.V.Y.T.L.P.P.S.R.D.E.L.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.
            # D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE
            # 4{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G.D.R.V.T.I.T.C.G.T.S.E.D.I.I.N.Y.L.N.W.Y.Q.Q.K.P.G.K.A.P.K.L.L.I.Y.H.T.S.
            # R.L.Q.S.G.V.P.S.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.Y.C.Q.Q.G.Y.T.L.P.Y.T.F.G.Q.G.T.K.V.E.
            # I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.
            # S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.
            # G.E.C}$PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE1,PEPTIDE1,134:R3-194:R3|PEPTIDE4,PEPTIDE4,23:R3-88:R3|PEPTIDE
            # 2,PEPTIDE3,233:R3-233:R3|PEPTIDE3,PEPTIDE4,224:R3-214:R3|PEPTIDE4,PEPTIDE4,134:R3-194:R3|PEPTIDE3,PEPTIDE3
            # ,22:R3-96:R3|PEPTIDE3,PEPTIDE3,371:R3-429:R3|PEPTIDE2,PEPTIDE2,265:R3-325:R3|PEPTIDE2,PEPTIDE3,230:R3-230:
            # R3|PEPTIDE1,PEPTIDE1,23:R3-88:R3|PEPTIDE2,PEPTIDE2,148:R3-204:R3|PEPTIDE2,PEPTIDE1,224:R3-214:R3|PEPTIDE2,
            # PEPTIDE2,371:R3-429:R3|PEPTIDE3,PEPTIDE3,265:R3-325:R3|PEPTIDE3,PEPTIDE3,148:R3-204:R3$$$' , 'PEPTIDE1{Q.S
            # .V.L.T.Q.P.P.S.V.S.G.A.P.G.Q.R.V.T.I.S.C.S.G.S.R.S.N.I.G.S.N.T.V.K.W.Y.Q.Q.L.P.G.T.A.P.K.L.L.I.Y.Y.N.D.Q.R
            # .P.S.G.V.P.D.R.F.S.G.S.K.S.G.T.S.A.S.L.A.I.T.G.L.Q.A.E.D.E.A.D.Y.Y.C.Q.S.Y.D.R.Y.T.H.P.A.L.L.F.G.T.G.T.K.V
            # .T.V.L.G.Q.P.K.A.A.P.S.V.T.L.F.P.P.S.S.E.E.L.Q.A.N.K.A.T.L.V.C.L.I.S.D.F.Y.P.G.A.V.T.V.A.W.K.A.D.S.S.P.V.K
            # .A.G.V.E.T.T.T.P.S.K.Q.S.N.N.K.Y.A.A.S.S.Y.L.S.L.T.P.E.Q.W.K.S.H.R.S.Y.S.C.Q.V.T.H.E.G.S.T.V.E.K.T.V.A.P.T
            # .E.C.S}|PEPTIDE2{Q.V.Q.L.V.E.S.G.G.G.V.V.Q.P.G.R.S.L.R.L.S.C.A.A.S.G.F.T.F.S.S.Y.G.M.H.W.V.R.Q.A.P.G.K.G.L
            # .E.W.V.A.F.I.R.Y.D.G.S.N.K.Y.Y.A.D.S.V.K.G.R.F.T.I.S.R.D.N.S.K.N.T.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.K.T
            # .H.G.S.H.D.N.W.G.Q.G.T.M.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P
            # .V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P
            # .S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V
            # .T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D
            # .W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L
            # .T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q
            # .Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{Q.V.Q.L.V.E.S.G.G.G.V.V.Q.P.G.R.S.L.R
            # .L.S.C.A.A.S.G.F.T.F.S.S.Y.G.M.H.W.V.R.Q.A.P.G.K.G.L.E.W.V.A.F.I.R.Y.D.G.S.N.K.Y.Y.A.D.S.V.K.G.R.F.T.I.S.R
            # .D.N.S.K.N.T.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.K.T.H.G.S.H.D.N.W.G.Q.G.T.M.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P
            # .L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y
            # .S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E
            # .L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N
            # .A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A
            # .K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K
            # .T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P
            # .G.K}|PEPTIDE4{Q.S.V.L.T.Q.P.P.S.V.S.G.A.P.G.Q.R.V.T.I.S.C.S.G.S.R.S.N.I.G.S.N.T.V.K.W.Y.Q.Q.L.P.G.T.A.P.K
            # .L.L.I.Y.Y.N.D.Q.R.P.S.G.V.P.D.R.F.S.G.S.K.S.G.T.S.A.S.L.A.I.T.G.L.Q.A.E.D.E.A.D.Y.Y.C.Q.S.Y.D.R.Y.T.H.P.A
            # .L.L.F.G.T.G.T.K.V.T.V.L.G.Q.P.K.A.A.P.S.V.T.L.F.P.P.S.S.E.E.L.Q.A.N.K.A.T.L.V.C.L.I.S.D.F.Y.P.G.A.V.T.V.A
            # .W.K.A.D.S.S.P.V.K.A.G.V.E.T.T.T.P.S.K.Q.S.N.N.K.Y.A.A.S.S.Y.L.S.L.T.P.E.Q.W.K.S.H.R.S.Y.S.C.Q.V.T.H.E.G.S
            # .T.V.E.K.T.V.A.P.T.E.C.S}$PEPTIDE1,PEPTIDE1,139:R3-198:R3|PEPTIDE2,PEPTIDE2,259:R3-319:R3|PEPTIDE3,PEPTIDE
            # 3,22:R3-96:R3|PEPTIDE3,PEPTIDE3,142:R3-198:R3|PEPTIDE4,PEPTIDE4,139:R3-198:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R
            # 3|PEPTIDE3,PEPTIDE3,259:R3-319:R3|PEPTIDE2,PEPTIDE3,227:R3-227:R3|PEPTIDE2,PEPTIDE3,224:R3-224:R3|PEPTIDE3
            # ,PEPTIDE4,218:R3-216:R3|PEPTIDE2,PEPTIDE2,142:R3-198:R3|PEPTIDE3,PEPTIDE3,365:R3-423:R3|PEPTIDE2,PEPTIDE1,
            # 218:R3-216:R3|PEPTIDE2,PEPTIDE2,365:R3-423:R3|PEPTIDE4,PEPTIDE4,22:R3-89:R3|PEPTIDE1,PEPTIDE1,22:R3-89:R3$
            # $$' , 'PEPTIDE1{E.I.V.L.T.Q.S.P.A.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.S.V.S.D.A.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.
            # R.L.L.I.Y.D.A.S.S.R.A.T.G.V.P.A.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.E.P.E.D.F.A.V.Y.Y.C.H.Q.Y.I.Q.L.H.S.F.
            # T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.
            # V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.
            # S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{Q.V.Q.L.V.Q.S.G.A.E.V.K.K.P.G.S.S.V.K.V.S.C.K.A.S.G.G.T.F.S.S.Y.G.I.S.W.
            # V.R.Q.A.P.G.Q.G.L.E.W.M.G.G.I.I.P.I.F.G.T.A.N.Y.A.Q.K.F.Q.G.R.V.T.I.T.A.D.E.S.T.S.T.A.Y.M.E.L.S.S.L.R.S.E.
            # D.T.A.V.Y.Y.C.A.R.Y.D.G.I.Y.G.E.L.D.F.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.
            # A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.
            # G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.
            # P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.
            # Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.
            # P.S.R.D.E.L.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.
            # L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{Q.V.Q.L.V.Q.
            # S.G.A.E.V.K.K.P.G.S.S.V.K.V.S.C.K.A.S.G.G.T.F.S.S.Y.G.I.S.W.V.R.Q.A.P.G.Q.G.L.E.W.M.G.G.I.I.P.I.F.G.T.A.N.
            # Y.A.Q.K.F.Q.G.R.V.T.I.T.A.D.E.S.T.S.T.A.Y.M.E.L.S.S.L.R.S.E.D.T.A.V.Y.Y.C.A.R.Y.D.G.I.Y.G.E.L.D.F.W.G.Q.G.
            # T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.
            # T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.
            # P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.
            # D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.
            # V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.D.E.L.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.
            # D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.
            # H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{E.I.V.L.T.Q.S.P.A.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.S.V.
            # S.D.A.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.R.L.L.I.Y.D.A.S.S.R.A.T.G.V.P.A.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.E.P.E.
            # D.F.A.V.Y.Y.C.H.Q.Y.I.Q.L.H.S.F.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.
            # C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.
            # E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE3,PEPTIDE3,1
            # 46:R3-202:R3|PEPTIDE2,PEPTIDE3,231:R3-231:R3|PEPTIDE2,PEPTIDE2,146:R3-202:R3|PEPTIDE3,PEPTIDE3,369:R3-427:
            # R3|PEPTIDE3,PEPTIDE4,222:R3-216:R3|PEPTIDE1,PEPTIDE1,136:R3-196:R3|PEPTIDE4,PEPTIDE4,23:R3-89:R3|PEPTIDE3,
            # PEPTIDE3,263:R3-323:R3|PEPTIDE1,PEPTIDE1,23:R3-89:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE2,PEPTIDE3,228:R
            # 3-228:R3|PEPTIDE2,PEPTIDE2,263:R3-323:R3|PEPTIDE4,PEPTIDE4,136:R3-196:R3|PEPTIDE2,PEPTIDE1,222:R3-216:R3|P
            # EPTIDE2,PEPTIDE2,369:R3-427:R3$$$' , 'PEPTIDE1{S.S.E.L.T.Q.D.P.A.V.S.V.A.L.G.Q.T.V.R.I.T.C.Q.G.D.S.L.R.S.Y
            # .Y.A.T.W.Y.Q.Q.K.P.G.Q.A.P.I.L.V.I.Y.G.E.N.K.R.P.S.G.I.P.D.R.F.S.G.S.S.S.G.N.T.A.S.L.T.I.T.G.A.Q.A.E.D.E.A
            # .D.Y.Y.C.K.S.R.D.G.S.G.Q.H.L.V.F.G.G.G.T.K.L.T.V.L.G.Q.P.K.A.A.P.S.V.T.L.F.P.P.S.S.E.E.L.Q.A.N.K.A.T.L.V.C
            # .L.I.S.D.F.Y.P.G.A.V.T.V.A.W.K.A.D.S.S.P.V.K.A.G.V.E.T.T.T.P.S.K.Q.S.N.N.K.Y.A.A.S.S.Y.L.S.L.T.P.E.Q.W.K.S
            # .H.R.S.Y.S.C.Q.V.T.H.E.G.S.T.V.E.K.T.V.A.P.A.E.C.S}|PEPTIDE2{E.V.Q.L.V.Q.S.G.A.E.V.K.K.P.G.S.S.V.K.V.S.C.K
            # .A.S.G.G.T.F.S.S.Y.A.I.S.W.V.R.Q.A.P.G.Q.G.L.E.W.M.G.G.I.I.P.I.F.G.T.A.N.Y.A.Q.K.F.Q.G.R.V.T.I.T.A.D.K.S.T
            # .S.T.A.Y.M.E.L.S.S.L.R.S.E.D.T.A.V.Y.Y.C.A.R.A.P.L.R.F.L.E.W.S.T.Q.D.H.Y.Y.Y.Y.Y.M.D.V.W.G.K.G.T.T.V.T.V.S
            # .S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T
            # .F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K
            # .T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F
            # .N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L
            # .P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W
            # .E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N
            # .H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{E.V.Q.L.V.Q.S.G.A.E.V.K.K.P.G.S.S.V.K.V.S.C.K.A.S.G.G.T.F.S.S.Y.A.I.S
            # .W.V.R.Q.A.P.G.Q.G.L.E.W.M.G.G.I.I.P.I.F.G.T.A.N.Y.A.Q.K.F.Q.G.R.V.T.I.T.A.D.K.S.T.S.T.A.Y.M.E.L.S.S.L.R.S
            # .E.D.T.A.V.Y.Y.C.A.R.A.P.L.R.F.L.E.W.S.T.Q.D.H.Y.Y.Y.Y.Y.M.D.V.W.G.K.G.T.T.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L
            # .A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S
            # .L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L
            # .L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A
            # .K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K
            # .G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T
            # .T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G
            # .K}|PEPTIDE4{S.S.E.L.T.Q.D.P.A.V.S.V.A.L.G.Q.T.V.R.I.T.C.Q.G.D.S.L.R.S.Y.Y.A.T.W.Y.Q.Q.K.P.G.Q.A.P.I.L.V.I
            # .Y.G.E.N.K.R.P.S.G.I.P.D.R.F.S.G.S.S.S.G.N.T.A.S.L.T.I.T.G.A.Q.A.E.D.E.A.D.Y.Y.C.K.S.R.D.G.S.G.Q.H.L.V.F.G
            # .G.G.T.K.L.T.V.L.G.Q.P.K.A.A.P.S.V.T.L.F.P.P.S.S.E.E.L.Q.A.N.K.A.T.L.V.C.L.I.S.D.F.Y.P.G.A.V.T.V.A.W.K.A.D
            # .S.S.P.V.K.A.G.V.E.T.T.T.P.S.K.Q.S.N.N.K.Y.A.A.S.S.Y.L.S.L.T.P.E.Q.W.K.S.H.R.S.Y.S.C.Q.V.T.H.E.G.S.T.V.E.K
            # .T.V.A.P.A.E.C.S}$PEPTIDE2,PEPTIDE1,233:R3-213:R3|PEPTIDE3,PEPTIDE3,157:R3-213:R3|PEPTIDE2,PEPTIDE2,380:R3
            # -438:R3|PEPTIDE2,PEPTIDE3,239:R3-239:R3|PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE4,PEPTIDE4,136:R3-195:R3|PEPT
            # IDE2,PEPTIDE3,242:R3-242:R3|PEPTIDE3,PEPTIDE3,380:R3-438:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE1,PEPTIDE
            # 1,136:R3-195:R3|PEPTIDE3,PEPTIDE4,233:R3-213:R3|PEPTIDE2,PEPTIDE2,157:R3-213:R3|PEPTIDE3,PEPTIDE3,274:R3-3
            # 34:R3|PEPTIDE1,PEPTIDE1,22:R3-87:R3|PEPTIDE2,PEPTIDE2,274:R3-334:R3|PEPTIDE4,PEPTIDE4,22:R3-87:R3$$$' , 'P
            # EPTIDE1{E.I.V.L.T.Q.S.P.G.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.G.I.S.R.S.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.S.L.L.I.
            # Y.G.A.S.S.R.A.T.G.I.P.D.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.R.L.E.P.E.D.F.A.V.Y.Y.C.Q.Q.F.G.S.S.P.W.T.F.G.Q.G.
            # T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.
            # Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.
            # S.F.N.R.G.E.C}|PEPTIDE2{Q.V.Q.L.Q.E.S.G.P.G.L.V.K.P.S.Q.T.L.S.L.T.C.T.V.S.G.G.S.I.S.S.G.D.Y.F.W.S.W.I.R.Q.
            # L.P.G.K.G.L.E.W.I.G.H.I.H.N.S.G.T.T.Y.Y.N.P.S.L.K.S.R.V.T.I.S.V.D.T.S.K.K.Q.F.S.L.R.L.S.S.V.T.A.A.D.T.A.V.
            # Y.Y.C.A.R.D.R.G.G.D.Y.Y.Y.G.M.D.V.W.G.Q.G.T.T.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.
            # G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.
            # Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.
            # D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.
            # V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.
            # R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.
            # S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{Q.V.Q.L.Q.E.S.G.
            # P.G.L.V.K.P.S.Q.T.L.S.L.T.C.T.V.S.G.G.S.I.S.S.G.D.Y.F.W.S.W.I.R.Q.L.P.G.K.G.L.E.W.I.G.H.I.H.N.S.G.T.T.Y.Y.
            # N.P.S.L.K.S.R.V.T.I.S.V.D.T.S.K.K.Q.F.S.L.R.L.S.S.V.T.A.A.D.T.A.V.Y.Y.C.A.R.D.R.G.G.D.Y.Y.Y.G.M.D.V.W.G.Q.
            # G.T.T.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.
            # L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.R.V.
            # E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.
            # E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.
            # K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.
            # S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.
            # M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{E.I.V.L.T.Q.S.P.G.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.G.
            # I.S.R.S.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.S.L.L.I.Y.G.A.S.S.R.A.T.G.I.P.D.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.R.L.E.P.
            # E.D.F.A.V.Y.Y.C.Q.Q.F.G.S.S.P.W.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.
            # C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.
            # E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE3,PEPTIDE3,266:R3-326:R3|PEPTIDE1,PEPTIDE1
            # ,23:R3-89:R3|PEPTIDE1,PEPTIDE1,135:R3-195:R3|PEPTIDE2,PEPTIDE3,231:R3-231:R3|PEPTIDE4,PEPTIDE4,23:R3-89:R3
            # |PEPTIDE2,PEPTIDE3,234:R3-234:R3|PEPTIDE2,PEPTIDE2,372:R3-430:R3|PEPTIDE2,PEPTIDE1,225:R3-215:R3|PEPTIDE4,
            # PEPTIDE4,135:R3-195:R3|PEPTIDE3,PEPTIDE4,225:R3-215:R3|PEPTIDE3,PEPTIDE3,22:R3-97:R3|PEPTIDE2,PEPTIDE2,266
            # :R3-326:R3|PEPTIDE2,PEPTIDE2,149:R3-205:R3|PEPTIDE3,PEPTIDE3,149:R3-205:R3|PEPTIDE3,PEPTIDE3,372:R3-430:R3
            # |PEPTIDE2,PEPTIDE2,22:R3-97:R3$$$' , 'PEPTIDE1{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G.D.R.V.T.I.T.C.R.S.S.Q.S.L.V
            # .H.S.N.G.N.T.F.L.H.W.Y.Q.Q.K.P.G.K.A.P.K.L.L.I.Y.T.V.S.N.R.F.S.G.V.P.S.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L
            # .Q.P.E.D.F.A.T.Y.F.C.S.Q.T.T.H.V.P.W.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S
            # .V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A
            # .D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{E.V.Q.L.V.E.S.G.G.G.L.V.Q.P.G.G.S.L
            # .R.L.S.C.A.A.S.G.Y.S.F.T.G.Y.Y.I.H.W.V.R.Q.A.P.G.K.G.L.E.W.V.A.R.V.I.P.N.A.G.G.T.S.Y.N.Q.K.F.K.G.R.F.T.L.S
            # .V.D.N.S.K.N.T.A.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.A.R.E.G.I.Y.W.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P
            # .L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y
            # .S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E
            # .L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N
            # .A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A
            # .K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K
            # .T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P
            # .G.K}|PEPTIDE3{E.V.Q.L.V.E.S.G.G.G.L.V.Q.P.G.G.S.L.R.L.S.C.A.A.S.G.Y.S.F.T.G.Y.Y.I.H.W.V.R.Q.A.P.G.K.G.L.E
            # .W.V.A.R.V.I.P.N.A.G.G.T.S.Y.N.Q.K.F.K.G.R.F.T.L.S.V.D.N.S.K.N.T.A.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.Y.C.A.R.E
            # .G.I.Y.W.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T
            # .V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N
            # .T.K.V.D.K.K.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C
            # .V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L
            # .N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C
            # .L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G
            # .N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{D.I.Q.M.T.Q.S.P.S.S.L.S.A.S.V.G.D.R.V.T.I
            # .T.C.R.S.S.Q.S.L.V.H.S.N.G.N.T.F.L.H.W.Y.Q.Q.K.P.G.K.A.P.K.L.L.I.Y.T.V.S.N.R.F.S.G.V.P.S.R.F.S.G.S.G.S.G.T
            # .D.F.T.L.T.I.S.S.L.Q.P.E.D.F.A.T.Y.F.C.S.Q.T.T.H.V.P.W.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D
            # .E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L
            # .S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE2,PEPTIDE1,217:R3-2
            # 19:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE1,PEPTIDE1,139:R3-199:R3|PEPTIDE1,PEPTIDE1,23:R3-93:R3|PEPTIDE2
            # ,PEPTIDE2,141:R3-197:R3|PEPTIDE4,PEPTIDE4,139:R3-199:R3|PEPTIDE2,PEPTIDE2,364:R3-422:R3|PEPTIDE3,PEPTIDE3,
            # 141:R3-197:R3|PEPTIDE2,PEPTIDE3,223:R3-223:R3|PEPTIDE3,PEPTIDE3,258:R3-318:R3|PEPTIDE4,PEPTIDE4,23:R3-93:R
            # 3|PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE2,PEPTIDE2,258:R3-318:R3|PEPTIDE3,PEPTIDE4,217:R3-219:R3|PEPTIDE2,P
            # EPTIDE3,226:R3-226:R3|PEPTIDE3,PEPTIDE3,364:R3-422:R3$$$' , 'PEPTIDE1{D.I.V.M.T.Q.S.P.L.S.L.P.V.T.P.G.Q.P.
            # A.S.I.S.C.R.S.S.Q.S.I.V.H.S.N.G.N.T.W.L.Q.W.W.L.Q.K.P.G.Q.S.P.Q.L.L.I.W.K.V.S.N.R.L.W.G.V.P.D.R.F.S.G.S.G.
            # S.G.T.D.F.T.L.K.I.S.R.V.Q.A.Q.D.V.G.V.W.W.C.F.Q.G.S.H.V.P.W.T.F.G.Q.G.T.K.V.Q.I.K.R.T.V.A.A.P.S.V.F.I.F.P.
            # P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.
            # Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}|PEPTIDE2{Q.V.Q.L.Q.Q.
            # S.G.P.G.L.V.K.P.S.Q.T.L.S.L.T.C.T.V.S.G.W.S.I.S.G.G.W.L.W.N.W.I.R.Q.P.P.G.K.G.L.Q.W.I.G.W.I.S.W.D.G.T.N.N.
            # W.K.P.S.L.K.D.R.V.T.I.S.V.D.T.S.K.N.Q.F.S.L.K.L.S.S.V.T.A.A.D.T.A.V.W.W.C.A.R.W.G.R.V.F.F.D.W.W.G.Q.G.T.L.
            # V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.
            # G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.
            # S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.
            # E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.
            # N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.
            # A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.
            # A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{Q.V.Q.L.Q.Q.S.G.P.G.L.V.K.P.S.Q.T.L.S.L.T.C.T.V.S.G.W.S.I.S.G.
            # G.W.L.W.N.W.I.R.Q.P.P.G.K.G.L.Q.W.I.G.W.I.S.W.D.G.T.N.N.W.K.P.S.L.K.D.R.V.T.I.S.V.D.T.S.K.N.Q.F.S.L.K.L.S.
            # S.V.T.A.A.D.T.A.V.W.W.C.A.R.W.G.R.V.F.F.D.W.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.
            # G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.
            # S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.
            # P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.
            # N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.
            # T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.
            # S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{D.I.V.
            # M.T.Q.S.P.L.S.L.P.V.T.P.G.Q.P.A.S.I.S.C.R.S.S.Q.S.I.V.H.S.N.G.N.T.W.L.Q.W.W.L.Q.K.P.G.Q.S.P.Q.L.L.I.W.K.V.
            # S.N.R.L.W.G.V.P.D.R.F.S.G.S.G.S.G.T.D.F.T.L.K.I.S.R.V.Q.A.Q.D.V.G.V.W.W.C.F.Q.G.S.H.V.P.W.T.F.G.Q.G.T.K.V.
            # Q.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.
            # N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.
            # R.G.E.C}$PEPTIDE4,PEPTIDE4,139:R3-199:R3|PEPTIDE2,PEPTIDE1,220:R3-219:R3|PEPTIDE3,PEPTIDE3,367:R3-425:R3|P
            # EPTIDE2,PEPTIDE3,226:R3-226:R3|PEPTIDE1,PEPTIDE1,23:R3-93:R3|PEPTIDE3,PEPTIDE4,220:R3-219:R3|PEPTIDE1,PEPT
            # IDE1,139:R3-199:R3|PEPTIDE4,PEPTIDE4,23:R3-93:R3|PEPTIDE2,PEPTIDE2,367:R3-425:R3|PEPTIDE2,PEPTIDE2,144:R3-
            # 200:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE2,PEPTIDE2,261:R3-321:R3|PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE
            # 2,PEPTIDE3,229:R3-229:R3|PEPTIDE3,PEPTIDE3,261:R3-321:R3|PEPTIDE3,PEPTIDE3,144:R3-200:R3$$$' , 'PEPTIDE1{E
            # .I.V.L.T.Q.S.P.A.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.S.V.S.S.Y.L.A.W.Y.Q.Q.K.P.G.Q.A.P.R.L.L.I.Y.D.A.S.N.R
            # .A.T.G.I.P.A.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.E.P.E.D.F.A.V.Y.Y.C.Q.Q.R.S.N.W.P.P.T.F.G.Q.G.T.K.V.E.I.K
            # .R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y.P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q
            # .E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A.C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E
            # .C}|PEPTIDE2{E.V.Q.L.L.E.S.G.G.G.L.V.Q.P.G.G.S.L.R.L.S.C.A.V.S.G.F.T.F.N.S.F.A.M.S.W.V.R.Q.A.P.G.K.G.L.E.W
            # .V.S.A.I.S.G.S.G.G.G.T.Y.Y.A.D.S.V.K.G.R.F.T.I.S.R.D.N.S.K.N.T.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.F.C.A.K.D.K
            # .I.L.W.F.G.E.P.V.F.D.Y.W.G.Q.G.T.L.V.T.V.S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D
            # .Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H.T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N
            # .V.N.H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.C.D.K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S
            # .R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K.F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T
            # .V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A.L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K
            # .N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E.W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D
            # .K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H.N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE3{E.V.Q.L.L.E.S.G.G.G.L.V.Q.P
            # .G.G.S.L.R.L.S.C.A.V.S.G.F.T.F.N.S.F.A.M.S.W.V.R.Q.A.P.G.K.G.L.E.W.V.S.A.I.S.G.S.G.G.G.T.Y.Y.A.D.S.V.K.G.R
            # .F.T.I.S.R.D.N.S.K.N.T.L.Y.L.Q.M.N.S.L.R.A.E.D.T.A.V.Y.F.C.A.K.D.K.I.L.W.F.G.E.P.V.F.D.Y.W.G.Q.G.T.L.V.T.V
            # .S.S.A.S.T.K.G.P.S.V.F.P.L.A.P.S.S.K.S.T.S.G.G.T.A.A.L.G.C.L.V.K.D.Y.F.P.E.P.V.T.V.S.W.N.S.G.A.L.T.S.G.V.H
            # .T.F.P.A.V.L.Q.S.S.G.L.Y.S.L.S.S.V.V.T.V.P.S.S.S.L.G.T.Q.T.Y.I.C.N.V.N.H.K.P.S.N.T.K.V.D.K.R.V.E.P.K.S.C.D
            # .K.T.H.T.C.P.P.C.P.A.P.E.L.L.G.G.P.S.V.F.L.F.P.P.K.P.K.D.T.L.M.I.S.R.T.P.E.V.T.C.V.V.V.D.V.S.H.E.D.P.E.V.K
            # .F.N.W.Y.V.D.G.V.E.V.H.N.A.K.T.K.P.R.E.E.Q.Y.N.S.T.Y.R.V.V.S.V.L.T.V.L.H.Q.D.W.L.N.G.K.E.Y.K.C.K.V.S.N.K.A
            # .L.P.A.P.I.E.K.T.I.S.K.A.K.G.Q.P.R.E.P.Q.V.Y.T.L.P.P.S.R.E.E.M.T.K.N.Q.V.S.L.T.C.L.V.K.G.F.Y.P.S.D.I.A.V.E
            # .W.E.S.N.G.Q.P.E.N.N.Y.K.T.T.P.P.V.L.D.S.D.G.S.F.F.L.Y.S.K.L.T.V.D.K.S.R.W.Q.Q.G.N.V.F.S.C.S.V.M.H.E.A.L.H
            # .N.H.Y.T.Q.K.S.L.S.L.S.P.G.K}|PEPTIDE4{E.I.V.L.T.Q.S.P.A.T.L.S.L.S.P.G.E.R.A.T.L.S.C.R.A.S.Q.S.V.S.S.Y.L.A
            # .W.Y.Q.Q.K.P.G.Q.A.P.R.L.L.I.Y.D.A.S.N.R.A.T.G.I.P.A.R.F.S.G.S.G.S.G.T.D.F.T.L.T.I.S.S.L.E.P.E.D.F.A.V.Y.Y
            # .C.Q.Q.R.S.N.W.P.P.T.F.G.Q.G.T.K.V.E.I.K.R.T.V.A.A.P.S.V.F.I.F.P.P.S.D.E.Q.L.K.S.G.T.A.S.V.V.C.L.L.N.N.F.Y
            # .P.R.E.A.K.V.Q.W.K.V.D.N.A.L.Q.S.G.N.S.Q.E.S.V.T.E.Q.D.S.K.D.S.T.Y.S.L.S.S.T.L.T.L.S.K.A.D.Y.E.K.H.K.V.Y.A
            # .C.E.V.T.H.Q.G.L.S.S.P.V.T.K.S.F.N.R.G.E.C}$PEPTIDE4,PEPTIDE4,134:R3-194:R3|PEPTIDE3,PEPTIDE3,266:R3-326:R
            # 3|PEPTIDE3,PEPTIDE4,225:R3-214:R3|PEPTIDE2,PEPTIDE2,372:R3-430:R3|PEPTIDE1,PEPTIDE1,23:R3-88:R3|PEPTIDE2,P
            # EPTIDE3,231:R3-231:R3|PEPTIDE2,PEPTIDE2,149:R3-205:R3|PEPTIDE3,PEPTIDE3,22:R3-96:R3|PEPTIDE1,PEPTIDE1,134:
            # R3-194:R3|PEPTIDE2,PEPTIDE3,234:R3-234:R3|PEPTIDE3,PEPTIDE3,372:R3-430:R3|PEPTIDE4,PEPTIDE4,23:R3-88:R3|PE
            # PTIDE2,PEPTIDE1,225:R3-214:R3|PEPTIDE3,PEPTIDE3,149:R3-205:R3|PEPTIDE2,PEPTIDE2,22:R3-96:R3|PEPTIDE2,PEPTI
            # DE2,266:R3-326:R3$$$'
            'indication_class': 'TEXT',
            # EXAMPLES:
            # 'Pharmaceutic Aid (aerosol propellant)' , 'Antihistaminic' , 'Vasodilator (coronary)' , 'Anti-Inflammatory
            # ' , 'Serotonin Inhibitor' , 'Serotonin Antagonist' , 'Analgesic' , 'Antidepressant' , 'Antineoplastic' , '
            # Anti-Asthmatic (leukotriene antagonist)'
            'max_phase': 'NUMERIC',
            # EXAMPLES:
            # '2.0' , '4.0' , '-1.0' , '-1.0' , '-1.0' , '4.0' , '4.0' , '1.0' , '2.0' , '1.0'
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL1742985' , 'CHEMBL350221' , 'CHEMBL348290' , 'CHEMBL345248' , 'CHEMBL155962' , 'CHEMBL1311' , 'CHEM
            # BL157138' , 'CHEMBL422872' , 'CHEMBL160629' , 'CHEMBL346919'
            'molecule_properties': 
            {
                'properties': 
                {
                    'alogp': 'NUMERIC',
                    # EXAMPLES:
                    # '0.83' , '2.28' , '2.45' , '0.59' , '-1.28' , '2.84' , '6.70' , '2.22' , '1.60' , '-0.49'
                    'aromatic_rings': 'NUMERIC',
                    # EXAMPLES:
                    # '2' , '0' , '0' , '3' , '0' , '2' , '4' , '0' , '1' , '0'
                    'cx_logd': 'NUMERIC',
                    # EXAMPLES:
                    # '-1.40' , '2.71' , '2.28' , '0.40' , '-0.71' , '2.06' , '3.57' , '-0.40' , '1.02' , '-3.77'
                    'cx_logp': 'NUMERIC',
                    # EXAMPLES:
                    # '-1.39' , '2.71' , '2.28' , '1.08' , '-0.71' , '2.17' , '6.36' , '2.78' , '1.07' , '-0.53'
                    'cx_most_apka': 'NUMERIC',
                    # EXAMPLES:
                    # '2.15' , '13.34' , '4.14' , '3.95' , '8.27' , '3.86' , '8.44' , '5.85' , '4.42' , '5.08'
                    'cx_most_bpka': 'NUMERIC',
                    # EXAMPLES:
                    # '9.18' , '7.97' , '6.88' , '3.12' , '6.09' , '2.19' , '8.99' , '8.46' , '8.41' , '8.79'
                    'full_molformula': 'TEXT',
                    # EXAMPLES:
                    # 'C11H12N2O3' , 'CCl3F' , 'CHBr3' , 'C23H32N6O3S' , 'C6H9NO6' , 'C20H26N4O' , 'C28H27NO3S' , 'C22H3
                    # 0O5' , 'C9H10O3' , 'C5H9NO3S'
                    'full_mwt': 'NUMERIC',
                    # EXAMPLES:
                    # '220.23' , '137.37' , '252.73' , '472.62' , '191.14' , '338.46' , '457.60' , '374.48' , '166.18' ,
                    #  '163.20'
                    'hba': 'NUMERIC',
                    # EXAMPLES:
                    # '3' , '0' , '0' , '10' , '6' , '2' , '4' , '4' , '3' , '3'
                    'hba_lipinski': 'NUMERIC',
                    # EXAMPLES:
                    # '5' , '0' , '0' , '9' , '7' , '5' , '4' , '5' , '3' , '4'
                    'hbd': 'NUMERIC',
                    # EXAMPLES:
                    # '4' , '0' , '0' , '1' , '1' , '2' , '1' , '3' , '1' , '3'
                    'hbd_lipinski': 'NUMERIC',
                    # EXAMPLES:
                    # '5' , '0' , '0' , '1' , '1' , '2' , '1' , '3' , '1' , '2'
                    'heavy_atoms': 'NUMERIC',
                    # EXAMPLES:
                    # '16' , '5' , '4' , '33' , '13' , '25' , '33' , '27' , '12' , '10'
                    'molecular_species': 'TEXT',
                    # EXAMPLES:
                    # 'ZWITTERION' , 'NEUTRAL' , 'NEUTRAL' , 'NEUTRAL' , 'ACID' , 'ACID' , 'NEUTRAL' , 'ACID' , 'NEUTRAL
                    # ' , 'ACID'
                    'mw_freebase': 'NUMERIC',
                    # EXAMPLES:
                    # '220.23' , '137.37' , '252.73' , '472.62' , '191.14' , '338.46' , '457.60' , '374.48' , '166.18' ,
                    #  '163.20'
                    'mw_monoisotopic': 'NUMERIC',
                    # EXAMPLES:
                    # '220.0848' , '135.9050' , '249.7628' , '472.2257' , '191.0430' , '338.2107' , '457.1712' , '374.20
                    # 93' , '166.0630' , '163.0303'
                    'np_likeness_score': 'NUMERIC',
                    # EXAMPLES:
                    # '0.79' , '-0.85' , '0.08' , '-1.44' , '0.58' , '0.11' , '-0.44' , '1.23' , '0.37' , '-0.01'
                    'num_lipinski_ro5_violations': 'NUMERIC',
                    # EXAMPLES:
                    # '0' , '0' , '0' , '0' , '0' , '0' , '1' , '0' , '0' , '0'
                    'num_ro5_violations': 'NUMERIC',
                    # EXAMPLES:
                    # '0' , '0' , '0' , '0' , '0' , '0' , '1' , '0' , '0' , '0'
                    'psa': 'NUMERIC',
                    # EXAMPLES:
                    # '99.34' , '0.00' , '0.00' , '88.53' , '91.06' , '51.37' , '59.42' , '86.99' , '46.53' , '66.40'
                    'qed_weighted': 'NUMERIC',
                    # EXAMPLES:
                    # '0.62' , '0.45' , '0.58' , '0.36' , '0.43' , '0.90' , '0.27' , '0.38' , '0.68' , '0.49'
                    'ro3_pass': 'TEXT',
                    # EXAMPLES:
                    # 'N' , 'Y' , 'Y' , 'N' , 'N' , 'N' , 'N' , 'N' , 'Y' , 'N'
                    'rtb': 'NUMERIC',
                    # EXAMPLES:
                    # '3' , '0' , '0' , '9' , '2' , '3' , '11' , '6' , '2' , '3'
                }
            },
            'molecule_structures': 
            {
                'properties': 
                {
                    'canonical_smiles': 'TEXT',
                    # EXAMPLES:
                    # 'N[C@@H](Cc1c[nH]c2ccc(O)cc12)C(=O)O' , 'FC(Cl)(Cl)Cl' , 'BrC(Br)Br' , 'Cn1c(=O)c2c(ncn2CC(O)CN2CC
                    # N(CCCSc3ccccc3)CC2)n(C)c1=O' , 'O=[N+]([O-])O[C@@H]1CO[C@H]2[C@@H]1OC[C@@H]2O' , 'CCN(CC)C(=O)N[C@
                    # H]1C=C2c3cccc4[nH]cc(c34)C[C@H]2N(C)C1' , 'O=C(O)CSC(CCCc1ccccc1)c1ccc(OCc2ccc3ccccc3n2)cc1' , 'CC
                    # C#CC[C@H](C)[C@H](O)C#C[C@@H]1[C@H]2C/C(=C/COCC(=O)O)C[C@H]2C[C@H]1O' , 'COc1cc(C(C)=O)ccc1O' , 'C
                    # C(S)C(=O)NCC(=O)O'
                    'standard_inchi': 'TEXT',
                    # EXAMPLES:
                    # 'InChI=1S/C11H12N2O3/c12-9(11(15)16)3-6-5-13-10-2-1-7(14)4-8(6)10/h1-2,4-5,9,13-14H,3,12H2,(H,15,1
                    # 6)/t9-/m0/s1' , 'InChI=1S/CCl3F/c2-1(3,4)5' , 'InChI=1S/CHBr3/c2-1(3)4/h1H' , 'InChI=1S/C23H32N6O3
                    # S/c1-25-21-20(22(31)26(2)23(25)32)29(17-24-21)16-18(30)15-28-12-10-27(11-13-28)9-6-14-33-19-7-4-3-
                    # 5-8-19/h3-5,7-8,17-18,30H,6,9-16H2,1-2H3' , 'InChI=1S/C6H9NO6/c8-3-1-11-6-4(13-7(9)10)2-12-5(3)6/h
                    # 3-6,8H,1-2H2/t3-,4+,5+,6+/m0/s1' , 'InChI=1S/C20H26N4O/c1-4-24(5-2)20(25)22-14-10-16-15-7-6-8-17-1
                    # 9(15)13(11-21-17)9-18(16)23(3)12-14/h6-8,10-11,14,18,21H,4-5,9,12H2,1-3H3,(H,22,25)/t14-,18+/m0/s1
                    # ' , 'InChI=1S/C28H27NO3S/c30-28(31)20-33-27(12-6-9-21-7-2-1-3-8-21)23-14-17-25(18-15-23)32-19-24-1
                    # 6-13-22-10-4-5-11-26(22)29-24/h1-5,7-8,10-11,13-18,27H,6,9,12,19-20H2,(H,30,31)' , 'InChI=1S/C22H3
                    # 0O5/c1-3-4-5-6-15(2)20(23)8-7-18-19-12-16(9-10-27-14-22(25)26)11-17(19)13-21(18)24/h9,15,17-21,23-
                    # 24H,3,6,10-14H2,1-2H3,(H,25,26)/b16-9+/t15-,17-,18+,19-,20+,21+/m0/s1' , 'InChI=1S/C9H10O3/c1-6(10
                    # )7-3-4-8(11)9(5-7)12-2/h3-5,11H,1-2H3' , 'InChI=1S/C5H9NO3S/c1-3(10)5(9)6-2-4(7)8/h3,10H,2H2,1H3,(
                    # H,6,9)(H,7,8)'
                    'standard_inchi_key': 'TEXT',
                    # EXAMPLES:
                    # 'LDCYZAJDBXYCGN-VIFPVBQESA-N' , 'CYRMSUTZVYGINF-UHFFFAOYSA-N' , 'DIKBFYAXUHHXCS-UHFFFAOYSA-N' , 'J
                    # TOUASWUIMAMAD-UHFFFAOYSA-N' , 'YWXYYJSYQOXTPL-SLPGGIOYSA-N' , 'BKRGVLQUQGGVSM-KBXCAEBGSA-N' , 'JOI
                    # XGLLMSDPZDN-UHFFFAOYSA-N' , 'ARUGKOZUKWAXDS-SEWALLKFSA-N' , 'DFYRUELUNQRZTB-UHFFFAOYSA-N' , 'YTGJW
                    # QPHMWSCST-UHFFFAOYSA-N'
                }
            },
            'molecule_synonyms': 
            {
                'properties': 
                {
                    'molecule_synonym': 'TEXT',
                    # EXAMPLES:
                    # 'Anrukinzumab' , '5-hydroxy-l-tryptophan' , 'Trichloromonofluoromethane' , 'Bromoform' , 'Tazifili
                    # na' , '1,1-diureidisobutane' , 'Lisurida' , 'AZ-12096971' , 'Cicaprost' , '4-hydroxy-3-methoxyacet
                    # ophenone'
                    'syn_type': 'TEXT',
                    # EXAMPLES:
                    # 'INN' , 'USP' , 'NATIONAL_FORMULARY' , 'OTHER' , 'INN_SPANISH' , 'OTHER' , 'INN_SPANISH' , 'RESEAR
                    # CH_CODE' , 'INN' , 'OTHER'
                    'synonyms': 'TEXT',
                    # EXAMPLES:
                    # 'ANRUKINZUMAB' , '5-HYDROXY-L-TRYPTOPHAN' , 'TRICHLOROMONOFLUOROMETHANE' , 'Bromoform' , 'TAZIFILI
                    # NA' , '1,1-DIUREIDISOBUTANE' , 'LISURIDA' , 'AZ-12096971' , 'CICAPROST' , '4-HYDROXY-3-METHOXYACET
                    # OPHENONE'
                }
            },
            'ob_patent': 'NUMERIC',
            # EXAMPLES:
            # '11458104' , '7390791' , '7399865' , '9271990' , '9089489' , '7687075' , '7587988' , '7297346' , '9949937'
            #  , '6936612'
            'oral': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '1' , '0' , '0' , '0' , '0'
            'parenteral': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'prodrug': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '1' , '0' , '0' , '0' , '0'
            'research_codes': 'TEXT',
            # EXAMPLES:
            # 'IMA-638' , 'NSC-92523' , 'RS-49014' , 'BM 22.145IS 5-MNAHR-4698' , 'L-674573' , 'NSC-209524' , 'NSC-76041
            # 6' , 'HN-65021' , 'NSC-297935' , '8599 R.P.'
            'rule_of_five': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '0' , '1' , '1'
            'sc_patent': 'TEXT',
            # EXAMPLES:
            # 'US-11458104-B1' , 'US-7390791-B2' , 'US-7399865-B2' , 'US-9271990-B2' , 'US-9089489-B2' , 'US-7687075-B2'
            #  , 'US-7587988-B2' , 'US-7297346-B2' , 'US-9949937-B2' , 'US-6936612-B2'
            'synonyms': 'TEXT',
            # EXAMPLES:
            # 'Anrukinzumab (INN, USAN)' , '5-hydroxy-l-tryptophan (USP)' , 'Trichloromonofluoromethane (NF)' , 'Bromofo
            # rm (MI, USP)' , 'Tazifylline hydrochloride (USAN)' , 'Diluted isosorbide mononitrate (USP)' , 'Lisuride (B
            # AN, INN, MI)' , 'Azd-4769' , 'Cicaprost (INN)' , 'Apocynin (MI)'
            'topical': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'usan_stem': 'TEXT',
            # EXAMPLES:
            # '-mab' , '-triptan' , '-fylline' , '-prost' , '-sartan' , '-profen' , '-erg-' , '-fentanil' , '-oxan' , '-
            # ast'
            'usan_stem_definition': 'TEXT',
            # EXAMPLES:
            # 'monoclonal antibodies' , 'antimigraine agents (5-HT1 receptor agonists); sumatriptan derivatives' , 'theo
            # phylline derivatives' , 'prostaglandins' , 'angiotensin II receptor antagonists' , 'anti-inflammatory/anal
            # gesic agents (ibuprofen type)' , 'ergot alkaloid derivatives' , 'narcotic analgesics (fentanyl derivatives
            # )' , 'alpha-adrenoceptor antagonists (benzodioxane derivatives)' , 'antiasthmatics/antiallergics (not acti
            # ng primarily as antihistamines): leukotriene receptor antagonists'
            'usan_stem_substem': 'TEXT',
            # EXAMPLES:
            # '-mab(-mab)' , '-triptan(-triptan)' , '-fylline(-fylline)' , '-prost(-prost)' , '-sartan(-sartan)' , '-pro
            # fen(-profen)' , '-erg-(-erg-)' , '-fentanil(-fentanil)' , '-oxan(-oxan)' , '-ast(-ast (-lukast))'
            'usan_year': 'NUMERIC',
            # EXAMPLES:
            # '2007' , '1985' , '1988' , '1976' , '1967' , '1992' , '1990' , '1984' , '1967' , '2021'
            'withdrawn_flag': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
        }
    }
