# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'action_type': 
            {
                'properties': 
                {
                }
            },
            'activity_comment': 'TEXT',
            # EXAMPLES:
            # 'See Activity_Supp For Individual Animal Data' , 'See Activity_Supp For Individual Animal Data' , 'See Act
            # ivity_Supp For Individual Animal Data' , 'See Activity_Supp For Individual Animal Data' , 'See Activity_Su
            # pp For Individual Animal Data' , 'See Activity_Supp For Individual Animal Data' , 'See Activity_Supp For I
            # ndividual Animal Data' , 'See Activity_Supp For Individual Animal Data' , 'See Activity_Supp For Individua
            # l Animal Data' , 'See Activity_Supp For Individual Animal Data'
            'activity_id': 'NUMERIC',
            # EXAMPLES:
            # '17127737' , '17127738' , '17127739' , '17127740' , '17127741' , '17127746' , '17127747' , '17127749' , '1
            # 7127752' , '17127753'
            'activity_properties': 
            {
                'properties': 
                {
                    'relation': 'TEXT',
                    # EXAMPLES:
                    # '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '='
                    'result_flag': 'NUMERIC',
                    # EXAMPLES:
                    # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
                    'standard_relation': 'TEXT',
                    # EXAMPLES:
                    # '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '='
                    'standard_text_value': 'TEXT',
                    # EXAMPLES:
                    # 'MCH (Ery. Mean Corpuscular Hemoglobin)' , 'MCHC (Ery. Mean Corpuscular HGB Concentration)' , 'RET
                    # IRBC (Reticulocytes/Erythrocytes)' , 'PLAT (Platelets)' , 'WBC (Leukocytes)' , 'LYMLE (Lymphocytes
                    # /Leukocytes)' , 'PT (Prothrombin Time)' , 'FIBRINO (Fibrinogen)' , 'HCT (Hematocrit)' , 'MCV (Ery.
                    #  Mean Corpuscular Volume)'
                    'standard_type': 'TEXT',
                    # EXAMPLES:
                    # 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVIT
                    # Y_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST'
                    'standard_units': 'TEXT',
                    # EXAMPLES:
                    # 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'mg.kg-1' , 'm
                    # g.kg-1' , 'mg.kg-1'
                    'standard_value': 'NUMERIC',
                    # EXAMPLES:
                    # '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0'
                    'text_value': 'TEXT',
                    # EXAMPLES:
                    # 'MCH (Ery. Mean Corpuscular Hemoglobin)' , 'MCHC (Ery. Mean Corpuscular HGB Concentration)' , 'RET
                    # IRBC (Reticulocytes/Erythrocytes)' , 'PLAT (Platelets)' , 'WBC (Leukocytes)' , 'LYMLE (Lymphocytes
                    # /Leukocytes)' , 'PT (Prothrombin Time)' , 'FIBRINO (Fibrinogen)' , 'HCT (Hematocrit)' , 'MCV (Ery.
                    #  Mean Corpuscular Volume)'
                    'type': 'TEXT',
                    # EXAMPLES:
                    # 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVIT
                    # Y_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST' , 'ACTIVITY_TEST'
                    'units': 'TEXT',
                    # EXAMPLES:
                    # 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg' , 'mg/kg'
                    'value': 'NUMERIC',
                    # EXAMPLES:
                    # '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0' , '150.0'
                }
            },
            'assay_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL3885863' , 'CHEMBL3885863' , 'CHEMBL3885863' , 'CHEMBL3885863' , 'CHEMBL3885863' , 'CHEMBL3885863' 
            # , 'CHEMBL3885863' , 'CHEMBL3885863' , 'CHEMBL3885863' , 'CHEMBL3885863'
            'assay_description': 'TEXT',
            # EXAMPLES:
            # 'Open TG-GATES - Regimen: Single' , 'Open TG-GATES - Regimen: Single' , 'Open TG-GATES - Regimen: Single' 
            # , 'Open TG-GATES - Regimen: Single' , 'Open TG-GATES - Regimen: Single' , 'Open TG-GATES - Regimen: Single
            # ' , 'Open TG-GATES - Regimen: Single' , 'Open TG-GATES - Regimen: Single' , 'Open TG-GATES - Regimen: Sing
            # le' , 'Open TG-GATES - Regimen: Single'
            'assay_type': 'TEXT',
            # EXAMPLES:
            # 'T' , 'T' , 'T' , 'T' , 'T' , 'T' , 'T' , 'T' , 'T' , 'T'
            'bao_endpoint': 'TEXT',
            # EXAMPLES:
            # 'BAO_0000179' , 'BAO_0000179' , 'BAO_0000179' , 'BAO_0000179' , 'BAO_0000179' , 'BAO_0000179' , 'BAO_00001
            # 79' , 'BAO_0000179' , 'BAO_0000179' , 'BAO_0000179'
            'bao_format': 'TEXT',
            # EXAMPLES:
            # 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_00002
            # 18' , 'BAO_0000218' , 'BAO_0000218' , 'BAO_0000218'
            'bao_label': 'TEXT',
            # EXAMPLES:
            # 'organism-based format' , 'organism-based format' , 'organism-based format' , 'organism-based format' , 'o
            # rganism-based format' , 'organism-based format' , 'organism-based format' , 'organism-based format' , 'org
            # anism-based format' , 'organism-based format'
            'canonical_smiles': 'TEXT',
            # EXAMPLES:
            # 'CCCC(CCC)C(=O)O' , 'CCCC(CCC)C(=O)O' , 'CCCC(CCC)C(=O)O' , 'CCCC(CCC)C(=O)O' , 'CCCC(CCC)C(=O)O' , 'CCCC(
            # CCC)C(=O)O' , 'CCCC(CCC)C(=O)O' , 'CCCC(CCC)C(=O)O' , 'CCCC(CCC)C(=O)O' , 'CCCC(CCC)C(=O)O'
            'data_validity_comment': 'TEXT',
            # EXAMPLES:
            # 'Outside typical range' , 'Outside typical range' , 'Outside typical range' , 'Outside typical range' , 'O
            # utside typical range' , 'Outside typical range'
            'data_validity_description': 'TEXT',
            # EXAMPLES:
            # 'Values for this activity type are unusually large/small, so may not be accurate' , 'Values for this activ
            # ity type are unusually large/small, so may not be accurate' , 'Values for this activity type are unusually
            #  large/small, so may not be accurate' , 'Values for this activity type are unusually large/small, so may n
            # ot be accurate' , 'Values for this activity type are unusually large/small, so may not be accurate' , 'Val
            # ues for this activity type are unusually large/small, so may not be accurate'
            'document_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL3885861' , 'CHEMBL3885861' , 'CHEMBL3885861' , 'CHEMBL3885861' , 'CHEMBL3885861' , 'CHEMBL3885861' 
            # , 'CHEMBL3885861' , 'CHEMBL3885861' , 'CHEMBL3885861' , 'CHEMBL3885861'
            'document_year': 'NUMERIC',
            # EXAMPLES:
            # '2019' , '2019' , '2019' , '2019' , '2019' , '2019' , '2019' , '2019' , '2019' , '2019'
            'ligand_efficiency': 
            {
                'properties': 
                {
                }
            },
            'molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL1
            # 09' , 'CHEMBL109' , 'CHEMBL109'
            'molecule_pref_name': 'TEXT',
            # EXAMPLES:
            # 'VALPROIC ACID' , 'VALPROIC ACID' , 'VALPROIC ACID' , 'VALPROIC ACID' , 'VALPROIC ACID' , 'VALPROIC ACID' 
            # , 'VALPROIC ACID' , 'VALPROIC ACID' , 'VALPROIC ACID' , 'VALPROIC ACID'
            'parent_molecule_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL109' , 'CHEMBL1
            # 09' , 'CHEMBL109' , 'CHEMBL109'
            'pchembl_value': 'NUMERIC',
            # EXAMPLES:
            # '5.68' , '5.12' , '4.98' , '5.41' , '6.46' , '4.80' , '5.94' , '4.64' , '5.41' , '4.72'
            'potential_duplicate': 'NUMERIC',
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0' , '0'
            'qudt_units': 'TEXT',
            # EXAMPLES:
            # 'http://qudt.org/vocab/unit#Percent' , 'http://qudt.org/vocab/unit#Percent' , 'http://qudt.org/vocab/unit#
            # Percent' , 'http://qudt.org/vocab/unit#SecondTime' , 'http://www.openphacts.org/units/MicrogramPerMillilit
            # er' , 'http://qudt.org/vocab/unit#Percent' , 'http://qudt.org/vocab/unit#Percent' , 'http://qudt.org/vocab
            # /unit#Percent' , 'http://qudt.org/vocab/unit#Percent' , 'http://qudt.org/vocab/unit#Percent'
            'record_id': 'NUMERIC',
            # EXAMPLES:
            # '2834606' , '2834606' , '2834606' , '2834606' , '2834606' , '2834606' , '2834606' , '2834606' , '2834606' 
            # , '2834606'
            'relation': 'TEXT',
            # EXAMPLES:
            # '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '='
            'src_id': 'NUMERIC',
            # EXAMPLES:
            # '11' , '11' , '11' , '11' , '11' , '11' , '11' , '11' , '11' , '11'
            'standard_flag': 'NUMERIC',
            # EXAMPLES:
            # '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1' , '1'
            'standard_relation': 'TEXT',
            # EXAMPLES:
            # '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '='
            'standard_type': 'TEXT',
            # EXAMPLES:
            # 'MCH' , 'MCHC' , 'RETIRBC' , 'PLAT' , 'WBC' , 'LYMLE' , 'PT' , 'FIBRINO' , 'HCT' , 'MCV'
            'standard_units': 'TEXT',
            # EXAMPLES:
            # 'pg' , '%' , '%' , 'cells.uL-1' , 'cells.uL-1' , '%' , 's' , 'ug.mL-1' , '%' , 'fL'
            'standard_value': 'NUMERIC',
            # EXAMPLES:
            # '21.3' , '34.2' , '7.8' , '1293000.0' , '8440.0' , '77.0' , '12.9' , '2240.0' , '37.4' , '62.5'
            'supplementary_data': 
            {
                'properties': 
                {
                    'as_id': 'NUMERIC',
                    # EXAMPLES:
                    # '20226' , '20227' , '20228' , '20229' , '20230' , '20235' , '20236' , '20238' , '20309' , '20310'
                    'comments': 'TEXT',
                    # EXAMPLES:
                    # 'SAMPLE_ID: 0090091' , 'SAMPLE_ID: 0090091' , 'SAMPLE_ID: 0090091' , 'SAMPLE_ID: 0090091' , 'SAMPL
                    # E_ID: 0090091' , 'SAMPLE_ID: 0090091' , 'SAMPLE_ID: 0090091' , 'SAMPLE_ID: 0090091' , 'SAMPLE_ID: 
                    # 0090101' , 'SAMPLE_ID: 0090101'
                    'relation': 'TEXT',
                    # EXAMPLES:
                    # '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '='
                    'rgid': 'NUMERIC',
                    # EXAMPLES:
                    # '7111' , '7111' , '7111' , '7111' , '7111' , '7111' , '7111' , '7111' , '7115' , '7115'
                    'standard_relation': 'TEXT',
                    # EXAMPLES:
                    # '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '=' , '='
                    'standard_text_value': 'TEXT',
                    # EXAMPLES:
                    # 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'm
                    # inimal' , 'slight'
                    'standard_type': 'TEXT',
                    # EXAMPLES:
                    # 'MCH' , 'MCHC' , 'RETIRBC' , 'PLAT' , 'WBC' , 'LYMLE' , 'PT' , 'FIBRINO' , 'HCT' , 'MCV'
                    'standard_units': 'TEXT',
                    # EXAMPLES:
                    # 'pg' , '%' , '%' , 'cells.uL-1' , 'cells.uL-1' , '%' , 's' , 'ug.mL-1' , '%' , 'fL'
                    'standard_value': 'NUMERIC',
                    # EXAMPLES:
                    # '21.5' , '33.8' , '7.3' , '1100000.0' , '7360.0' , '74.0' , '12.4' , '2770.0' , '37.9' , '62.4'
                    'text_value': 'TEXT',
                    # EXAMPLES:
                    # 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'minimal' , 'm
                    # inimal' , 'slight'
                    'type': 'TEXT',
                    # EXAMPLES:
                    # 'MCH' , 'MCHC' , 'Ret' , 'Plat' , 'WBC' , 'Lym' , 'PT' , 'Fbg' , 'Ht' , 'MCV'
                    'units': 'TEXT',
                    # EXAMPLES:
                    # 'pg' , '%' , '%' , 'x10_4/uL' , 'x10_2/uL' , '%' , 's' , 'mg/dL' , '%' , 'fL'
                    'value': 'NUMERIC',
                    # EXAMPLES:
                    # '21.5' , '33.8' , '7.3' , '110.0' , '73.6' , '74.0' , '12.4' , '277.0' , '37.9' , '62.4'
                }
            },
            'target_chembl_id': 'TEXT',
            # EXAMPLES:
            # 'CHEMBL376' , 'CHEMBL376' , 'CHEMBL376' , 'CHEMBL376' , 'CHEMBL376' , 'CHEMBL376' , 'CHEMBL376' , 'CHEMBL3
            # 76' , 'CHEMBL376' , 'CHEMBL376'
            'target_organism': 'TEXT',
            # EXAMPLES:
            # 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus
            # ' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norveg
            # icus'
            'target_pref_name': 'TEXT',
            # EXAMPLES:
            # 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus
            # ' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norvegicus' , 'Rattus norveg
            # icus'
            'target_tax_id': 'NUMERIC',
            # EXAMPLES:
            # '10116' , '10116' , '10116' , '10116' , '10116' , '10116' , '10116' , '10116' , '10116' , '10116'
            'toid': 'NUMERIC',
            # EXAMPLES:
            # '4869' , '4869' , '4869' , '4869' , '4869' , '4869' , '4869' , '4869' , '4870' , '4870'
            'type': 'TEXT',
            # EXAMPLES:
            # 'MCH' , 'MCHC' , 'Ret' , 'Plat' , 'WBC' , 'Lym' , 'PT' , 'Fbg' , 'Ht' , 'MCV'
            'units': 'TEXT',
            # EXAMPLES:
            # 'pg' , '%' , '%' , 'x10_4/uL' , 'x10_2/uL' , '%' , 's' , 'mg/dL' , '%' , 'fL'
            'uo_units': 'TEXT',
            # EXAMPLES:
            # 'UO_0000025' , 'UO_0000187' , 'UO_0000187' , 'UO_0000187' , 'UO_0000010' , 'UO_0000274' , 'UO_0000187' , '
            # UO_0000187' , 'UO_0000187' , 'UO_0000187'
            'value': 'NUMERIC',
            # EXAMPLES:
            # '21.3' , '34.2' , '7.8' , '129.3' , '84.4' , '77.0' , '12.9' , '224.0' , '37.4' , '62.5'
        }
    }
