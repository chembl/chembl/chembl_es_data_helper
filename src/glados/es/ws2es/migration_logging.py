import logging
import logging.config
import logging.handlers
import os.path
import traceback
import sys

__author__ = 'jfmosquera@ebi.ac.uk'

logging_config = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default-format': {
            'format': '%(asctime)s %(levelname)-8s %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S'
        }
    },
    'handlers': {
        'console': {
            'level': logging.DEBUG,
            'class': 'logging.StreamHandler',
            'formatter': 'default-format',
        },
    },
    'loggers': {
        'elasticsearch': {
            'handlers': ['console'],
            'level': logging.ERROR,
            'propagate': True,
        },
        'elasticsearch.trace': {
            'handlers': ['console'],
            'level': logging.ERROR,
            'propagate': True,
        },
        'chembl_ws2es': {
            'handlers': ['console'],
            'level': logging.DEBUG,
            'propagate': True,
        }
    },
}
logging_config_loaded = False


def setup_migration_logging(level=logging.DEBUG, es_level=logging.ERROR, output_dir_path=None):
    global logging_config, logging_config_loaded
    if not logging_config_loaded:
        logging.config.dictConfig(logging_config)
        logging_config_loaded = True
    logging.getLogger('chembl_ws2es').setLevel(level)
    logging.getLogger('elasticsearch').setLevel(es_level)
    logging.getLogger('elasticsearch.trace').setLevel(es_level)
    if output_dir_path:
        try:
            if not os.path.exists(output_dir_path):
                try:
                    os.makedirs(output_dir_path)
                except:
                    traceback.print_exc(file=sys.stderr)
                    print('{0} is an invalid path for progress bar output.'.format(output_dir_path),
                          file=sys.stderr)
                    sys.exit(1)
            if not os.path.isdir(output_dir_path):
                print('{0} is not a directory.'.format(output_dir_path), file=sys.stderr)
                sys.exit(1)

            test_path = os.path.join(output_dir_path, 'test_log.out')
            test_text = 'JUST SOME TESTING . . .'
            with open(test_path, 'w') as test_file:
                test_file.write(test_text)
            with open(test_path, 'r') as test_file:
                file_lines = test_file.readlines()
                if len(file_lines) != 1 and file_lines[0] != test_text:
                    raise IOError('Testing file contents do not match.')
            os.remove(test_path)

            ws2es_log_path = os.path.join(output_dir_path, 'ws2es.log')
            ws2es_handler = logging.FileHandler(ws2es_log_path, 'w')
            logging.getLogger('chembl_ws2es').addHandler(ws2es_handler)

            es_log_path = os.path.join(output_dir_path, 'elasticsearch.log')
            es_handler = logging.FileHandler(es_log_path, 'w')
            logging.getLogger('elasticsearch').handlers = [es_handler]

            es_trace_log_path = os.path.join(output_dir_path, 'elasticsearch.trace.log')
            es_trace_handler = logging.FileHandler(es_trace_log_path, 'w')
            logging.getLogger('elasticsearch').handlers = [es_trace_handler]
        except:
            traceback.print_exc(file=sys.stderr)
            print('ERROR: Can\'t use the path {0} to store logs.'.format(output_dir_path), file=sys.stderr)
            sys.exit(1)


setup_migration_logging()
