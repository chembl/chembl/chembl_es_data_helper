from glados.es.ws2es.denormalization import DenormalizationHandler
from glados.es.ws2es.es_util import DefaultMappings, es_util
from glados.es.ws2es.resources_description import PROTEIN_CLASS
import glados.es.ws2es.progress_bar_handler as progress_bar_handler
import logging


logger = logging.getLogger('chembl_ws2es')


class ProteinClassDenormalizationHandler(DenormalizationHandler):

    RESOURCE = DenormalizationHandler.AVAILABLE_RESOURCES.PROTEIN_CLASSIFICATION

    METADATA_MAPPING = {
        'properties':
        {
            '_metadata':
            {
                'properties':
                {
                    'protein_classification':
                    {
                        'properties':
                        {
                            'l1': DefaultMappings.KEYWORD,
                            'l2': DefaultMappings.KEYWORD,
                            'l3': DefaultMappings.KEYWORD,
                            'l4': DefaultMappings.KEYWORD,
                            'l5': DefaultMappings.KEYWORD,
                            'l6': DefaultMappings.KEYWORD,
                            'l1_short': DefaultMappings.ALT_NAME,
                            'l2_short': DefaultMappings.ALT_NAME,
                            'l3_short': DefaultMappings.ALT_NAME,
                            'l4_short': DefaultMappings.ALT_NAME,
                            'l5_short': DefaultMappings.ALT_NAME,
                            'l6_short': DefaultMappings.ALT_NAME,
                            'l1_desc': DefaultMappings.KEYWORD,
                            'l2_desc': DefaultMappings.KEYWORD,
                            'l3_desc': DefaultMappings.KEYWORD,
                            'l4_desc': DefaultMappings.KEYWORD,
                            'l5_desc': DefaultMappings.KEYWORD,
                            'l6_desc': DefaultMappings.KEYWORD,
                            'l1_definition': DefaultMappings.TEXT_STD,
                            'l2_definition': DefaultMappings.TEXT_STD,
                            'l3_definition': DefaultMappings.TEXT_STD,
                            'l4_definition': DefaultMappings.TEXT_STD,
                            'l5_definition': DefaultMappings.TEXT_STD,
                            'l6_definition': DefaultMappings.TEXT_STD,
                            'protein_class_id': DefaultMappings.ID_REF,
                        }

                    }
                }
            }
        }
    }

    @staticmethod
    def get_new_index_mappings():
        return ProteinClassDenormalizationHandler.METADATA_MAPPING['properties']['_metadata']['properties']['protein_classification']

    def __init__(self):
        super().__init__()
        self.protein_class_aggregated_by_id = {}
        self.protein_class_by_level = [[] for i in range(0, 7)]
        self.protein_class_by_id = {}
        self.generated_resource = PROTEIN_CLASS

    def handle_doc(self, doc: dict, total_docs: int, index: int, first: bool, last: bool):
        self.protein_class_by_id['{0}'.format(doc['protein_class_id'])] = doc
        self.protein_class_by_level[doc['class_level']].append(doc)

    def get_protein_family_class_aggregated(self, fpc: {}):
        parent_id = fpc['parent_id']
        if parent_id is None:
            # Root node defines the base document to be used by the next nodes
            ret_doc = {'protein_class_id': fpc['protein_class_id'], 'class_level': 0}
            for i in range(1, 7):
                ret_doc['l{0}'.format(i)] = None
                ret_doc['l{0}_short'.format(i)] = None
                ret_doc['l{0}_desc'.format(i)] = None
                ret_doc['l{0}_definition'.format(i)] = None
            return ret_doc
        else:
            pc_level = fpc['class_level']
            # Copies the parent document to be used as base document the tree needs to be iterated by levels
            base_doc = self.protein_class_aggregated_by_id[parent_id].copy()
            base_doc['protein_class_id'] = fpc['protein_class_id']
            base_doc['class_level'] = fpc['class_level']
            base_doc['l{0}'.format(pc_level)] = fpc['pref_name']
            base_doc['l{0}_short'.format(pc_level)] = fpc['short_name']
            base_doc['l{0}_desc'.format(pc_level)] = fpc['protein_class_desc']
            base_doc['l{0}_definition'.format(pc_level)] = fpc['definition']
            return base_doc

    def summarize_class_families(self):
        for i in range(0, 7):
            cur_level_fpc = self.protein_class_by_level[i]
            for fpc_j in cur_level_fpc:
                self.protein_class_aggregated_by_id[fpc_j['protein_class_id']] = \
                    self.get_protein_family_class_aggregated(fpc_j)
        self.log_protein_families()

    def log_protein_families(self):
        for k, v in self.protein_class_aggregated_by_id.items():
            logger.info('PC_ID: {0} => {1}'.format(k, [v['l{0}'.format(i)] for i in range(1, 7)]))
        logger.info('TOTAL PROTEIN CLASSIFICATIONS {0} OUT OF {1} records'.format(
            len(self.protein_class_aggregated_by_id), len(self.protein_class_by_id)))

    def save_denormalization_for_new_index(self):
        es_util.delete_idx(self.generated_resource.idx_name)
        es_util.create_idx(
            self.generated_resource.idx_name, 1, 1, analysis=DefaultMappings.COMMON_ANALYSIS,
            mappings=ProteinClassDenormalizationHandler.get_new_index_mappings()
        )

        dn_dict = {}

        logger.info('{0} GROUPED RECORDS WERE FOUND'.format(len(self.protein_class_aggregated_by_id)))
        p_bar = progress_bar_handler.get_new_progressbar('protein_class_family-dn-generation',
                                                         len(self.protein_class_aggregated_by_id))
        i = 0
        for pc_class_id, pc_class_family in self.protein_class_aggregated_by_id.items():
            dn_dict[pc_class_id] = pc_class_family
            i += 1
            p_bar.update(i)
        p_bar.finish()

        self.save_denormalization_dict(
            self.generated_resource, dn_dict, DenormalizationHandler.default_update_script_and_size, do_index=True
        )
