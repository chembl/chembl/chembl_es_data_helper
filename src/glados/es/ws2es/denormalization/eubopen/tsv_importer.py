import csv
import pathlib
import logging
import re
import time

import glados.es.ws2es.util as util
from glados.es.ws2es.es_util import es_util, DefaultMappings
from glados.es.ws2es.resources_description import \
    EUBOPEN_MOLECULE, EUBOPEN_TARGET, EUBOPEN_ACTIVITY, EUBOPEN_ASSAY, EUBOPEN_MAIN_TARGET
from glados.es.ws2es.denormalization import DenormalizationHandler
import glados.es.ws2es.progress_bar_handler as progress_bar_handler
from glados.es.ws2es.denormalization.eubopen import assay_aidx_types

__author__ = 'jfmosquera@ebi.ac.uk'

logger = logging.getLogger('chembl_ws2es')

base_dir = pathlib.Path(__file__).parent.resolve().joinpath('extra-flat-files')

compound_annotations_path = str(base_dir.joinpath('COMPOUND_ANNOTATION.tsv'))
compound_relations_path = str(base_dir.joinpath('COMPOUND_RELATIONS.tsv'))
main_targets_path = str(base_dir.joinpath('MAIN_TARGETS.tsv'))
assay_aidx_fix_path = str(base_dir.joinpath('ASSAY_AIDX_FIX.tsv'))
assay_tmshift_qc_path = str(base_dir.joinpath('ASSAY_TMSHIFT_QC_LIABILITY.tsv'))


molecule_data = {}
main_targets_id_seq = 0
main_targets_data = []
molecule_data_from_mt = {}
target_data_from_mt = {}
assay_dn_data = {}

molecule_chembl_id_2_eubopen_id = {}
target_chembl_id_2_eubopen_id = {}

# ----------------------------------------------------------------------------------------------------------------------
# PROTEIN FAMILY LABELS
# ----------------------------------------------------------------------------------------------------------------------

protein_family_labels = {
    'AT': 'Histone Acetyltransferases',
    'BD': 'Bromodomains',
    'CA': 'Carbonic Anhydrase',
    'CD': 'Chromo Domains',
    'DA': 'Histone Deacetylases',
    'DE': 'Deaminases',
    'DP': 'Diverse compounds for screening in disease platforms ',
    'DR': 'WDR targets',
    'DU': 'Pyrazine Deubiqitinase',
    'EP': 'Epigenetics Peptide',
    'EL': 'E3 ligases',
    'EO': 'Epigenetic other',
    'F9': 'F9 Fragments',
    'FM': 'Fragments',
    'GA': 'GTPases',
    'GC': 'Guanylate Cyclase',
    'GR': 'GPCR',
    'HA': 'NCGC Helicase',
    'HD': 'HDAC',
    'HL': 'Helicase',
    'HM': 'HMT',
    'HY': 'Other Hydrolases',
    'IC': 'Ion Channel',
    'KC': 'Potassium Channel',
    'KD': 'Lysine Demethylases',
    'MB': 'MBT Domain',
    'ME': 'Membranse Associated Enzymes',
    'MI': 'Membrane Associated',
    'MR': 'Methyl-lysine Readers',
    'MT': 'Histone Methyltransferases',
    'NC': 'NCGS',
    'NK': 'Non-protein Kinase',
    'NR': 'Nuclear Receptors',
    'NU': 'NUDIX',
    'OR': 'Other Oxidoreductases',
    'PA': 'Parasitology',
    'PD': 'PHD Domains',
    'PH': 'PHD Peptides',
    'PK': 'Protein Kinase',
    'PR': 'PARPs',
    'PT': 'Proteases',
    'PY': 'Tyrosine Phosphate',
    'RD': 'RNA-dependent RNA polymerase',
    'RN': 'RNA Molecules',
    'SD': 'Structure-based Drug Design Consortium/ Short chain dehydrogenase',
    'SG': 'Small GTPases',
    'SN': 'Sirtuin',
    'SP': 'KDM Peptides',
    'SR': 'RNA Probes',
    'ST': 'Sulfotransferase',
    'TD': 'Tudor Domains',
    'TF': 'Other Transferases',
    'TI': 'PHD Domains',
    'TP': 'ATPase',
    'UB': 'Ubiqitin',
    'VP': 'VPS System vsp35/vsp29',
    'WD': 'WD40 repeat',
    'WY': 'Organelle Biogenesis',
    'XS': 'Cross Target Family',
    'YP': 'YAP-TEAD',
    'YT': 'YEATS',
    'ZP': 'Miscellaneous Peptides',
    'ZZ': 'Miscellaneous Compounds',
}

# ----------------------------------------------------------------------------------------------------------------------
# UTILS
# ----------------------------------------------------------------------------------------------------------------------


def empty_str_2_none(text: str):
    return None if len(text.strip()) == 0 else text.strip()


def reduce_spaces(text: str):
    text = empty_str_2_none(text)
    if text:
        return re.sub(r'\s+', ' ', text)
    return None


def standarize_mode_of_action(mode_of_action: str):
    mode_of_action = empty_str_2_none(mode_of_action)
    if mode_of_action:
        return mode_of_action.upper()
    return mode_of_action

# ----------------------------------------------------------------------------------------------------------------------
# CHEMBL IDS TO EUBOPEN IDS
# ----------------------------------------------------------------------------------------------------------------------


def load_chembl_ids_to_eubopen_ids():
    global molecule_chembl_id_2_eubopen_id, target_chembl_id_2_eubopen_id

    def map_molecule_chembl_id(eub_molecule_doc, *args, **kwargs):
        molecule_chembl_id_2_eubopen_id[eub_molecule_doc['molecule_chembl_id']] \
            = eub_molecule_doc['molecule_eubopen_id']

    def map_target_chembl_id(eub_target_doc, *args, **kwargs):
        target_chembl_id_2_eubopen_id[eub_target_doc['target_chembl_id']] \
            = eub_target_doc['target_eubopen_id']

    es_util.scan_index(EUBOPEN_MOLECULE.idx_name, on_doc=map_molecule_chembl_id)
    es_util.scan_index(EUBOPEN_TARGET.idx_name, on_doc=map_target_chembl_id)

# ----------------------------------------------------------------------------------------------------------------------
# LOAD TSV FILES
# ----------------------------------------------------------------------------------------------------------------------


def yes_no_2_bool(text: str):
    if text is None:
        return None
    bool_val = None
    text = text.strip().lower()
    if text == 'yes' or text == 'y':
        bool_val = True
    elif text == 'no' or text == 'n':
        bool_val = False
    else:
        logger.warning('UNKNOWN YES/NO VALUE: "{0}"'.format(text))
    return bool_val


def load_compound_annotation_file():
    with open(compound_annotations_path) as tsv_file:
        tsv_reader = csv.reader(tsv_file, delimiter='\t')
        for row in tsv_reader:
            chembl_id = empty_str_2_none(row[1])
            if chembl_id.lower() == 'MOLECULE_CHEMBL_ID'.lower():
                continue
            if chembl_id is None or not chembl_id.startswith('CHEMBL'):
                logger.warning('MALFORMED CHEMBL_ID FOR COMPOUND ANNOTATION')
                continue

            eubopen_id = molecule_chembl_id_2_eubopen_id.get(chembl_id, None)
            if eubopen_id is None:
                logger.warning(f'EUBOPEN ID NOT FOUND FOR MOLECULE CHEMBL ID: {chembl_id}')
                continue

            if eubopen_id not in molecule_data:
                molecule_data[eubopen_id] = {}

            in_vivo_use = yes_no_2_bool(row[3])
            if in_vivo_use is None:
                logger.warning('MISSING ANNOTATION FOR {0}'.format(eubopen_id))
            else:
                if eubopen_id in molecule_data and 'in_vivo_use' in molecule_data[eubopen_id]\
                        and molecule_data[eubopen_id]['in_vivo_use'] != in_vivo_use:
                    logger.warning('DUPLICATE CHEMBL ID ANNOTATION FOR {0}'.format(eubopen_id))
                molecule_data[eubopen_id]['in_vivo_use'] = in_vivo_use


def load_compound_relations_file():
    global molecule_chembl_id_2_eubopen_id
    with open(compound_relations_path) as tsv_file:
        tsv_reader = csv.reader(tsv_file, delimiter='\t')
        for row in tsv_reader:
            chembl_id = empty_str_2_none(row[1])
            # skip first row
            if chembl_id.lower() == 'MOLECULE_CHEMBL_ID'.lower():
                continue

            eubopen_id = molecule_chembl_id_2_eubopen_id.get(chembl_id, None)
            if eubopen_id is None:
                logger.warning(f'EUBOPEN ID NOT FOUND FOR MOLECULE CHEMBL ID: {chembl_id}')
                continue

            chembl_id_2 = empty_str_2_none(row[4])
            eubopen_id_2 = molecule_chembl_id_2_eubopen_id.get(chembl_id_2, None)
            if eubopen_id_2 is None:
                logger.warning(f'EUBOPEN ID NOT FOUND FOR MOLECULE CHEMBL ID: {chembl_id_2}')
                continue

            if eubopen_id is None or eubopen_id_2 is None:
                logger.warning('MISSING EUBOPEN ID FOR COMPOUND RELATION')
                continue

            if eubopen_id not in molecule_data:
                molecule_data[eubopen_id] = {}
            if eubopen_id_2 not in molecule_data:
                molecule_data[eubopen_id_2] = {}

            if row[2] == 'HAS_CONTROL':
                if 'is_control_for' not in molecule_data[eubopen_id_2]:
                    molecule_data[eubopen_id_2]['is_control_for'] = []
                molecule_data[eubopen_id_2]['is_control_for'].append(eubopen_id)

                if 'has_control' not in molecule_data[eubopen_id]:
                    molecule_data[eubopen_id]['has_control'] = []
                molecule_data[eubopen_id]['has_control'].append(eubopen_id_2)
            elif row[2] == 'HAS_PROBE':
                if 'is_probe_for' not in molecule_data[eubopen_id_2]:
                    molecule_data[eubopen_id_2]['is_probe_for'] = []
                molecule_data[eubopen_id_2]['is_probe_for'].append(eubopen_id)

                if 'has_probe' not in molecule_data[eubopen_id]:
                    molecule_data[eubopen_id]['has_probe'] = []
                molecule_data[eubopen_id]['has_probe'].append(eubopen_id_2)


def load_main_targets_file():
    global main_targets_id_seq, molecule_data_from_mt, target_data_from_mt
    with open(main_targets_path) as tsv_file:
        tsv_reader = csv.reader(tsv_file, delimiter='\t')
        first_row = True
        for row in tsv_reader:
            if first_row or len(row) < 10:
                first_row = False
                continue

            eub_scarab = empty_str_2_none(row[2])
            molecule_chembl_id = None
            molecule = None
            if eub_scarab:
                molecule = EUBOPEN_MOLECULE.get_doc_by_id_from_es(eub_scarab, metadata=True)
                if not molecule:
                    eub_scarab = eub_scarab[:10]
                    molecule = EUBOPEN_MOLECULE.get_doc_by_id_from_es(eub_scarab, metadata=True)
            if molecule:
                molecule_chembl_id = molecule['molecule_chembl_id']
            else:
                molecule_chembl_id = empty_str_2_none(row[3])
                molecule = EUBOPEN_MOLECULE.get_doc_by_chembl_id_from_es(molecule_chembl_id)
            if molecule is None:
                logger.warning(f'MISSING MOLECULE IN EUBOPEN DATA: EUB ID: {eub_scarab} AND CHEMBL ID: {molecule_chembl_id}')
                continue

            molecule_eubopen_id = molecule['molecule_eubopen_id']

            target_chembl_id = empty_str_2_none(row[8])
            target_eubopen_id = None
            if target_chembl_id:
                target = EUBOPEN_TARGET.get_doc_by_chembl_id_from_es(target_chembl_id)
                if target is None:
                    logger.warning(f'MISSING TARGET IN EUBOPEN DATA {target_chembl_id}')
                    continue
                target_eubopen_id = target['target_eubopen_id']
            protein_family = empty_str_2_none(row[4])
            recommended_cell_concentration = reduce_spaces(row[10])
            main_targets_id_seq += 1
            main_targets_data.append({
                'main_target_id': str(main_targets_id_seq),
                'molecule_chembl_id': molecule_chembl_id,
                'target_chembl_id': target_chembl_id,
                'mode_of_action': standarize_mode_of_action(row[9]),
            })

            # Data to be included in molecule and target
            molecule_data_from_mt[molecule_eubopen_id] = {
                'recommended_cell_concentration': recommended_cell_concentration
            }
            if target_eubopen_id:
                if protein_family_labels.get(protein_family, None) is None:
                    logger.warning(f'UNKNOWN PROTEIN FAMILY LABEL FOR {protein_family}')
                target_data_from_mt[target_eubopen_id] = {
                    'protein_family': {
                        'short': protein_family,
                        'label': protein_family_labels.get(protein_family, None)
                    }
                }


def load_assay_classification_files():
    global assay_dn_data
    with open(assay_aidx_fix_path) as tsv_file:
        tsv_reader = csv.reader(tsv_file, delimiter='\t')
        first_row = True
        for row in tsv_reader:
            if first_row or len(row) < 2:
                first_row = False
                continue
            assay_chembl_id = empty_str_2_none(row[0])
            if not assay_chembl_id:
                logger.warning('ROW MISSING ASSAY CHEMBL ID')
                continue
            assay_eub_type = empty_str_2_none(row[1])
            if assay_eub_type == 'Selectivity':
                assay_eub_type = 'S'
            if assay_eub_type == 'Tm assay':
                assay_eub_type = 'TSA'
            if assay_eub_type not in assay_aidx_types:
                logger.warning(f'UNKNOWN ASSAY TYPE FOR ASSAY {assay_chembl_id} - {assay_eub_type}')
            assay_dn_data[assay_chembl_id] = {
                'assay_type': {
                    'type': [{
                        'acronym': assay_eub_type,
                        'description': assay_aidx_types[assay_eub_type]['name']
                    }]
                }
            }

    with open(assay_tmshift_qc_path) as tsv_file:
        tsv_reader = csv.reader(tsv_file, delimiter='\t')
        first_row = True
        for row in tsv_reader:
            if first_row or len(row) < 2:
                first_row = False
                continue
            assay_chembl_id = empty_str_2_none(row[0])
            if not assay_chembl_id:
                logger.warning('ROW MISSING ASSAY CHEMBL ID')
                continue
            tm_shift_type = {
                'acronym': 'TSA-QC-L',
                'description': 'Thermal shift assay QC Liability'
            }
            if assay_chembl_id in assay_dn_data:
                assay_dn_data[assay_chembl_id]['assay_type']['type'].append(tm_shift_type)
            else:
                assay_dn_data[assay_chembl_id] = {
                    'assay_type': {
                        'type': [tm_shift_type]
                    }
                }


# ----------------------------------------------------------------------------------------------------------------------
# LOAD TSV FILES
# ----------------------------------------------------------------------------------------------------------------------


def load_data_in_es():
    load_chembl_ids_to_eubopen_ids()
    load_compound_annotation_file()
    load_compound_relations_file()
    load_main_targets_file()
    load_assay_classification_files()
    compound_tsv_dh = CompoundTSVDenormalizationHandler(molecule_data, molecule_data_from_mt)
    compound_tsv_dh.scan_data_from_es()
    target_tsv_dh = TargetTSVDenormalizationHandler(target_data_from_mt)
    target_tsv_dh.scan_data_from_es()
    # let elastic search sink in the recommended concentration and the protein family
    time.sleep(60*3)
    main_target_tsv_dh = MainTargetTSVDenormalizationHandler(main_targets_data)
    main_target_tsv_dh.save_denormalization()
    assay_tsv_dh = AssayTSVDenormalizationHandler(assay_dn_data)
    assay_tsv_dh.scan_data_from_es(include_metadata=True)

    activity_tsv_dh = ActivityTSVDenormalizationHandler(assay_tsv_dn=assay_tsv_dh)
    activity_tsv_dh.scan_data_from_es()
    activity_tsv_dh.save_denormalization()

# ----------------------------------------------------------------------------------------------------------------------
# DENORMALIZERS TO LOAD DATA INTO ES
# ----------------------------------------------------------------------------------------------------------------------


class CompoundTSVDenormalizationHandler(DenormalizationHandler):

    RESOURCE = EUBOPEN_MOLECULE

    def __init__(self, eubopen_data: dict, molecule_data_from_mt_tsv: dict
                 ):
        super().__init__(eubopen_data is not None and molecule_data_from_mt_tsv is not None)
        self.eubopen_data = eubopen_data
        self.main_targets_dn_data_for_molecule = molecule_data_from_mt_tsv

    def get_custom_mappings_for_complete_data(self):
        mappings = {}
        eubopen_mappings = {
                                'in_vivo_use': DefaultMappings.BOOLEAN,
                                'is_control_for': DefaultMappings.CHEMBL_ID_REF,
                                'has_control': DefaultMappings.CHEMBL_ID_REF,
                                'is_probe_for': DefaultMappings.CHEMBL_ID_REF,
                                'has_probe': DefaultMappings.CHEMBL_ID_REF,
                                'is_probe': DefaultMappings.BOOLEAN,
                                'is_control': DefaultMappings.BOOLEAN,
                                'is_chemogenomic_probe': DefaultMappings.BOOLEAN,
                                'recommended_cell_concentration': DefaultMappings.KEYWORD
                            }
        util.put_js_path_in_dict(mappings, '._metadata.eubopen.', eubopen_mappings, es_properties_style=True)
        return mappings

    def get_doc_for_complete_data(self, doc: dict):
        global molecule_chembl_id_2_eubopen_id
        molecule_chembl_id = doc['molecule_chembl_id']
        molecule_eubpopen_id = molecule_chembl_id_2_eubopen_id.get(molecule_chembl_id, None)
        if molecule_eubpopen_id is None:
            logger.warning(f'MISSING EUBOPEN ID FOR MOLECULE: {molecule_chembl_id}')
            return

        eubopen_metadata = util.SummableDict()
        if molecule_eubpopen_id in self.eubopen_data:
            eubopen_metadata += self.eubopen_data[molecule_eubpopen_id]

        eubopen_metadata['is_probe'] = 'is_probe_for' in eubopen_metadata and len(eubopen_metadata['is_probe_for']) > 0

        eubopen_metadata['is_control'] = 'is_control_for' in eubopen_metadata\
                                         and len(eubopen_metadata['is_control_for']) > 0

        eubopen_metadata['is_chemogenomic_probe'] = molecule_eubpopen_id in self.main_targets_dn_data_for_molecule
        if molecule_eubpopen_id in self.main_targets_dn_data_for_molecule:
            eubopen_metadata += self.main_targets_dn_data_for_molecule[molecule_eubpopen_id]

        return util.put_js_path_in_dict({}, '_metadata.eubopen', eubopen_metadata)


class TargetTSVDenormalizationHandler(DenormalizationHandler):

    RESOURCE = EUBOPEN_TARGET

    def __init__(self, target_data_from_mt_tsv: dict):
        super().__init__(target_data_from_mt_tsv is not None)
        self.main_targets_dn_data_for_target = target_data_from_mt_tsv

    def get_custom_mappings_for_complete_data(self):
        mappings = {}
        eubopen_mappings = {
            'has_chemogenomic_probe': DefaultMappings.BOOLEAN,
            'protein_family': {
                'properties':
                {
                    'short': DefaultMappings.KEYWORD,
                    'label': DefaultMappings.LOWER_CASE_KEYWORD
                }
            }
        }
        util.put_js_path_in_dict(mappings, '._metadata.eubopen.', eubopen_mappings, es_properties_style=True)
        return mappings

    def get_doc_for_complete_data(self, doc: dict):
        global target_chembl_id_2_eubopen_id
        target_chembl_id = doc['target_chembl_id']
        target_eubpopen_id = target_chembl_id_2_eubopen_id.get(target_chembl_id, None)
        if target_eubpopen_id is None:
            logger.warning(f'MISSING EUBOPEN ID FOR TARGET: {target_chembl_id}')
            return

        eubopen_metadata = util.SummableDict()

        eubopen_metadata += {'has_chemogenomic_probe': target_eubpopen_id in self.main_targets_dn_data_for_target}

        if target_eubpopen_id in self.main_targets_dn_data_for_target:
            eubopen_metadata += self.main_targets_dn_data_for_target[target_eubpopen_id]

        return util.put_js_path_in_dict({}, '_metadata.eubopen', eubopen_metadata)


class MainTargetTSVDenormalizationHandler(DenormalizationHandler):

    RESOURCE = EUBOPEN_MAIN_TARGET

    @staticmethod
    def get_new_index_mappings():
        return {
            'properties': {
                'molecule':
                {
                    'properties': EUBOPEN_MOLECULE.get_resource_mapping_from_es()
                },
                'target':
                {
                    'properties': EUBOPEN_TARGET.get_resource_mapping_from_es()
                },
                'main_target': {
                    'properties': {
                        'main_target_id': DefaultMappings.ID,
                        'molecule_chembl_id': DefaultMappings.CHEMBL_ID_REF,
                        'target_chembl_id': DefaultMappings.CHEMBL_ID_REF,
                        'mode_of_action': DefaultMappings.KEYWORD,
                    }
                }
            }
        }

    def __init__(self, main_targets_tsv_data):
        super().__init__(False)
        self.main_targets_tsv_data = main_targets_tsv_data

    def save_denormalization(self):
        if self.main_targets_tsv_data:
            es_util.delete_idx(self.RESOURCE.idx_name)
            es_util.create_idx(
                self.RESOURCE.idx_name, 3, 1, analysis=DefaultMappings.COMMON_ANALYSIS,
                mappings=MainTargetTSVDenormalizationHandler.get_new_index_mappings()
            )

            dn_dict = {}

            logger.info('{0} EUBOPEN MAIN TARGET RECORDS'.format(len(self.main_targets_tsv_data)))
            p_bar = progress_bar_handler.get_new_progressbar('eubopen-main-target-dn-generation',
                                                             len(self.main_targets_tsv_data))

            i = 0
            for main_target_i in self.main_targets_tsv_data:
                new_main_target_doc = {
                    'molecule': EUBOPEN_MOLECULE.get_doc_by_chembl_id_from_es(main_target_i['molecule_chembl_id']),
                    'main_target': main_target_i
                }
                target_chembl_id = main_target_i.get('target_chembl_id', None)
                new_main_target_doc['target'] = EUBOPEN_TARGET.get_doc_by_chembl_id_from_es(target_chembl_id)
                dn_dict[main_target_i['main_target_id']] = new_main_target_doc
                i += 1
                p_bar.update(i)
            p_bar.finish()

            def update_script_and_size(doc_id, es_doc):
                return es_doc, util.count_fields_in_doc(es_doc)*1000

            self.save_denormalization_dict(
                self.RESOURCE, dn_dict, update_script_and_size, do_index=True
            )


class AssayTSVDenormalizationHandler(DenormalizationHandler):

    RESOURCE = EUBOPEN_ASSAY

    def __init__(self, assay_tsv_data: dict):
        super().__init__(assay_tsv_data is not None)
        self.assay_tsv_data = assay_tsv_data
        self.eub_assay_type_by_chembl_id = {}

    def get_doc_for_complete_data(self, doc: dict):
        assay_chembl_id = doc['assay_chembl_id']

        eubopen_data = doc['_metadata'].get('eubopen', None)

        if not eubopen_data:
            logger.warning(f'ASSAY {assay_chembl_id} IS MISSING EUBOPEN METADATA')
            logger.error(doc['_metadata'])
            return {}

        current_types = eubopen_data.get('assay_type', {}).get('type', [{}])

        new_types = []
        total_types = current_types
        if assay_chembl_id in self.assay_tsv_data:
            fix_types = self.assay_tsv_data[assay_chembl_id]['assay_type'].get('type', [{}])
            total_types = fix_types + current_types

        is_tsa = False
        is_tsa_qc = False
        existing_types = set()
        for type_i in total_types:
            acronym_i = type_i['acronym']
            description_i = type_i['description']
            if description_i is None:
                continue
            if acronym_i not in existing_types:
                existing_types.add(acronym_i)
                new_types.append(type_i)
            if acronym_i == 'TSA':
                is_tsa = True
            if acronym_i == 'TSA-QC-L':
                is_tsa = True
                is_tsa_qc = True
        if is_tsa and not is_tsa_qc:
            new_types = [{
                'acronym': 'TSA',
                'description': 'Thermal shift assay'
            }]
        elif is_tsa and is_tsa_qc:
            new_types = [{
                'acronym': 'TSA-QC-L',
                'description': 'Thermal shift assay QC Liability'
            }]

        eubopen_dn_dict = {}
        if new_types is not None:
            eubopen_dn_dict = {
                'assay_type': {
                    'type': new_types
                }
            }

        self.eub_assay_type_by_chembl_id[assay_chembl_id] = new_types

        return util.put_js_path_in_dict({}, '_metadata.eubopen', eubopen_dn_dict)


class ActivityTSVDenormalizationHandler(DenormalizationHandler):

    RESOURCE = EUBOPEN_ACTIVITY

    def __init__(self, assay_tsv_dn: AssayTSVDenormalizationHandler):
        super().__init__(assay_tsv_dn is not None)
        self.assay_tsv_dn = assay_tsv_dn
        self.doi_pubmed_links = {}
        self.doi_pubmed_pattern = re.compile(
            r'(https?\:\/\/[^\s,]*(?:doi|pubmed|nature\.com|PMID|24171924|21168764)[^\s,]*)', re.IGNORECASE
        )

    def get_doc_for_complete_data(self, doc: dict):

        assay_chembl_id = doc['assay_chembl_id']

        new_types = self.assay_tsv_dn.eub_assay_type_by_chembl_id.get(assay_chembl_id, [])

        eubopen_dn_dict = {}
        if new_types is not None:
            eubopen_dn_dict = {
                'assay_type': {
                    'type': new_types
                }
            }

        for act_prop_i in doc.get('activity_properties', []):
            text_value = act_prop_i.get('text_value', None)
            if text_value:
                matches = self.doi_pubmed_pattern.findall(text_value)
                matches = list(set(matches))
                self.doi_pubmed_links[assay_chembl_id] = matches
                if len(matches) > 1:
                    logger.warning(f'ASSAY: {assay_chembl_id} WITH TEXT: {text_value} - CONTAINED MULTIPLE LINKS: {matches}')

        return util.put_js_path_in_dict({}, '_metadata.eubopen', eubopen_dn_dict)

    def save_denormalization(self):
        if self.doi_pubmed_links:
            es_util.update_doc_type_mappings(
                EUBOPEN_ASSAY.idx_name,
                mappings={
                    'properties': {
                        '_metadata': {
                            'properties': {
                                'eubopen': {
                                    'properties': {
                                        'doi_pubmed_link': DefaultMappings.KEYWORD
                                    }
                                }
                            }
                        }
                    }
                }
            )

            dn_dict = {}
            for assay_chembl_id, doi_pubmed_link in self.doi_pubmed_links.items():
                dn_dict[assay_chembl_id] = {
                    '_metadata': {
                        'eubopen': {
                            'doi_pubmed_link': doi_pubmed_link
                        }
                    }
                }

            def update_script_and_size(doc_id, es_doc):
                return es_doc, util.count_fields_in_doc(es_doc)*2

            self.save_denormalization_dict(
                EUBOPEN_ASSAY, dn_dict, update_script_and_size
            )
