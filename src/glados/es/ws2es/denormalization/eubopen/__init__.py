import sys

from glados.es.ws2es.resources_description import ResourceDescription
from glados.es.ws2es.resources_description import MOLECULE, EUBOPEN_MOLECULE
from glados.es.ws2es.resources_description import ACTIVITY, EUBOPEN_ACTIVITY
from glados.es.ws2es.resources_description import ASSAY, EUBOPEN_ASSAY
from glados.es.ws2es.resources_description import TARGET, EUBOPEN_TARGET
from glados.es.ws2es.es_util import es_util, DefaultMappings
from glados.es.ws2es.denormalization.activity_handler import ActivityDenormalizationHandler
import glados.es.ws2es.util as util
import glados.es.ws2es.progress_bar_handler as progress_bar_handler
import re
import logging

__author__ = 'jfmosquera@ebi.ac.uk'

# they need to be strings otherwise the ' OR ' string join will fail due to st
eubopen_src_ids = [54, 55, 65]
eubopen_src_ids_es_query_string = '('+' OR '.join([f'{src_id_i}' for src_id_i in eubopen_src_ids])+')'

molecule_ebuopen_ids_fix = {
    'CHEMBL4796803': 'EUB0000289bCl',
    'CHEMBL4586794': 'EUB0000289',
    'CHEMBL4513786': 'EUB0000289c',
    'CHEMBL4750155': 'EUB0000326a',
    'CHEMBL4536197': 'EUB0000326',
    'CHEMBL3697701': 'EUB0000308',
    'CHEMBL377219': 'EUB0000308b',
    'CHEMBL3609637': 'EUB0000753',
    'CHEMBL3609749': 'EUB0000753a',
    'CHEMBL440687': 'EUB0000290',
    'CHEMBL1600943': 'EUB0000290bCl',
    'CHEMBL111612': 'EUB0000348',
    'CHEMBL4761843': 'EUB0000348aPTSA',
    'CHEMBL4741904': 'EUB0000710a',
    'CHEMBL4543737': 'EUB0000710',
    'CHEMBL4745071': 'EUB0000337aCl',
    'CHEMBL4566600': 'EUB0000337',
    'CHEMBL4533008': 'EUB0000332a',
    'CHEMBL4630641': 'EUB0000332',
    'CHEMBL3685796': 'EUB0000714',
    'CHEMBL4530401': 'EUB0000704',
    'CHEMBL4075278': 'EUB0000316b',
    'CHEMBL4578164': 'EUB0000316',
    'CHEMBL3799292': 'EUB0000300',
    'CHEMBL4753450': 'EUB0000300bCl',
    'CHEMBL3799512': 'EUB0000303',
    'CHEMBL3800596': 'EUB0000303b',
    'CHEMBL4754103': 'EUB0000328a',
    'CHEMBL4282264': 'EUB0000328',
    'CHEMBL3752911': 'EUB0000199c',
    'CHEMBL4650213': 'EUB0000199b',
    'CHEMBL4784835': 'EUB0000484aNa',
    'CHEMBL177880': 'EUB0000484b',
    'CHEMBL372199': 'EUB0000750a',
    'CHEMBL1091513': 'EUB0000750',
    'CHEMBL4803066': 'EUB0000525b',
    'CHEMBL4760718': 'EUB0000525aCl',
    'CHEMBL4588626': 'EUB0001072',
    'CHEMBL5083872': 'EUB0001072a',
    'CHEMBL4066040': 'EUB0000264',
    'CHEMBL4093612': 'EUB0000544',
    'CHEMBL4752561': 'EUB0000739a',
    'CHEMBL4534005': 'EUB0000739',
    'CHEMBL4777443': 'EUB0000741a',
    'CHEMBL4558518': 'EUB0000741',
    'CHEMBL1800685': 'EUB0000274',
    'CHEMBL4743495': 'EUB0000274bCl',
    'CHEMBL4538849': 'EUB0000752',
    'CHEMBL4798829': 'EUB0000752a',
    'CHEMBL3774655': 'EUB0000195c',
    'CHEMBL3906203': 'EUB0000195b',
    'CHEMBL4564126': 'EUB0000312',
    'CHEMBL4750687': 'EUB0000312b',
    'CHEMBL3797887': 'EUB0000341',
    'CHEMBL4762646': 'EUB0000341aCl',
    'CHEMBL4764659': 'EUB0000291b',
    'CHEMBL4630644': 'EUB0000291',
    'CHEMBL4561806': 'EUB0001135',
    'CHEMBL4637129': 'EUB0001754a',
    'CHEMBL4641539': 'EUB0001720a',
    'CHEMBL4516471': 'EUB0000319c',
    'CHEMBL4778870': 'EUB0001572a',
    'CHEMBL4522773': 'CHEMBL4522773',
    'CHEMBL5027251': 'EUB0001559a',
    'CHEMBL4802050': 'EUB0001574a',
    'CHEMBL5092747': 'CHEMBL5092747',
    'CHEMBL3342191': 'CHEMBL3342191',
    'CHEMBL4802043': 'EUB0001566aK',
    'CHEMBL4802039': 'EUB0001561a',
    'CHEMBL5082184': 'EUB0001549a',
    'CHEMBL3236549': 'CHEMBL3236549',
    'CHEMBL5092058': 'EUB0001556a',
    'CHEMBL5094181': 'CHEMBL5094181',
    'CHEMBL3989870': 'EUB0000064a',
    'CHEMBL4802051': 'EUB0001563a',
    'CHEMBL4445645': 'CHEMBL4445645',
    'CHEMBL5090218': 'CHEMBL5090218',
    'CHEMBL5075693': 'EUB0001718a',
    'CHEMBL4588900': 'EUB0001182a',
    'CHEMBL4802047': 'CHEMBL4802047',
    'CHEMBL4802053': 'EUB0001575a',
    'CHEMBL5084426': 'EUB0001548a',
    'CHEMBL5081774': 'CHEMBL5081774',
    'CHEMBL5086970': 'EUB0001553a',
    'CHEMBL4522930': 'EUB0001492a',
    'CHEMBL5079207': 'EUB0001555a',
    'CHEMBL4802040': 'CHEMBL4802040',
    'CHEMBL5092999': 'EUB0001700a',
    'CHEMBL4636332': 'EUB0001578a',
    'CHEMBL2180408': 'EUB0001570a',
    'CHEMBL2403318': 'EUB0001577a',
    'CHEMBL5016886': 'EUB0001560a',
    'CHEMBL4097778': 'EUB0000720a',
    'CHEMBL4572210': 'CHEMBL4572210',
    'CHEMBL3623630': 'EUB0001552a',
    'CHEMBL4475494': 'EUB0001558a',
    'CHEMBL4748908': 'EUB0001573a',
    'CHEMBL4802048': 'EUB0001565a',
    'CHEMBL5077022': 'EUB0001557a',
    'CHEMBL538943': 'EUB0000063a',
    'CHEMBL2048623': 'EUB0001554a',
    'CHEMBL4589954': 'CHEMBL4589954',
    'CHEMBL5027912': 'EUB0001571a',
    'CHEMBL4802049': 'EUB0001486a',
    'CHEMBL4740744': 'EUB0000878a',
    'CHEMBL4802037': 'EUB0001576a',
    'CHEMBL5087167': 'CHEMBL5087167',
    'CHEMBL4802046': 'EUB0001567a',
    'CHEMBL4802044': 'EUB0001562a',
    'CHEMBL4802041': 'EUB0001569a',
    'CHEMBL4802042': 'EUB0001568a',
    'CHEMBL4802045': 'EUB0001564a',
    'CHEMBL3236584': 'CHEMBL3236584',
    'CHEMBL4647642': 'CHEMBL4647642',
}

# Activity types that belong to multiplex data
multiplex_activity_types = [
    'Control DMSO Apoptotic Cells (%)',
    'Control DMSO Fragmented Nuclei %',
    'Control DMSO Growth Rate',
    'Control DMSO Healthy Nuclei (%)',
    'Control DMSO Membrane Permeable-Phenotype Cells (%)',
    'Control DMSO Mitochondria Different-Phenotype Cells (%)',
    'Control DMSO Mitotic Cells (%)',
    'Control DMSO Pyknosed Nuclei (%)',
    'Control DMSO Total Cells',
    'Control DMSO Tubulin Phenotype Different Cells (%)',
    'Population Apoptotic (%)',
    'Population Fragmented Nuclei (%)',
    'Population Healthy Nuclei (%)',
    'Population Hoechst High Intensity Objects (%)',
    'Population Membrane Permeable-Phenotype (%)',
    'Population Mitochondria Different-Phenotype (%)',
    'Population Mitotic Cells (%)',
    'Population Normal (%)',
    'Population Pyknosed Nuclei (%)',
    'Population Tubulin-Different-Phenotype (%)',
    'Total Cell Count',
]

# This set gets filled after generating the assay index
multiplex_molecule_ids = set()


logger = logging.getLogger('chembl_ws2es')

space_replace_pattern = re.compile(r'\s+')


def generate_eubopen_index(src_res: ResourceDescription, dest_res: ResourceDescription,
                           querystring, transform_func=None, additional_mappings=None):
    if additional_mappings is None:
        additional_mappings = {}
    src_index = src_res.idx_name
    dest_index = dest_res.idx_name

    es_util.delete_idx(dest_index)
    es_util.create_idx(
        dest_index, 3, 1, analysis=DefaultMappings.COMMON_ANALYSIS,
        mappings=util.SummableDict(properties=src_res.get_resource_mapping_from_es())+additional_mappings
    )

    complete_data_pb = None

    def handle_doc(doc: dict, doc_id: str, total_docs: int, index: int, first: bool, last: bool):
        nonlocal complete_data_pb, transform_func, dest_index, dest_res
        if first:
            complete_data_pb = progress_bar_handler.get_new_progressbar(
                dest_res.idx_alias, total_docs
            )
        update_doc = doc
        if transform_func:
            transform_func(doc)
        if update_doc is not None:
            es_util.update_doc_bulk(dest_index, dest_res.get_doc_id(doc), doc=update_doc, upsert=True)
        es_util.bulk_submitter.set_complete_futures(True)

        if last:
            es_util.bulk_submitter.finish_current_queues()
            es_util.bulk_submitter.set_complete_futures(False)
            complete_data_pb.finish()
        else:
            complete_data_pb.update(index)

    query = {}
    util.put_js_path_in_dict(query, 'query.query_string.query', querystring)
    es_util.scan_index(src_index, handle_doc, query=query)


def generate_activity():
    global multiplex_molecule_ids, multiplex_activity_types, eubopen_assay_chembl_id_2_eubopen_assay_type
    multiplex_molecule_ids = set()

    def add_eubopen_data(doc):
        if doc.get('type', None) in multiplex_activity_types:
            multiplex_molecule_ids.add(doc['molecule_chembl_id'])
        # Assay types are now assigned after TSV correction
        # doc['_metadata']['eubopen'] = {
        #     'assay_type': eubopen_assay_chembl_id_2_eubopen_assay_type[doc['assay_chembl_id']]
        # }

    generate_eubopen_index(
        ACTIVITY, EUBOPEN_ACTIVITY, f'src_id:{eubopen_src_ids_es_query_string}',
        transform_func=add_eubopen_data,
        additional_mappings=assay_additional_mappings
    )


def generate_assay():
    def add_eubopen_data(doc):
        doc['assay_eubopen_id'] = doc['assay_chembl_id']
        add_assay_dn_properties(doc)

    generate_eubopen_index(
        ASSAY, EUBOPEN_ASSAY, f'src_id:{eubopen_src_ids_es_query_string}',
        transform_func=add_eubopen_data,
        additional_mappings=assay_additional_mappings
    )


def generate_target():
    def add_eubopen_id(doc):
        # Old generation of target ID based on target classification
        # doc['target_eubopen_id'] = generate_target_id(doc)
        doc['target_eubopen_id'] = doc['target_chembl_id']
        add_target_dn_properties(doc)

    generate_eubopen_index(
        TARGET, EUBOPEN_TARGET, f'_metadata.source.src_id:{eubopen_src_ids_es_query_string}',
        transform_func=add_eubopen_id,
        additional_mappings=target_additional_mappings
    )


def generate_molecule():
    global multiplex_molecule_ids, molecule_ebuopen_ids_fix
    eub_id_2_chembl_id = {}

    def add_eubopen_data(doc):
        add_molecule_dn_properties(doc)
        chembl_id = doc['molecule_chembl_id']
        eub_id = doc['molecule_eubopen_id']

        # fix molecule ids for duplicate documents with the same eubopen id
        if chembl_id in molecule_ebuopen_ids_fix:
            eub_id = molecule_ebuopen_ids_fix[chembl_id]
            doc['molecule_eubopen_id'] = eub_id
            logger.warning(f'FIXING ID : {chembl_id} - {eub_id}')

        if eub_id not in eub_id_2_chembl_id:
            eub_id_2_chembl_id[eub_id] = [chembl_id]
        else:
            eub_id_2_chembl_id[eub_id].append(chembl_id)
        util.put_js_path_in_dict(doc, '_metadata.multiplex', doc['molecule_chembl_id'] in multiplex_molecule_ids)

    generate_eubopen_index(
        MOLECULE, EUBOPEN_MOLECULE, f'_metadata.compound_records.src_id:{eubopen_src_ids_es_query_string}',
        transform_func=add_eubopen_data,
        additional_mappings={
            'properties': {
                'molecule_eubopen_id': DefaultMappings.ID,
                '_metadata': {
                    'properties': {
                        'multiplex': DefaultMappings.BOOLEAN
                    }
                }
            }
        }
    )

    # EUBOPEN_ID 2 CHEMBL_ID VALIDATION
    for eub_id_i, chembl_ids_i in eub_id_2_chembl_id.items():
        if len(chembl_ids_i) > 1:
            logger.warning(f'EUBOPEN ID {eub_id_i} REFERS TO MULTIPLE CHEMBL IDS: {chembl_ids_i}')

# ----------------------------------------------------------------------------------------------------------------------
# ASSAY EUBOPEN CHANGES
# ----------------------------------------------------------------------------------------------------------------------


assay_aidx_types = {
    'AB': {
        'name': 'Affinity biochemical Assay',
    },
    'AOT': {
        'name': 'Affinity on-target cellular assay',
    },
     'AP': {
        'name': 'Affinity phenotypic cellular assay',
    },
     'S': {
        'name': 'Selectivity assay',
    },
    'ASIC50': {
        'name': 'Alphascreen assay',
    },
     'ASSS': {
        'name': 'Alphascreen assay',
    },
    'GPCR': {
        'name': 'GPCR beta-arrestin recruitment assay',
    },
    'NanoBRET': {
        'name': 'NanoBRET assay or Cellular target engagement assay',
    },
    'INCU': {
        'name': 'Incucyte cell viability',
    },
    'MULTI': {
        'name': 'Cell Health Data',
    },
    'ITC': {
        'name': 'ITC assay',
    },
    'HTRF': {
        'name': 'HTRF assay',
    },
    'TSA': {
        'name': 'Thermal shift assay',
    }
}

eubopen_assay_chembl_id_2_eubopen_assay_type = {}
assay_additional_mappings = {
        'properties':
        {
            '_metadata':
            {
                'properties':
                {
                    'eubopen':
                    {
                        'properties':
                        {
                            'assay_type':
                            {
                                'properties': {
                                    'aidx': DefaultMappings.KEYWORD,
                                    'aidx_parts': DefaultMappings.LOWER_CASE_KEYWORD,
                                    'type': {
                                        'properties': {
                                            'acronym': DefaultMappings.KEYWORD,
                                            'description': DefaultMappings.LOWER_CASE_KEYWORD,
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


def add_assay_dn_properties(assay_doc):
    chembl_id = ASSAY.get_doc_id(assay_doc)
    aidx = assay_doc.get('aidx', None)
    eubopen_assay_type = {}
    if aidx:
        aidx_parts = aidx.split('_')
        eubopen_assay_type['aidx'] = aidx
        eubopen_assay_type['aidx_parts'] = aidx_parts
        eubopen_assay_type['type'] = [{
                                    'acronym': aidx_parts[0],
                                    'description': assay_aidx_types.get(aidx_parts[0], {}).get('name', None)
                                }]
    util.put_js_path_in_dict(assay_doc['_metadata'], 'eubopen.assay_type', eubopen_assay_type)
    eubopen_assay_chembl_id_2_eubopen_assay_type[chembl_id] = eubopen_assay_type

# ----------------------------------------------------------------------------------------------------------------------
# MOLECULE EUBOPEN CHANGES
# ----------------------------------------------------------------------------------------------------------------------


def get_eubopenid_and_pref_name(compound_records, chembl_id):
    global eubopen_src_ids
    known_tuples = set()
    for cr_i in compound_records:
        if cr_i['src_id'] in eubopen_src_ids:
            eubopen_id = cr_i.get('compound_key', None)
            if eubopen_id:
                eubopen_id = eubopen_id[:10]
            pref_name = cr_i.get('compound_name', None)
            known_tuples.add((eubopen_id, pref_name))

    eub_id_set = set()
    pref_name_set = set()

    for eub_id_i, pref_name_i in known_tuples:
        if eub_id_i is not None:
            eub_id_set.add(eub_id_i)
        if pref_name_i is not None:
            pref_name_set.add(pref_name_i)

    if len(known_tuples) == 0:
        logger.warning(f'COMPOUND {chembl_id} IS MISSING EUBOPEN COMPOUND RECORDS.')
        return None, None
    if len(eub_id_set) > 1:
        logger.warning(
            f'COMPOUND {chembl_id} HAS MULTIPLE EUBOPEN IDS. FOUND: {list(eub_id_set)}'
        )
    elif len(eub_id_set) == 0:
        eub_id_set.add(chembl_id)
        logger.warning(
            f'COMPOUND {chembl_id} DOES NOT HAVE EUBOPEN ID USING CHEMBL ID INSTEAD'
        )

    if len(pref_name_set) > 1:
        logger.warning(
            f'COMPOUND {chembl_id} HAS MULTIPLE PREF NAMES. FOUND: {list(pref_name_set)}'
        )

    # Return the first tuple with both non-empty values
    for eub_id_i, pref_name_i in known_tuples:
        if eub_id_i and pref_name_i:
            return eub_id_i, pref_name_i

    # if not return a random preferably not empty value for them
    eub_id_ret = list(eub_id_set)[0] if len(eub_id_set) > 0 else None
    pref_name_ret = list(pref_name_set)[0] if len(pref_name_set) > 0 else None

    return eub_id_ret, pref_name_ret


def add_molecule_dn_properties(molecule_doc):
    chembl_id = molecule_doc['molecule_chembl_id']
    eubopen_id, pref_name_eubopen = get_eubopenid_and_pref_name(
        molecule_doc['_metadata']['compound_records'],
        chembl_id
    )
    molecule_doc['molecule_eubopen_id'] = eubopen_id
    molecule_doc['pref_name'] = pref_name_eubopen
    pref_name = molecule_doc.get('pref_name', None)
    es_completion = [
        {
            "weight": 10,
            "input": eubopen_id
        },
        {
            "weight": 1,
            "input": chembl_id
        }
    ]
    if pref_name_eubopen:
        es_completion.append(
            {
                "weight": 100,
                "input": pref_name_eubopen
            }
        )
    if pref_name is not None:
        es_completion.append({'weight': 100, 'input': pref_name})
    for mol_syn_i in molecule_doc.get('molecule_synonyms', []):
        weight = 75
        if mol_syn_i.get('syn_type') in ['RESEARCH_CODE', 'E_NUMBER']:
            weight = 5
        es_completion.append({'weight': weight, 'input': mol_syn_i.get('molecule_synonym')})
    molecule_doc['_metadata']['es_completion'] = es_completion


# ----------------------------------------------------------------------------------------------------------------------
# TARGET EUBOPEN CHANGES
# ----------------------------------------------------------------------------------------------------------------------


eubopen_target_id_2_chembl_id = {}
target_additional_mappings = {
        'properties':
        {
            'target_eubopen_id': DefaultMappings.ID,
            '_metadata':
            {
                'properties':
                {
                    'accession': DefaultMappings.ALT_NAME,
                    'gene_symbol': DefaultMappings.ALT_NAME,
                    'uniprot_synonyms': DefaultMappings.ALT_NAME,
                    'other_synonyms': DefaultMappings.ALT_NAME
                }
            }
        }
    }


def get_component_eubopen_id(component, chembl_id):
    comp_id = component.get('component_id')
    accession = component.get('accession', None)

    if accession is None:
        logger.warning(
            'Component in Target {0} does not have accession using component_id.'.format(chembl_id)
        )
        comp_eubopen_id = 'CHEMBL-COMP-ID:{0}'.format(comp_id)
    else:
        comp_eubopen_id = '{0}'.format(accession)

    return comp_eubopen_id


def generate_target_id(target_doc):
    chembl_id = target_doc['target_chembl_id']
    t_type = target_doc['target_type']
    components = target_doc.get('target_components', [])
    t_eu_id = t_type
    if t_type == 'UNCHECKED':
        pass
    elif t_type == 'CELL-LINE':
        name = target_doc.get('pref_name', None)
        if name is None:
            logger.warning('Target {0} is a CELL-LINE with no name.'.format(chembl_id))
        t_eu_id += ':{0}'.format(name)
    elif t_type == 'SINGLE PROTEIN':
        if len(components) != 1:
            logger.warning('Target {0} is a SINGLE PROTEIN with multiple components.'.format(chembl_id))
        t_eu_id += ':{0}'.format(get_component_eubopen_id(components[0], chembl_id))
    else:
        id_parts = set()
        if len(components) == 0:
            logger.warning('Target {0} is a {1} with multiple components.'.format(chembl_id, t_type))
        for component_i in components:
            comp_id = get_component_eubopen_id(component_i, chembl_id)
            if comp_id in id_parts:
                logger.warning('Target {0} has a duplicate component id for {1}.'.format(chembl_id, comp_id))
            id_parts.add(comp_id)
        for id_part_i in sorted(id_parts):
            t_eu_id += ':{0}'.format(id_part_i)

    t_eu_id = space_replace_pattern.sub('-', t_eu_id)

    if t_eu_id not in eubopen_target_id_2_chembl_id.keys():
        eubopen_target_id_2_chembl_id[t_eu_id] = []
    eubopen_target_id_2_chembl_id[t_eu_id].append(chembl_id)

    if len(eubopen_target_id_2_chembl_id[t_eu_id]) > 1:
        logger.warning('Target with EUBOPEN id "{0}" has multiple chembl ids {1}.'
                       .format(t_eu_id, eubopen_target_id_2_chembl_id[t_eu_id]))

    return t_eu_id


def add_target_dn_properties(target_doc):
    es_id = EUBOPEN_TARGET.get_doc_id(target_doc)
    pref_name = target_doc.get('pref_name', None)
    components = target_doc.get('target_components', [])
    accessions = []
    gene_symbol = []
    uniprot_synonyms = []
    other_synonyms = []
    es_completion = [
        {
            "weight": 10,
            "input": es_id
        }
    ]
    if pref_name is not None:
        es_completion.append({'weight': 100, 'input': pref_name})
    score_divider = len(components)
    for comp_i in components:
        accession_i = comp_i.get('accession', None)
        if accession_i is not None:
            accessions.append(accession_i)
            es_completion.append({'weight': int(50/score_divider), 'input': accession_i})

        for syn_i in comp_i.get('target_component_synonyms', []):
            syn_type_i = syn_i['syn_type']
            comp_syn_i = syn_i['component_synonym']
            if comp_syn_i is not None and len(comp_syn_i.strip()) > 0:
                if syn_type_i == 'GENE_SYMBOL':
                    gene_symbol.append(comp_syn_i)
                    es_completion.append({'weight': int(50/score_divider), 'input': comp_syn_i})
                elif syn_type_i == 'UNIPROT':
                    uniprot_synonyms.append(comp_syn_i)
                    es_completion.append({'weight': int(75/score_divider), 'input': comp_syn_i})
                else:
                    other_synonyms.append(comp_syn_i)
                    es_completion.append({'weight': int(20/score_divider), 'input': comp_syn_i})
    target_doc['_metadata']['es_completion'] = es_completion
    target_doc['_metadata']['accession'] = accessions
    target_doc['_metadata']['gene_symbol'] = gene_symbol
    target_doc['_metadata']['uniprot_synonyms'] = uniprot_synonyms
    target_doc['_metadata']['other_synonyms'] = other_synonyms

    # If there are gene symbols use them as pref names
    if len(gene_symbol) > 0:
        target_doc['pref_name'] = '@'.join(gene_symbol)

