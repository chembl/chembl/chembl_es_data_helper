import argparse
import time
from datetime import datetime, timedelta
from glados.es.ws2es.es_util import es_util
import glados.es.ws2es.signal_handler as signal_handler
from glados.es.ws2es.denormalization.activity_handler import ActivityDenormalizationHandler
from glados.es.ws2es.denormalization.atc_class_handler import ATCClassDenormalizationHandler
from glados.es.ws2es.denormalization.assay_handler import AssayDenormalizationHandler
from glados.es.ws2es.denormalization.binding_site_handler import BindingSiteHandler
from glados.es.ws2es.denormalization.compound_handler import CompoundDenormalizationHandler
from glados.es.ws2es.denormalization.compound_structural_alert_handler import \
    CompoundStructuralAlertDenormalizationHandler
from glados.es.ws2es.denormalization.cell_handler import CellDenormalizationHandler
from glados.es.ws2es.denormalization.compound_record_handler import CompoundRecordDenormalizationHandler
from glados.es.ws2es.denormalization.document_handler import DocumentDenormalizationHandler
from glados.es.ws2es.denormalization.document_similarity_handler import DocumentSimilarityHandler
from glados.es.ws2es.denormalization.drug_handler import DrugDenormalizationHandler
from glados.es.ws2es.denormalization.drug_indication_handler import DrugIndicationDenormalizationHandler
from glados.es.ws2es.denormalization.drug_warning_handler import DrugWarningDenormalizationHandler
from glados.es.ws2es.denormalization.metabolism_handler import MetabolismDenormalizationHandler
from glados.es.ws2es.denormalization.mechanism_handler import MechanismDenormalizationHandler
from glados.es.ws2es.denormalization.organism_handler import OrganismDenormalizationHandler
from glados.es.ws2es.denormalization.protein_class_handler import ProteinClassDenormalizationHandler
from glados.es.ws2es.denormalization.source_handler import SourceDenormalizationHandler
from glados.es.ws2es.denormalization.target_component_handler import TargetComponentDenormalizationHandler
from glados.es.ws2es.denormalization.target_handler import TargetDenormalizationHandler
from glados.es.ws2es.denormalization.tissue_handler import TissueDenormalizationHandler
import glados.es.ws2es.denormalization.eubopen as eubopen
import glados.es.ws2es.denormalization.eubopen.tsv_importer as tsv_importer
import glados.es.ws2es.progress_bar_handler
import glados.es.ws2es.migration_logging as migration_logging
import logging

__author__ = 'jfmosquera@ebi.ac.uk'


logger = logging.getLogger('chembl_ws2es')


# ----------------------------------------------------------------------------------------------------------------------
# Denormalization
# ----------------------------------------------------------------------------------------------------------------------

def denormalize_all_but_activity():

    protein_class_dh = ProteinClassDenormalizationHandler()
    protein_class_dh.scan_data_from_es()
    protein_class_dh.summarize_class_families()
    protein_class_dh.save_denormalization_for_new_index()

    target_component_dh = TargetComponentDenormalizationHandler(complete_x_refs=True,
                                                                protein_class_dn_handler=protein_class_dh)
    target_component_dh.scan_data_from_es()
    target_component_dh.save_denormalization()

    csa_dh = CompoundStructuralAlertDenormalizationHandler()
    csa_dh.scan_data_from_es()
    csa_dh.save_denormalization()

    source_dh = SourceDenormalizationHandler()
    source_dh.scan_data_from_es()
    source_dh.save_denormalization()

    organism_dh = OrganismDenormalizationHandler()
    organism_dh.scan_data_from_es()
    organism_dh.save_denormalization()

    atc_class_dh = ATCClassDenormalizationHandler()
    atc_class_dh.scan_data_from_es()
    atc_class_dh.save_denormalization()

    target_pre_dh = TargetDenormalizationHandler()
    target_pre_dh.scan_data_from_es()

    drug_indication_dh = DrugIndicationDenormalizationHandler()
    drug_indication_dh.scan_data_from_es()
    drug_indication_dh.save_denormalization()

    drug_dh = DrugDenormalizationHandler()
    drug_dh.scan_data_from_es()
    drug_dh.save_denormalization()

    cell_dh = CellDenormalizationHandler(organism_dh=organism_dh)
    cell_dh.scan_data_from_es()
    cell_dh.save_denormalization()

    compound_record_dh = CompoundRecordDenormalizationHandler(source_dh=source_dh)
    compound_record_dh.scan_data_from_es()
    compound_record_dh.save_denormalization()

    assay_document_dh = DocumentDenormalizationHandler()
    assay_document_dh.scan_data_from_es()

    assay_dh = AssayDenormalizationHandler(
        complete_from_assay=True, organism_dh=organism_dh, source_dh=source_dh, document_dh=assay_document_dh
    )
    assay_dh.scan_data_from_es()
    # Save the denormalization on the next step
    # assay_dh.save_denormalization()

    compound_dh = CompoundDenormalizationHandler(
        complete_x_refs=True, atc_dh=atc_class_dh
    )
    compound_dh.scan_data_from_es()
    compound_dh.save_denormalization()

    document_dh = DocumentDenormalizationHandler(assay_dh=assay_dh, source_dh=source_dh)
    document_dh.scan_data_from_es()
    document_dh.save_denormalization()

    doc_similarity_dh = DocumentSimilarityHandler(document_dh)
    doc_similarity_dh.scan_data_from_es()
    doc_similarity_dh.save_denormalization()

    metabolism_dh = MetabolismDenormalizationHandler(compound_dh=compound_dh)
    metabolism_dh.scan_data_from_es()
    metabolism_dh.save_denormalization()

    target_dh = TargetDenormalizationHandler(complete_x_refs=True, organism_dh=organism_dh)
    target_dh.scan_data_from_es()
    target_dh.save_denormalization()

    tissue_dh = TissueDenormalizationHandler(assay_dh=assay_dh, organism_dh=organism_dh)
    tissue_dh.scan_data_from_es()
    tissue_dh.save_denormalization()


def denormalize_unichem():
    c_dh = CompoundDenormalizationHandler()
    c_dh.scan_data_from_es()
    c_dh.complete_unichem_data()


def denormalize_activity():

    protein_class_dh = ProteinClassDenormalizationHandler()
    protein_class_dh.scan_data_from_es()
    protein_class_dh.summarize_class_families()

    source_dh = SourceDenormalizationHandler()
    source_dh.scan_data_from_es()

    # required to change the order to be able to load source description
    assay_dh = AssayDenormalizationHandler(source_dh=source_dh)
    assay_dh.scan_data_from_es()

    compound_dh = CompoundDenormalizationHandler()
    compound_dh.scan_data_from_es()

    organism_dh = OrganismDenormalizationHandler()
    organism_dh.scan_data_from_es()

    target_dh = TargetDenormalizationHandler()
    target_dh.scan_data_from_es()

    target_component_dh = TargetComponentDenormalizationHandler(protein_class_dn_handler=protein_class_dh)
    target_component_dh.scan_data_from_es()

    compound_record_dh = CompoundRecordDenormalizationHandler()
    compound_record_dh.scan_data_from_es()

    document_dh = DocumentDenormalizationHandler()
    document_dh.scan_data_from_es()

    activity_dh = ActivityDenormalizationHandler(
        complete_from_activity=True, assay_dh=assay_dh,
        compound_dh=compound_dh, organism_dh=organism_dh,
        source_dh=source_dh, target_dh=target_dh,
        target_component_dh=target_component_dh,
        compound_record_dh=compound_record_dh,
        document_dh=document_dh
    )
    activity_dh.scan_data_from_es()

    activity_dh.complete_compound()
    assay_dh.complete_cell_n_tissue(
        assay_2_compound=activity_dh.assay_2_compound, ac_dh_assay_dict=activity_dh.assay_dict
    )

    # Save denormalization after the completion methods have run
    activity_dh.save_denormalization()
    assay_dh.save_denormalization()


def denormalize_compound_hierarchy():
    compound_dh = CompoundDenormalizationHandler(analyze_hierarchy=True)
    compound_dh.scan_data_from_es(include_metadata=True)
    compound_dh.molecule_family_desc.print_tree()
    compound_dh.complete_hierarchy_data()


def denormalize_generated_resources():

    compound_dh = CompoundDenormalizationHandler(analyze_hierarchy=True)
    compound_dh.scan_data_from_es(include_metadata=True)
    compound_dh.molecule_family_desc.print_tree()

    drug_warn_dh = DrugWarningDenormalizationHandler(compound_families_dir=compound_dh.molecule_family_desc)
    drug_warn_dh.scan_data_from_es(include_metadata=True)
    drug_warn_dh.save_denormalization()

    mechanism_dh = MechanismDenormalizationHandler(compound_families_dir=compound_dh.molecule_family_desc)
    mechanism_dh.scan_data_from_es(include_metadata=True)
    mechanism_dh.save_denormalization()

    drug_ind_dh = DrugIndicationDenormalizationHandler(compound_families_dir=compound_dh.molecule_family_desc)
    drug_ind_dh.scan_data_from_es(include_metadata=True)
    drug_ind_dh.save_denormalization()


def denormalize_eubopen():
    # required to complete source data in target
    source_dh = SourceDenormalizationHandler()
    source_dh.scan_data_from_es()

    assay_dh = AssayDenormalizationHandler(source_dh=source_dh)
    assay_dh.scan_data_from_es()

    target_dh = TargetDenormalizationHandler(assay_dh=assay_dh)
    target_dh.scan_data_from_es()
    """
       1-generate assay because is required by activity
       2-generate activity because is required by molecule
    """
    logger.info('GENERATING EUBOPEN ASSAY')
    eubopen.generate_assay()
    logger.info('GENERATING EUBOPEN ACTIVITY')
    eubopen.generate_activity()
    logger.info('GENERATING EUBOPEN MOLECULE')
    eubopen.generate_molecule()
    logger.info('GENERATING EUBOPEN TARGET')
    eubopen.generate_target()
    logger.info('GENERATING EUBOPEN TSV DATA')
    tsv_importer.load_data_in_es()


def fix_max_phase_label():
    compound_dh = CompoundDenormalizationHandler(complete_x_refs=True)
    compound_dh.scan_data_from_es()

    activity_dh = ActivityDenormalizationHandler(
        compound_dh=compound_dh
    )
    activity_dh.scan_data_from_es()


def fix_drug():
    drug_dh = DrugDenormalizationHandler()
    drug_dh.scan_data_from_es()
    drug_dh.save_denormalization()

# ----------------------------------------------------------------------------------------------------------------------
# MAIN
# ----------------------------------------------------------------------------------------------------------------------


def main():
    t_ini = time.time()
    parser = argparse.ArgumentParser(description="Denormalize ChEMBL data existing in Elastic Search")
    parser.add_argument("--host",
                        dest="es_host",
                        help="Elastic Search Hostname or IP address.",
                        default="localhost")
    parser.add_argument("--user",
                        dest="es_user",
                        help="Elastic Search username.",
                        default=None)
    parser.add_argument("--password",
                        dest="es_password",
                        help="Elastic Search username password.",
                        default=None)
    parser.add_argument("--port",
                        dest="es_port",
                        help="Elastic Search port.",
                        default=9200)
    parser.add_argument("--unichem",
                        dest="denormalize_unichem",
                        help="If included will denormalize the unichem related data.",
                        action="store_true",)
    parser.add_argument("--activity",
                        dest="denormalize_activity",
                        help="If included will denormalize the configured activity related data.",
                        action="store_true",)
    parser.add_argument("--compound_hierarchy",
                        dest="denormalize_compound_hierarchy",
                        help="If included will denormalize the Compound Hierarchy data.",
                        action="store_true",)
    parser.add_argument("--generated_resources",
                        dest="denormalize_generated_resources",
                        help="If included will denormalize the Mechanism and Drug Indication and Warnings data.",
                        action="store_true",)
    parser.add_argument("--eubopen",
                        dest="eubopen",
                        help="Generate eubopen data.",
                        action="store_true",)
    parser.add_argument("--max_phase_fix",
                        dest="max_phase",
                        help="Fix max phase labels.",
                        action="store_true",)
    parser.add_argument("--drug_fix",
                        dest="drug_fix",
                        help="Fix drugs.",
                        action="store_true",)
    args = parser.parse_args()

    migration_logging.setup_migration_logging(output_dir_path='./')
    glados.es.ws2es.progress_bar_handler.set_progressbar_out_path('./')

    migration_logging.setup_migration_logging()

    es_util.setup_connection(args.es_host, args.es_port, args.es_user, args.es_password)
    es_util.bulk_submitter.max_docs_per_request = 750
    es_util.bulk_submitter.start()

    signal_handler.add_termination_handler(es_util.stop_scan)

    dn_type = None
    if args.denormalize_compound_hierarchy:
        denormalize_compound_hierarchy()
        dn_type = 'COMPOUND-HIERARCHY'
    elif args.denormalize_activity:
        denormalize_activity()
        dn_type = 'ACTIVITY'
    elif args.denormalize_unichem:
        denormalize_unichem()
        dn_type = 'UNICHEM'
    elif args.denormalize_generated_resources:
        denormalize_generated_resources()
        dn_type = 'GENERATED-RESOURCES'
    elif args.eubopen:
        denormalize_eubopen()
        dn_type = 'EUBOPEN'
    elif args.max_phase:
        fix_max_phase_label()
        dn_type = 'MAX_PHASE FIX'
    elif args.drug_fix:
        fix_drug()
        dn_type = 'DRUG FIX'
    else:
        denormalize_all_but_activity()
        dn_type = 'ALL-NO-ACTIVITY'
    end_msg = 'DENORMALIZATION FOR "{}" FINISHED'.format(dn_type)

    logger.info('ALL SUBMITTED - JOINING BULK SUBMITTER')
    es_util.bulk_submitter.join()
    logger.info('BULK SUBMITTER JOINED')
    glados.es.ws2es.progress_bar_handler.write_after_progress_bars()

    total_time = time.time() - t_ini
    sec = timedelta(seconds=total_time)
    d = datetime(1, 1, 1) + sec

    logger.info(end_msg)
    logger.info(
        "Finished in: {0} Day(s), {1} Hour(s), {2} Minute(s) and {3} Second(s)"
        .format(d.day-1, d.hour, d.minute, d.second)
    )


if __name__ == "__main__":
    main()
