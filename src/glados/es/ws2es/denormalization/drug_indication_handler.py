import glados.es.ws2es.progress_bar_handler as progress_bar_handler
from glados.es.ws2es.util import SummableDict
from glados.es.ws2es.denormalization import DenormalizationHandler, get_max_phase_label, get_phase_standard_float_value
import glados.es.ws2es.mappings.es_chembl_drug_indication_mapping as drug_indication_mapping
from glados.es.ws2es.denormalization.compound_family_helper import CompoundFamiliesDir, max_phase_from_string_float
from glados.es.ws2es.es_util import DefaultMappings, es_util
from glados.es.ws2es.util import put_js_path_in_dict
import traceback
import pprint

from glados.es.ws2es.resources_description import MOLECULE, DRUG_INDICATION, DRUG_INDICATION_BY_PARENT
import logging

__author__ = 'jfmosquera@ebi.ac.uk'


logger = logging.getLogger('chembl_ws2es')


class DrugIndicationDenormalizationHandler(DenormalizationHandler):

    RESOURCE = DenormalizationHandler.AVAILABLE_RESOURCES.DRUG_INDICATION

    @staticmethod
    def get_new_index_mappings():
        return {
            'properties':
            {
                'parent_molecule':
                {
                    'properties': MOLECULE.get_resource_mapping_from_es()
                },
                'drug_indication': {
                    'properties': SummableDict(**DRUG_INDICATION.get_resource_mapping_from_es()) -
                    ['efo_term', 'efo_id'] +
                    {
                        'efo': {
                            'properties': {
                                'term': DefaultMappings.LOWER_CASE_KEYWORD + DefaultMappings.TEXT_STD,
                                'id': DefaultMappings.ID
                            }
                        },
                        'max_phase_for_ind_label': DefaultMappings.KEYWORD
                    } + {
                        'indication_refs_by_molecule': DefaultMappings.NO_INDEX_OBJECT
                    }
                }
            }
        }

    def __init__(self, compound_families_dir: CompoundFamiliesDir=None):
        super().__init__(compound_families_dir is not None)
        self.compound_families_dir = compound_families_dir
        self.compound_dict = {}
        self.drug_inds_by_grouping_id = {}
        self.generated_resource = DRUG_INDICATION_BY_PARENT

    def get_drug_ind_grouping_id_parts(self, doc):
        if self.compound_families_dir is None:
            raise Exception('The grouping will not be correct if the compound families directory is not provided!')
        family_data = self.compound_families_dir.find_node(doc['molecule_chembl_id'])
        parent_chembl_id = family_data.get_family_parent_id()
        return parent_chembl_id, doc.get('mesh_id')

    def get_drug_ind_grouping_id(self, doc):
        parent_chembl_id, mesh_id = self.get_drug_ind_grouping_id_parts(doc)
        return '{0}-{1}'.format(parent_chembl_id, mesh_id)

    def handle_doc(self, es_doc: dict, total_docs: int, index: int, first: bool, last: bool):
        molecule_c_id = es_doc.get('molecule_chembl_id', None)
        if molecule_c_id:
            if molecule_c_id not in self.compound_dict:
                self.compound_dict[molecule_c_id] = []
            self.compound_dict[molecule_c_id].append(es_doc)
            if self.compound_families_dir:
                grouping_id = self.get_drug_ind_grouping_id(es_doc)
                if grouping_id not in self.drug_inds_by_grouping_id:
                    self.drug_inds_by_grouping_id[grouping_id] = []
                self.drug_inds_by_grouping_id[grouping_id].append(es_doc)
        else:
            logger.warning('Drug-indication without compound :{0}'.format(molecule_c_id))


    def save_denormalization(self):
        def get_update_script_and_size(es_doc_id, es_doc):
            update_size = len(es_doc)

            update_doc = {
                '_metadata': {
                    'drug_indications': es_doc
                }
            }

            return update_doc, update_size

        self.save_denormalization_dict(
            DenormalizationHandler.AVAILABLE_RESOURCES.MOLECULE,
            self.compound_dict,
            get_update_script_and_size,
            new_mappings={
                'properties': {
                    '_metadata': {
                        'properties': {
                            'drug_indications': drug_indication_mapping.mappings
                        }
                    }
                }
            }
        )

        if self.compound_families_dir:
            self.save_denormalization_for_new_index()

    def get_custom_mappings_for_complete_data(self):
        mappings = SummableDict()
        mappings += {
            'properties':
            {
                '_metadata':
                {
                    'properties':
                    {
                        'all_molecule_chembl_ids': DefaultMappings.CHEMBL_ID_REF
                    }
                }
            }
        }
        return mappings

    def get_doc_for_complete_data(self, doc: dict):
        molecule_chembl_id = doc['molecule_chembl_id']
        family_data = self.compound_families_dir.find_node(molecule_chembl_id)
        if family_data:
            all_branch_ids = family_data.get_all_branch_ids()
        else:
            all_branch_ids = []
            logger.warning(
                'DRUG INDICATION WITH molecule_chembl_id="{0}" NOT FOUND IN HIERARCHY'.format(molecule_chembl_id)
            )
        return {
            '_metadata': {
                'all_molecule_chembl_ids': all_branch_ids
            }
        }

    def save_molecule_dn(self, molecule_chembl_ids):
        dn_dict = {}

        logger.info(f'UPDATING {len(molecule_chembl_ids)} MOLECULES WITH DRUG INDICATIONS')
        for mol_chembl_id in molecule_chembl_ids:

            update_doc = put_js_path_in_dict({}, '_metadata.generated_resources.has_drug_indications', True)
            dn_dict[mol_chembl_id] = update_doc

        self.save_denormalization_dict(
            MOLECULE, dn_dict, DenormalizationHandler.default_update_script_and_size
        )

    def save_denormalization_for_new_index(self):
        es_util.delete_idx(self.generated_resource.idx_name)
        es_util.create_idx(
            self.generated_resource.idx_name, 3, 1, analysis=DefaultMappings.COMMON_ANALYSIS,
            mappings=DrugIndicationDenormalizationHandler.get_new_index_mappings()
        )

        molecule_chembl_ids_with_indications = set()
        dn_dict = {}

        logger.info('{0} GROUPED RECORDS WERE FOUND'.format(len(self.drug_inds_by_grouping_id)))
        p_bar = progress_bar_handler.get_new_progressbar('drug_inds_by_parent-dn-generation',
                                                         len(self.drug_inds_by_grouping_id))
        i = 0
        for group_drug_inds in self.drug_inds_by_grouping_id.values():
            base_drug_ind = group_drug_inds[0]
            efo_data = {}
            indication_refs = []
            max_phase_for_ind = None
            all_molecule_chembl_ids = set()
            indication_refs_by_molecule = {}
            for drug_ind_i in group_drug_inds:

                max_phase_for_ind = max_phase_from_string_float(
                    max_phase_for_ind, drug_ind_i.get('max_phase_for_ind', None)
                )

                efo_id_i = drug_ind_i.get('efo_id', None)
                if efo_id_i is not None:
                    efo_data[efo_id_i] = drug_ind_i.get('efo_term', None)

                indication_refs += drug_ind_i.get('indication_refs', [])
                if drug_ind_i['molecule_chembl_id'] not in indication_refs_by_molecule:
                    indication_refs_by_molecule[drug_ind_i['molecule_chembl_id']] = []
                indication_refs_by_molecule[drug_ind_i['molecule_chembl_id']] += drug_ind_i.get('indication_refs', [])
                try:
                    for c_id in drug_ind_i.get('_metadata').get('all_molecule_chembl_ids'):
                        all_molecule_chembl_ids.add(c_id)
                except Exception as e:
                    logger.error(traceback.format_exception(e))
                    logger.error("DRUG INDS GROUP")
                    logger.error(pprint.pformat(group_drug_inds))
                    logger.error("DRUG IND I")
                    logger.error(pprint.pformat(drug_ind_i))

            parent_chembl_id, mesh_id = self.get_drug_ind_grouping_id_parts(base_drug_ind)

            drug_ind_data = SummableDict(**DRUG_INDICATION.get_doc_by_id_from_es(base_drug_ind['drugind_id']))
            drug_ind_data -= ['efo_term', 'efo_id']
            drug_ind_data['efo'] = [{'id': efo_id, 'term': term} for efo_id, term in efo_data.items()]
            drug_ind_data['max_phase_for_ind'] = get_phase_standard_float_value(max_phase_for_ind)
            drug_ind_data['max_phase_for_ind_label'] = get_max_phase_label(max_phase_for_ind)
            drug_ind_data['indication_refs'] = indication_refs
            drug_ind_data['indication_refs_by_molecule'] = indication_refs_by_molecule
            drug_ind_data['_metadata']['all_molecule_chembl_ids'] = list(all_molecule_chembl_ids)

            # Collect molecule chembl_ids
            for mol_chembl_id_i in all_molecule_chembl_ids:
                molecule_chembl_ids_with_indications.add(mol_chembl_id_i)

            new_mechanism_doc = {
                'parent_molecule': MOLECULE.get_doc_by_id_from_es(parent_chembl_id),
                'drug_indication': drug_ind_data
            }
            doc_id = self.generated_resource.get_doc_id(new_mechanism_doc)

            dn_dict[doc_id] = new_mechanism_doc
            i += 1
            p_bar.update(i)
        p_bar.finish()

        self.save_denormalization_dict(
            self.generated_resource, dn_dict, DenormalizationHandler.default_update_script_and_size, do_index=True
        )
        self.save_molecule_dn(molecule_chembl_ids_with_indications)
