import time
import argparse
from glados.es.ws2es.es_util import es_util
from glados.es.ws2es.util import SummableDict
from glados.es.ws2es.resources_description import ASSAY, ACTIVITY
import json
import copy


def get_assay_activities(assay_chembl_id):
    assay_activities = []

    def on_activity_doc(act_doc, activity_id, total_docs, count, first, last):
        assay_activities.append(act_doc)

    es_util.scan_index(
        ACTIVITY.idx_alias, on_doc=on_activity_doc,
        query={
            'query': {
                'query_string': {
                    'query': 'assay_chembl_id:{0}'.format(assay_chembl_id)
                }
            }
        },
        progressbar=False
    )
    return assay_activities


def generate_variants_json_file():
    with open('./OTAR48-variant-assay-data.json', 'w') as variant_data_file:
        with open('./OTAR48-variant-assay-data-props-desc.json', 'w') as props_desc_file:
            example_all_props_otar = SummableDict()

            def generate_assay_json(assay_doc, assay_id, total_docs, count, first, last):
                nonlocal example_all_props_otar
                variant_data = copy.deepcopy(assay_doc)
                variant_data['activities'] = get_assay_activities(assay_id)

                variant_data_file.write(json.dumps(variant_data)+'\n')

                example_all_props_otar += variant_data
            es_util.scan_index(
                ASSAY.idx_alias, on_doc=generate_assay_json,
                query={
                    'query': {
                        'query_string': {
                            'query': '_exists_:variant_sequence'
                        }
                    }
                }
            )
            props_desc_file.write(json.dumps(example_all_props_otar, indent=4))


def main():
    t_ini = time.time()
    parser = argparse.ArgumentParser(description="Migrate ChEMBL data from the WebServices to Elastic Search")
    parser.add_argument("--host",
                        dest="es_host",
                        help="Elastic Search Hostname or IP address.",
                        default="localhost")
    parser.add_argument("--user",
                        dest="es_user",
                        help="Elastic Search username.",
                        default=None)
    parser.add_argument("--password",
                        dest="es_password",
                        help="Elastic Search username password.",
                        default=None)
    parser.add_argument("--port",
                        dest="es_port",
                        help="Elastic Search port.",
                        default=9200)
    args = parser.parse_args()
    es_util.setup_connection(args.es_host, args.es_port, args.es_user, args.es_password)
    generate_variants_json_file()


if __name__ == "__main__":
    main()
