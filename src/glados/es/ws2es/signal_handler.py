from typing import Callable
import signal
import sys
import logging

__author__ = 'jfmosquera@ebi.ac.uk'


logger = logging.getLogger('chembl_ws2es')

########################################################################################################################

HANDLERS = []


def add_termination_handler(handler: Callable):
    global HANDLERS
    if len(HANDLERS) == 0:
        signal.signal(signal.SIGTERM, termination_handler)
        signal.signal(signal.SIGINT, termination_handler)
    if handler:
        HANDLERS.append(handler)


termination_attempts = 0


def termination_handler(stop_signal, frame):
    global HANDLERS, termination_attempts
    termination_attempts += 1
    logger.warning(
        'TERMINATION INVOKED! TERMINATION ATTEMPT NUMBER {} - WILL KILL PROCESS AT ATTEMPT 5'
        .format(termination_attempts)
    )
    if termination_attempts >= 5:
        sys.exit(5)
    for handler_i in HANDLERS:
        handler_i(stop_signal, frame)
