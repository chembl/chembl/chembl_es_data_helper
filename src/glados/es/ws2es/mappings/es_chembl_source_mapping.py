# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 0

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties':
        {
            'src_description': DefaultMappings.KEYWORD,
            # EXAMPLES:
            # 'DONT USE, RESERVED FOR K4DD' , 'FDA Approval Packages' , 'GSK Kinetoplastid Screening' , 'MMV Pathoge
            # n Box' , 'External Project Compounds' , 'Gene Expression Atlas Compounds' , 'Gates Library compound co
            # llection' , 'HeCaToS Compounds' , 'Withdrawn Drugs' , 'BindingDB Database'
            'src_id': DefaultMappings.ID,
            # EXAMPLES:
            # '30' , '28' , '29' , '34' , '25' , '26' , '33' , '35' , '36' , '37'
            'src_short_name': DefaultMappings.KEYWORD,
            # EXAMPLES:
            # 'FDA_APPROVAL' , 'GSK_TCAKS' , 'MMV_PBOX' , 'EXT. PROJECT CPDS' , 'ATLAS' , 'GATES_LIBRARY' , 'HECATOS
            # ' , 'WITHDRAWN' , 'BINDINGDB' , 'DRUGS'
            'src_url': DefaultMappings.KEYWORD,
            # EXAMPLES:
            # 'https://www.cancerrxgene.org' , 'https://www.ebi.ac.uk/pdbe/' , 'https://pubchem.ncbi.nlm.nih.gov' , 'htt
            # ps://www.clinicaltrials.gov/' , 'https://www.fda.gov/drugs/drug-approvals-and-databases/approved-drug-prod
            # ucts-therapeutic-equivalence-evaluations-orange-book' , 'http://togodb.dbcls.jp/open_tggates_main' , 'http
            # s://www.fda.gov/drugs/development-approval-process-drugs/novel-drug-approvals-fda;  https://www.fda.gov/va
            # ccines-blood-biologics/development-approval-process-cber/biological-approvals-year' , 'https://www.ama-ass
            # n.org/about/united-states-adopted-names' , 'https://dndi.org' , 'https://cebs.niehs.nih.gov/cebs/paper/156
            # 70'
            'src_comment': DefaultMappings.TEXT_STD,
            # EXAMPLES:
            # 'Bioactivity data for a published subset of GlaxoSmithKlines compound library (the Tres Cantos antimalaria
            # l compound set (TCAMS)) screened against P. falciparum.' , 'Bioactivity data for a published subset of the
            #  Novartis GNF library that was screened against P. falciparum.' , 'Bioactivity data for a published subset
            #  of compounds from a multi-organisation antimalarial study (including St. Jude Children’s Research Hospita
            # l) screened against P. falciparum.' , 'The Sanger Institute, Genomics of Drug Sensitivity in Cancer projec
            # t aims to identify molecular features of cancers that predict response to anti-cancer drugs.' , 'DEPRECATE
            # D' , 'Drugs that are progressing through the phases of clinical development - clinical candidate drugs. Da
            # ta are predominantly extracted from ClinicalTrials.gov via our Clinical Trials Pipeline, with a small numb
            # er of manually curated clinical candidates. ' , 'FDA Orange Book database of Approved Drug Products with T
            # herapeutic Equivalence Evaluations.' , 'DEPRECATED' , 'FDA Novel Drug Therapy Approvals and FDA Biological
            #  License Application Approvals' , 'Drugs for Neglected Diseases Initiative (DNDi) is a research organizati
            # on developing new treatments for neglected patients. Deposited datasets from this initiative were provided
            # .'
        }
    }
