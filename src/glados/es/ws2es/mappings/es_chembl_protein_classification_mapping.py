# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 1

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties': 
        {
            'class_level': DefaultMappings.SHORT,
            # EXAMPLES:
            # '0' , '1' , '1' , '1' , '1' , '1' , '2' , '1' , '1' , '1'
            'definition': DefaultMappings.TEXT_STD,
            # EXAMPLES:
            # 'Root of the ChEMBL protein family classification' , 'Biological molecules that possess catalytic activity
            # . They may occur naturally or be synthetically created. Enzymes are usually proteins, however CATALYTIC RN
            # A and CATALYTIC DNA molecules have also been identified. [MESH:D004798]' , 'Surface ligands, usually glyco
            # proteins, that mediate cell-to-cell adhesion. Their functions include the assembly and interconnection of 
            # various vertebrate systems, as well as maintenance of tissue integration, wound healing, morphogenic movem
            # ents, cellular migrations, and metastasis. [MESH:D015815]' , 'A rather large group of enzymes comprising n
            # ot only those transferring phosphate but also diphosphate, nucleotidyl residues, and others. These have al
            # so been subdivided according to the acceptor group. (From Enzyme Nomenclature, 1992) EC 2.7. [MESH:D010770
            # ]' , 'Antigens on surfaces of cells, including infectious or foreign cells or viruses. They are usually pr
            # otein-containing groups on cell membranes or walls and may be isolated. [MESH:D000954]' , 'The class of al
            # l enzymes catalyzing oxidoreduction reactions. The substrate that is oxidized is regarded as a hydrogen do
            # nor. The systematic name is based on donor:acceptor oxidoreductase. The recommended name will be dehydroge
            # nase, wherever this is possible; as an alternative, reductase can be used. Oxidase is only used in cases w
            # here O2 is the acceptor. (Enzyme Nomenclature, 1992, p9) [MESH:D010088]' , 'Cell surface proteins that bin
            # d signalling molecules external to the cell with high affinity and convert this extracellular event into o
            # ne or more intracellular signals that alter the behavior of the target cell (From Alberts, Molecular Biolo
            # gy of the Cell, 2nd ed, pp693-5). Cell surface receptors, unlike enzymes, do not chemically alter their li
            # gands. [MESH:D011956]' , 'Endogenous substances, usually proteins, which are effective in the initiation, 
            # stimulation, or termination of the genetic transcription process. [MESH:D014157]' , 'A soluble cytochrome 
            # P-450 enzyme that catalyzes camphor monooxygenation in the presence of putidaredoxin, putidaredoxin reduct
            # ase, and molecular oxygen. This enzyme, encoded by the CAMC gene also known as CYP101, has been crystalliz
            # ed from bacteria and the structure is well defined. Under anaerobic conditions, this enzyme reduces the po
            # lyhalogenated compounds bound at the camphor-binding site. [MESH:D019475]' , 'A serine endopeptidase isola
            # ted from Bacillus subtilis. It hydrolyzes proteins with broad specificity for peptide bonds, and a prefere
            # nce for a large uncharged residue in P1. It also hydrolyzes peptide amides. (From Enzyme Nomenclature, 199
            # 2) EC 3.4.21.62. [MESH:D020860]'
            'parent_id': DefaultMappings.INTEGER,
            # EXAMPLES:
            # '0' , '0' , '0' , '0' , '0' , '1' , '0' , '0' , '0' , '1'
            'pref_name': DefaultMappings.PREF_NAME,
            # EXAMPLES:
            # 'Protein class' , 'Enzyme' , 'Adhesion' , 'Secreted protein' , 'Structural protein' , 'Other nuclear prote
            # in' , 'Kinase' , 'Other membrane protein' , 'Other cytosolic protein' , 'Surface antigen'
            'protein_class_desc': DefaultMappings.TEXT_STD,
            # EXAMPLES:
            # 'protein class' , 'enzyme' , 'adhesion' , 'secreted' , 'structural' , 'nuclear other' , 'enzyme  kinase' ,
            #  'membrane other' , 'cytosolic other' , 'surface antigen'
            'protein_class_id': DefaultMappings.INTEGER,
            # EXAMPLES:
            # '0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9'
            'short_name': DefaultMappings.TEXT_STD,
            # EXAMPLES:
            # 'Protein class' , 'Enzyme' , 'Adhesion' , 'Secreted' , 'Structural' , 'Nuclear other' , 'Kinase' , 'Membra
            # ne other' , 'Cytosolic other' , 'Surface antigen'
            'sort_order': DefaultMappings.SHORT,
            # EXAMPLES:
            # '1' , '6' , '9' , '10' , '14' , '13' , '12' , '11' , '2' , '5'
        }
    }
