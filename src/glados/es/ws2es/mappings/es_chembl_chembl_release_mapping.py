# Elastic search mapping definition for the Molecule entity
from glados.es.ws2es.es_util import DefaultMappings

# Shards size - can be overridden from the default calculated value here
# shards = 3,
replicas = 0

analysis = DefaultMappings.COMMON_ANALYSIS

mappings = \
    {
        'properties':
        {
            'chembl_release': DefaultMappings.KEYWORD,
            'creation_date': DefaultMappings.DATE_Y_M_D
        }
    }
