FROM python:3.10-slim-buster
ARG USERNAME=${USERNAME:-chembl_user}
ARG UID=${UID:-123}
ARG GID=${GID:-321}
ARG WORKDIR=${WORKDIR:-/chembl-es-data-helper}

# setup user and app root directory
RUN useradd -m ${USERNAME} -u ${UID}
RUN mkdir -p ${WORKDIR}
RUN chown -R ${UID}:${GID} ${WORKDIR}
WORKDIR ${WORKDIR}

# revents Python from writing pyc files to disc and from buffering stdout and stderr
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install the pip requirements
COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

# set user
USER ${UID}:${GID}

# copy code
COPY src src
COPY *.py ./

ENTRYPOINT [ "/bin/bash" ]