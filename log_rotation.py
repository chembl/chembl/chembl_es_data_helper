from logging.handlers import RotatingFileHandler
import argparse
import sys
import yaml
import os.path

default_max_logs = 10

parser = argparse.ArgumentParser(
    description="Rotate logs given by argument."
)
parser.add_argument("--config",
                    dest="config_file",
                    help="Configuration file for the log rotation.")
args = parser.parse_args()

if not args.config_file:
    print(
        'A configuration file needs to be specified using --config', file=sys.stderr
    )
    sys.exit(1)
try:
    with open(args.config_file, 'r') as conf_file:
        config_data = yaml.safe_load(conf_file)
        max_logs = config_data.get('max-logs', default_max_logs)
        paths = config_data.get('paths', [])
        if not isinstance(paths, list) or len(paths) == 0:
            print(
                'paths should be a list with the paths to be rotated.', file=sys.stderr
            )
            sys.exit(1)
        for path_i in paths:
            if os.path.exists(path_i):
                rfh_i = RotatingFileHandler(path_i, backupCount=max_logs)
                rfh_i.doRollover()
                os.remove(path_i)
            else:
                print('WARNING: path not found {0} skipping rollover.'.format(path_i))
except:
    print(
        'Could not parse the config file at {0}'.format(args.config_file), file=sys.stderr
    )
    sys.exit(1)
